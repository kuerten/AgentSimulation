﻿using System;
using Catel.Data;

namespace AutomationInterface.Models
{
    public class MainModel : SavableModelBase<MainModel>
    {
        #region Properties

        public string MapConfigFilePath { get; set; }
        public string LoggingConfigFilePath { get; set; }
        public string[] ConfigDirectories { get; set; } = Array.Empty<string>();
        public int Seed { get; set; }
        public TimeSpan SimulationDuration { get; set; } = TimeSpan.FromHours(12);
        public bool DisplayConsole { get; set; } = true;

        #endregion
    }
}