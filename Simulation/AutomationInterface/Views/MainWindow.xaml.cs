﻿using System.Windows.Controls;

namespace AutomationInterface.Views
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        #region Constructors

        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        private void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is TextBox box) box.ScrollToEnd();
        }

        #endregion
    }
}