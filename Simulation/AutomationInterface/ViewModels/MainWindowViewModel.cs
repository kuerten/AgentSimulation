﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using AutomationInterface.Models;
using Catel.Data;
using Catel.MVVM;
using Catel.Runtime.Serialization.Xml;
using Catel.Services;

namespace AutomationInterface.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region Fields

        private readonly IOpenFileService _openFileService;
        private readonly Random _rng = new();
        private readonly ISaveFileService _saveFileService;

        private readonly CancellationTokenSource _tokenSource = new();
        private readonly IXmlSerializer _xmlSerializer;

        #endregion

        #region Constructors

        public MainWindowViewModel(IOpenFileService openFileService, ISaveFileService saveFileService, IXmlSerializer xmlSerializer)
        {
            _openFileService = openFileService;
            _saveFileService = saveFileService;
            _xmlSerializer = xmlSerializer;
            RunCommand = new TaskCommand(OnExecuteRun, CanExecuteRun);
            AbortCommand = new Command(OnExecuteAbort, CanExecuteAbort);
            SelectLoggingConfigCommand = new TaskCommand(OnExecuteSelectLoggingConfig, SimulationNotRunning);
            SelectMapConfigCommand = new TaskCommand(OnExecuteSelectMapConfig, SimulationNotRunning);
            SelectConfigCommand = new TaskCommand(OnExecuteSelectConfig, SimulationNotRunning);
            GenerateSeedCommand = new Command(() => Seed = _rng.Next(), SimulationNotRunning);
            SaveConfigCommand = new TaskCommand(OnExecuteSaveConfig);
            LoadConfigCommand = new TaskCommand(OnExecuteLoadConfig);
        }

        #endregion

        #region Properties

        [Model] public MainModel Model { get; private set; } = new();

        [ViewModelToModel] public string MapConfigFilePath { get; set; }
        [ViewModelToModel] public string LoggingConfigFilePath { get; set; }
        [ViewModelToModel] public string[] ConfigDirectories { get; set; }
        [ViewModelToModel] public int Seed { get; set; }
        [ViewModelToModel] public TimeSpan SimulationDuration { get; set; }
        [ViewModelToModel] public bool DisplayConsole { get; set; }
        public int CurrentConfigNumer { get; set; }
        public bool SimulationRunning { get; set; }
        public bool IsAborting { get; set; }
        public TaskCommand RunCommand { get; }
        public Command AbortCommand { get; }
        public TaskCommand SelectLoggingConfigCommand { get; }
        public TaskCommand SelectMapConfigCommand { get; }
        public TaskCommand SelectConfigCommand { get; }
        public Command GenerateSeedCommand { get; }
        public TaskCommand SaveConfigCommand { get; }
        public TaskCommand LoadConfigCommand { get; }

        public string ConsoleText { get; set; } = string.Empty;

        #endregion

        #region Methods

        private void LoadModel(string path)
        {
            using FileStream fileStream = File.Open(path, FileMode.Open);
            Model = MainModel.Load(fileStream, _xmlSerializer);
        }

        private bool SimulationNotRunning() => !SimulationRunning;

        private Task<int> RunSimulation(ProcessStartInfo info, CancellationToken token)
        {
            TaskCompletionSource<int> tcs = new();
            Process process = new()
            {
                StartInfo = info,
                EnableRaisingEvents = true
            };
            CancellationTokenRegistration tokenRegistration = token.Register(process.Kill);
            process.Exited += (_, _) =>
            {
                Log($"Simulation total duration: {process.ExitTime - process.StartTime:hh\\:mm\\:ss}");
                tcs.SetResult(process.ExitCode);
                process.Dispose();
                tokenRegistration.Unregister();
            };
            process.Start();
            return tcs.Task;
        }

        private void ClearLog() => Application.Current.Dispatcher.InvokeAsync(() => ConsoleText = string.Empty);

        private void Log(string message)
        {
            Application.Current.Dispatcher.InvokeAsync(() => ConsoleText += message + Environment.NewLine);
        }

        private void Log(string target, string message) => Log($"[{target}] {message}");

        protected override Task OnClosingAsync()
        {
            _tokenSource.Cancel();
            return base.OnClosingAsync();
        }

        #region Command Methods

        private bool CanExecuteAbort() => SimulationRunning;

        private void OnExecuteAbort()
        {
            IsAborting = true;
            SimulationRunning = false;
        }

        private bool CanExecuteRun() =>
            !SimulationRunning &&
            !string.IsNullOrEmpty(MapConfigFilePath) &&
            !string.IsNullOrEmpty(LoggingConfigFilePath)
            && ConfigDirectories.Length > 0;

        private async Task OnExecuteRun()
        {
            SimulationRunning = true;
            CurrentConfigNumer = 0;

            ClearLog();
            foreach (string configPath in ConfigDirectories)
            {
                if (!SimulationRunning)
                {
                    Log("Simulation has been aborted");
                    IsAborting = false;
                    return;
                }
                CurrentConfigNumer++;
                string configDirectory = Path.GetDirectoryName(configPath);
                if (configDirectory == null)
                {
                    Log($"Cannot get the directory of the config {configPath}, skipping entry");
                    continue;
                }
                string configName = Path.GetFileNameWithoutExtension(configPath);
                string configFile = Path.GetFileName(configPath);
                string workingDirectory = Path.Combine(configDirectory, "Data", configName);
                if (Directory.Exists(workingDirectory))
                {
                    if (File.Exists(Path.Combine(workingDirectory, configFile)))
                    {
                        Log(configName, "Simulation already done, skipping entry");
                        continue;
                    }
                    Log(configName, "Simulation has not been completed, resetting data");
                    Directory.Delete(workingDirectory, true);
                }
                else
                {
                    Directory.CreateDirectory(workingDirectory);
                }

                Log(configName, "Starting simulation");
                int exitCode = await RunSimulation(new ProcessStartInfo
                {
                    FileName = "Simulation.exe",
                    CreateNoWindow = !DisplayConsole,
                    ArgumentList =
                    {
                        "-s",
                        $"-l={LoggingConfigFilePath}",
                        $"-m={MapConfigFilePath}",
                        $"-input={configPath}",
                        $"-seed={Seed}",
                        $"-length={SimulationDuration.TotalSeconds}",
                        $"-loggingPath={workingDirectory}"
                    }
                }, _tokenSource.Token);
                if (exitCode == 0) File.Copy(configPath, Path.Combine(workingDirectory, configFile)); // Signals that a run was completed successfully
                Log(configName, $"Simulation stopped with code {exitCode}");
            }
            Log("Done!");
            SimulationRunning = false;
        }

        private async Task OnExecuteLoadConfig()
        {
            DetermineOpenFileContext context = new()
            {
                Filter = "Config files|*.xml",
                CheckFileExists = true
            };
            DetermineOpenFileResult result = await _openFileService.DetermineFileAsync(context);
            if (result.Result) LoadModel(result.FileName);
        }

        private async Task OnExecuteSaveConfig()
        {
            DetermineSaveFileContext context = new()
            {
                Filter = "Config files|*.xml",
                CheckFileExists = true,
                AddExtension = true
            };
            DetermineSaveFileResult result = await _saveFileService.DetermineFileAsync(context);
            if (result.Result) Model.SaveAsXml(result.FileName);
        }

        private async Task OnExecuteSelectConfig()
        {
            DetermineOpenFileContext context = new()
            {
                Filter = "Json files|*.json*",
                CheckFileExists = true,
                IsMultiSelect = true
            };
            DetermineOpenFileResult result = await _openFileService.DetermineFileAsync(context);
            if (result.Result) ConfigDirectories = result.FileNames.Select(x => Path.GetRelativePath(Directory.GetCurrentDirectory(), x)).ToArray();
        }

        private async Task OnExecuteSelectMapConfig()
        {
            DetermineOpenFileContext context = new()
            {
                Filter = "Json files|*.json*",
                CheckFileExists = true,
                FileName = MapConfigFilePath
            };
            DetermineOpenFileResult result = await _openFileService.DetermineFileAsync(context);
            if (result.Result) MapConfigFilePath = Path.GetRelativePath(Directory.GetCurrentDirectory(), result.FileName);
        }

        private async Task OnExecuteSelectLoggingConfig()
        {
            DetermineOpenFileContext context = new()
            {
                Filter = "Json files|*.json*",
                CheckFileExists = true,
                FileName = LoggingConfigFilePath
            };
            DetermineOpenFileResult result = await _openFileService.DetermineFileAsync(context);
            if (result.Result) LoggingConfigFilePath = Path.GetRelativePath(Directory.GetCurrentDirectory(), result.FileName);
        }
        
        #endregion

        #endregion
    }
}