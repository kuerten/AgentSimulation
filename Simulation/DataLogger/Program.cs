﻿using System;
using System.Collections.Generic;
using System.Net;
using NDesk.Options;

namespace DataLogger
{
    public class Program
    {
        private static void Main(string[] args)
        {
            string targetDirectory = null;
            bool showHelp = false;
            IPEndPoint targetEndpoint = new IPEndPoint(IPAddress.Parse("239.0.1.2"), 12345);
            var p = new OptionSet
            {
                {"d|directory=", "directory for writing data", dir => targetDirectory = dir},
                {"h|?|help", "show this message and exit", v => showHelp = v != null},
                {"i|ip=", "UDP multicast address to listen on, default is 239.0.1.2", s => targetEndpoint.Address = IPAddress.Parse(s) },
                {"p|port=", "UDP port to listen on, default is 12345", s => targetEndpoint.Port = int.Parse(s) }
            };

            List<string> extra;
            try
            {
                extra = p.Parse(args);
            }
            catch (OptionException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Try --help' for more information.");
                return;
            }

            if (extra.Count > 0)
            {
                Console.WriteLine($"Unkown parameter: {string.Join(',', extra)}");
                Console.WriteLine("Try --help' for more information.");
                return;
            }

            if (showHelp)
            {
                ShowHelp(p);
                return;
            }
            
            UdpReceiver receiver = new UdpReceiver(targetEndpoint);
            if (targetDirectory != null) receiver.DirectoryBasePath = targetDirectory;

            Console.WriteLine("Starting logger, press any key to stop...");
            receiver.Start();
            Console.ReadKey();
            receiver.Stop();
            Console.WriteLine("Logger stopped");
        }

        private static void ShowHelp(OptionSet p)
        {
            Console.WriteLine("Usage: [OPTIONS]+ message");
            Console.WriteLine("Logs and saves data of a simulation.");
            Console.WriteLine("If no configuration is specifid, default values will be used.");
            Console.WriteLine();
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);
        }
    }
}
