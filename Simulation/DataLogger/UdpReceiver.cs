﻿using EntitySimulation.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EntitySimulation.Data.Transmitter;
using Utility;

namespace DataLogger
{
    public class UdpReceiver
    {
        #region Constants

        public const string DataFileName = "data.txt";
        public const string ConfigFileName = "config.txt";
        public static string EntrySeparator = Environment.NewLine;

        #endregion

        #region Fields

        private readonly UdpClient _client;
        private readonly object _locObject = new object();
        private readonly TimeSpan _minDelay = TimeSpan.FromMilliseconds(50);
        private Queue<byte[]> _dataBuffer = new Queue<byte[]>();
        private Queue<byte[]> _backDataBuffer = new Queue<byte[]>();
        private string _currentDirectory;
        private DateTime _lastCall;
        private Task _networkTask;
        private Task _processingTask;
        private CancellationTokenSource _tokenSource;

        #endregion

        #region Constructors

        public UdpReceiver(IPEndPoint endPoint)
        {
            Endpoint = endPoint;
            _client = new UdpClient(Endpoint.Port);
            _client.JoinMulticastGroup(Endpoint.Address);
        }

        #endregion

        #region Properties

        public IPEndPoint Endpoint { get; }
        public bool Running { get; private set; }
        public string DirectoryBasePath { get; set; } = Path.Combine(Directory.GetCurrentDirectory(), "Data");

        #endregion

        #region Methods

        public void Start()
        {
            if (Running) return;
            _tokenSource = new CancellationTokenSource();
            CancellationToken token = _tokenSource.Token;
            _lastCall = DateTime.Now;
            _processingTask = Task.Run(() => ProcessData(token), token);
            _networkTask = Task.Run(() => ReceiveAsync(token), token);
            Running = true;
        }

        public void Stop()
        {
            if (!Running) return;
            _tokenSource.Cancel();
            _networkTask.Wait();
            _processingTask.Wait();
            Running = false;
        }

        private async Task ReceiveAsync(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                UdpReceiveResult result = await _client.ReceiveAsync();
                lock (_locObject)
                {
                    _dataBuffer.Enqueue(result.Buffer);
                }
            }
        }

        private async Task ProcessData(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                DateTime now = DateTime.Now;
                TimeSpan diff = now - _lastCall;
                if (diff < _minDelay)
                {
                    await Task.Delay(_minDelay - diff, token);
                    continue;
                }
                _lastCall = now;

                lock (_locObject)
                {
                    Queue<byte[]> temp = _dataBuffer;
                    _backDataBuffer = _dataBuffer;
                    _dataBuffer = temp;
                }

                Dictionary<string, List<string>> dataToSave = new Dictionary<string, List<string>>();
                while (_backDataBuffer.Count > 0)
                {
                    JObject entry = JObject.Parse(Encoding.ASCII.GetString(_backDataBuffer.Dequeue()));
                    switch ((EMessageType)entry["MessageType"].ToObject<int>())
                    {
                        case EMessageType.Delete:
                            break;
                        case EMessageType.Update:
                            if (string.IsNullOrEmpty(_currentDirectory))
                            {
                                Console.WriteLine("No directory to write, entry will be skipped");
                                break;
                            }
                            entry.Remove("MessageType");
                            dataToSave.GetOrCreate(Path.Combine(_currentDirectory, DataFileName)).Add(entry.ToString(Formatting.None));
                            break;
                        case EMessageType.Reset:
                            _currentDirectory = Path.Combine(DirectoryBasePath, DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
                            Directory.CreateDirectory(_currentDirectory);
                            break;
                        case EMessageType.Config:
                            dataToSave.GetOrCreate(Path.Combine(_currentDirectory, ConfigFileName)).Add(entry["Config"].ToString(Formatting.Indented));
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                int written = 0;
                foreach ((string path, List<string> lines) in dataToSave)
                {
                    await using FileStream fs = File.Open(path, FileMode.Append, FileAccess.Write);
                    await using StreamWriter sw = new StreamWriter(fs);
                    await sw.WriteAsync(string.Concat( string.Join(EntrySeparator, lines), EntrySeparator));
                    written += lines.Count;
                }
                if (written > 0) Console.WriteLine($"saved {written} entries");
            }
        }

        #endregion
    }
}