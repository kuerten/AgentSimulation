﻿using System;
using System.Collections.Generic;
using System.Linq;
using Neat.ActivationFunctions;
using Neat.Genes;
using Neat.Innovation;
using Neat.Operators.Selection;
using Neat.Species;

namespace Neat
{
    public class Population
    {
        private const int DefaultCycles = 4;

        private long _geneIdCounter;
        private long _innovationIdCounter;

        public int PopulationSize;
        public double WeightRange { get; set; } = 5;
        public int InputCount { get; set; }
        public int OutputCount { get; set; }
        public SelectorList<IActivationFunction> ActivationFunctions { get; } = new SelectorList<IActivationFunction>();
        public double InitialConnectionDensity { get; set; } = 0.1;
        public List<BasicSpecies> Species { get; } = new List<BasicSpecies>();
        private Genome BestGenome { get; set; }

        public int Count => Flatten().Count();

        public InnovationList Innovations { get; set; }
        public int ActivationCycles { get; set; } = DefaultCycles;
        public Population() { }

        public Population(int inputCount, int outputCount)
        {
            InputCount = inputCount;
            OutputCount = outputCount;
            ActivationFunctions.Add(new ActivationSigmoid(), 1);
        }

        /*public BasicSpecies CreateSpecies()
        {
            BasicSpecies species = new BasicSpecies();
            Species.Add(species);
            return species;
        }*/

        public BasicSpecies BestSpecies() => Species.FirstOrDefault(s => s.Contains(BestGenome));

        public IEnumerable<Genome> Flatten() => Species.SelectMany(s => s);

        public void PurgeInvalidGenomes()
        {
            for (int i = Species.Count - 1; i >= 0; i--)
            {
                BasicSpecies species = Species[i];
                species.RemoveInvalids();
                if (!species.IsEmpty) continue;
                Species.Remove(species);
                if (species.Contains(species.Leader)) continue;
                species.Leader = species[0];
            }
        }

        public long NextGeneId() => _geneIdCounter++;

        public long NextInnovationId() => _innovationIdCounter++;

        public void Reset()
        {
            Species.Clear();
            _geneIdCounter = 0;
            _innovationIdCounter = 0;
            BasicSpecies defaultSpecies = new BasicSpecies();
            Random rng = new Random();
            for (int i = 0; i < PopulationSize; i++) defaultSpecies.Add(Genome.Random(rng, InputCount, OutputCount, InitialConnectionDensity));
            defaultSpecies.Leader = defaultSpecies[0];
            Species.Add(defaultSpecies);
            Innovations = new InnovationList(this);
        }
    }

    public static class MathEx
    {
        public static double Clamp(this double w, double range) => w < -range ? -range : w > range ? range : w;
    }
}