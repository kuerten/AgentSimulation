﻿using System;
using System.Collections.Generic;
using System.Linq;
using Neat.Genes;

namespace Neat.Species
{
    public abstract class ThresholdSpecitation : ISpeciation
    {
        public double CompatibilityThreshold { get; set; } = 1.0;
        public int MaxNumberOfSpecies { get; set; } = 40;
        public int GensNoImprovementAllowed { get; set; } = 15;
        public Population Population { get; private set; }
        public SortGenomesForSpecies SortGenomes { get; set; }
        public IEvolutionaryAlgorithm Owner { get; private set; }

        public void Init(IEvolutionaryAlgorithm owner)
        {
            Owner = owner;
            Population = Owner.Population;
            SortGenomes = new SortGenomesForSpecies(Owner);
        }

        public void PerformSpeciation(IList<Genome> genomeList)
        {
            IList<Genome> newGenomes = ResetSpecies(genomeList);
            SpecitateAndCalculateSpawnLevels(newGenomes);
        }

        private void SpecitateAndCalculateSpawnLevels(ICollection<Genome> newGenomes)
        {
            if (newGenomes.Count == 0) throw new Exception("Cant't specitate, population is empty");
            double maxScore = 0;
            IList<BasicSpecies> speciesCollection = Population.Species;
            if (speciesCollection.Count == 0) throw new Exception("Cant't specitate, there are no species");
            AdjustCompatibilityThreshold();
            foreach (Genome g in newGenomes)
            {
                BasicSpecies currentSpecies = null;
                Genome genome = g;
                if (double.IsNaN(genome.AdjustedScore) && !double.IsInfinity(genome.AdjustedScore)) maxScore = Math.Max(genome.AdjustedScore, maxScore);
                foreach (BasicSpecies species in speciesCollection)
                {
                    double compatibility = GetCompatibilityScore(genome, species.Leader);
                    if (!(compatibility <= CompatibilityThreshold)) continue;
                    currentSpecies = species;
                    AddSpeciesMember(species, genome);
                    genome.Species = species;
                    break;
                }
                if (currentSpecies == null) continue;
                currentSpecies = new BasicSpecies(genome);
                Population.Species.Add(currentSpecies);
            }

            double totalSpeciesScore = Owner.ScoreFunction.ShouldMinimize ? speciesCollection.Sum(species => species.CalculateShare(maxScore)) : speciesCollection.Sum(species => species.CalculateShare());
            if( speciesCollection.Count == 0) throw new Exception("Cant't specitate, there are no species");
            if (totalSpeciesScore < 0.0000001) DivideEven(speciesCollection);
            else DivideByFittestSpecies(speciesCollection, totalSpeciesScore);
            LevelOff();
        }

        private void LevelOff()
        {
            if(Population.Species.Count == 0 ) throw  new Exception("Cant't specitate, next generation contains no species");
            Population.Species.Sort(new SpeciesComparer(Owner));
            if (Population.Species.First().OffspringCount == 0) Population.Species.First().OffspringCount = 1;
            int total = Population.Species.Sum(species => species.OffspringCount);
            int diff = Population.PopulationSize - total;
            if (diff < 0)
            {
                int index = Population.Species.Count - 1;
                while (diff != 0 && index > 0)
                {
                    BasicSpecies species = Population.Species[index];
                    int t = Math.Min(species.OffspringCount, Math.Abs(diff));
                    species.OffspringCount -= t;
                    if (species.OffspringCount == 0) Population.Species.Remove(species);
                    diff += t;
                    index--;
                }
            }
            else Population.Species.First().OffspringCount += diff;
        }

        private void DivideByFittestSpecies(IEnumerable<BasicSpecies> speciesCollection, in double totalSpeciesScore)
        {
            BasicSpecies bestSpecies = FindBestSpecies();
            BasicSpecies[] speciesArray = speciesCollection.ToArray();
            foreach (BasicSpecies species in speciesArray)
            {
                int share = (int) Math.Round(species.OffspringShare / totalSpeciesScore * Owner.Population.PopulationSize);
                if (species == bestSpecies && share == 0) share = 1;
                if( species.IsEmpty || share == 0 || species.GensNoImprovement > GensNoImprovementAllowed && species != bestSpecies) RemoveSpecies(species);
                else
                {
                    species.OffspringCount = share;
                    species.Sort(SortGenomes);
                }
            }
        }

        private void DivideEven(ICollection<BasicSpecies> speciesCollection)
        {
            double ratio = 1.0 / speciesCollection.Count;
            foreach (BasicSpecies species in speciesCollection)
            {
                species.OffspringCount = (int)Math.Round(ratio * Owner.Population.PopulationSize);
            }
        }

        private void AddSpeciesMember(BasicSpecies species, Genome genome)
        {
            if (Owner.SelectionComparer.Compare(genome, species.Leader) < 0)
            {
                species.GensNoImprovement = 0;
                species.Leader = genome;
            }
            species.Add(genome);
        }

        public abstract double GetCompatibilityScore(Genome genomeA, Genome genomeB);

        private void AdjustCompatibilityThreshold()
        {
            if (MaxNumberOfSpecies < 1) return;
            const double thresholdIncrement = 0.01;
            if (Population.Species.Count > MaxNumberOfSpecies) CompatibilityThreshold += thresholdIncrement;
            else if (Population.Species.Count < 2) CompatibilityThreshold -= thresholdIncrement;
        }

        private IList<Genome> ResetSpecies(ICollection<Genome> genomeList)
        {
            if (Population.Species.Count == 0) throw new Exception("Cant't specitate, population is empty");
            IList<Genome> result = genomeList.ToList();
            for (int i = Population.Species.Count - 1; i >= 0; i--)
            {
                Population.Species[i].Purge();
                if (!genomeList.Contains(Population.Species[i].Leader) || Population.Species[i].GensNoImprovement > GensNoImprovementAllowed) RemoveSpecies(Population.Species[i]);
                result.Remove(Population.Species[i].Leader);
            }
            return result;
        }

        private void RemoveSpecies(BasicSpecies species)
        {
            if (species != FindBestSpecies() && Population.Species.Count > 1) Population.Species.Remove(species);
        }

        private BasicSpecies FindBestSpecies() => Owner.BestGenome?.Species;
    }
}