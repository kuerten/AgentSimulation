﻿using System;
using System.Collections.Generic;

namespace Neat.Species
{
    public class SpeciesComparer : IComparer<BasicSpecies>
    {
        private readonly IEvolutionaryAlgorithm _trainer;
        public SpeciesComparer(IEvolutionaryAlgorithm owner)
        {
            _trainer = owner;
        }

        public int Compare(BasicSpecies x, BasicSpecies y)
        {
            if (x == null || y == null) throw new Exception("Cannot compare null");
            return _trainer.BestComparer.Compare(x.Leader, y.Leader);
        }
    }
}