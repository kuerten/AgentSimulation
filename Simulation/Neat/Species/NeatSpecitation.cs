﻿using System;
using Neat.Genes;

namespace Neat.Species
{
    public class NeatSpecitation : ThresholdSpecitation
    {
        public double ConstDisjoint { get; set; } = 1;
        public double ConstExcess { get; set; } = 1;
        public double ConstMatched { get; set; } = 0.4;

        public override double GetCompatibilityScore(Genome genomeA, Genome genomeB)
        {
            double numDisjoint = 0;
            double numExcess = 0;
            double numMatched = 0;
            double weightDifference = 0;
            int gA = 0;
            int gB = 0;
            while (gA < genomeA.Size || gB < genomeB.Size)
            {
                if (gA == genomeA.Size)
                {
                    gB++;
                    numExcess++;
                    continue;
                }
                if (gB == genomeB.Size)
                {
                    gA++;
                    numExcess++;
                    continue;
                }
                long innoIdA = genomeA.Links[gA].InnovationId;
                long innoIdB = genomeB.Links[gB].InnovationId;

                if (innoIdA == innoIdB)
                {
                    weightDifference += Math.Abs(genomeA.Links[gA].Weight - genomeB.Links[gB].Weight);
                    gA++;
                    gB++;
                    numMatched++;
                }
                else if (innoIdA < innoIdB)
                {
                    numDisjoint++;
                    gA++;
                }
                else if (innoIdA > innoIdB)
                {
                    numDisjoint++;
                    gB++;
                }
            }
            return ConstExcess * numExcess + ConstDisjoint * numDisjoint + ConstMatched * (weightDifference / numMatched);
        }
    }
}
