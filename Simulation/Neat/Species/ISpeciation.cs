﻿using System.Collections.Generic;
using Neat.Genes;

namespace Neat.Species
{
    public interface ISpeciation
    {
        void Init(IEvolutionaryAlgorithm owner);
        void PerformSpeciation(IList<Genome> genomeList);
    }
}