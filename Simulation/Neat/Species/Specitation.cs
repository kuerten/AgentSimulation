﻿using System;
using System.Collections.Generic;
using System.Linq;
using Neat.Genes;

namespace Neat.Species
{
    public class Specitation
    {
        public double CompatibilityThreshold { get; set; } = 1;
        public double MaxNumberOfSpecies { get; set; } = 40;
        public int GensAllowedNoImprovement { get; set; } = 15;
        public SortGenomesForSpecies SortGenomes { get; set; }
        public IEvolutionaryAlgorithm Owner { get; set; }
        public Population Population { get; set; }

        public double ConstDisjoint { get; set; } = 1;

        public double ConstExcess { get; set; } = 1;
        public double ConstMatched { get; set; } = 0.4;

        public void AddSpeciesMember(BasicSpecies species, Genome genome)
        {
            if (Owner.SelectionComparer.Compare(genome, species.Leader) < 0)
            {
                species.Leader = genome;
                species.GensNoImprovement = 0;
                
            }
            species.Add(genome);
        }

        private void AdjustCompatibilityThreshold()
        {
            if (MaxNumberOfSpecies < 1) return;
            const double thresholdIncrement = 0.01;
            if (Population.Species.Count > MaxNumberOfSpecies) CompatibilityThreshold += thresholdIncrement;
            else if (Population.Species.Count < 2) CompatibilityThreshold -= thresholdIncrement;
        }

        private void DivideByFittestSpecies(IEnumerable<BasicSpecies> speciesCollection, double totalSpeciesScore)
        {
            BasicSpecies bestSpecies = FindBestSpecies();
            foreach (BasicSpecies species in speciesCollection.ToArray())
            {
                int share = (int)Math.Round(species.OffspringShare / totalSpeciesScore * Owner.Population.PopulationSize);
                if (species == bestSpecies && share == 0) share = 1;
                if (species.Count == 0 || share == 0 || species.GensNoImprovement > GensAllowedNoImprovement && species != bestSpecies) RemoveSpecies(species);
                else
                {
                    species.OffspringCount = share;
                    species.Sort(SortGenomes);
                }
            }
        }



        public void RemoveSpecies(BasicSpecies species)
        {
            if (species == FindBestSpecies() || Population.Species.Count <= 1) return;
            Population.Species.Remove(species);
        }

        public BasicSpecies FindBestSpecies() => Owner.BestGenome?.Species;
        private void DIvideEven(ICollection<BasicSpecies> speciesCollection)
        {
            double ratio = 1.0 / speciesCollection.Count;
            int share = (int)Math.Round(ratio * Owner.Population.PopulationSize);
            foreach (BasicSpecies species in speciesCollection) species.OffspringCount = share;
        }

        private void LevelOff()
        {
            List<BasicSpecies> list = Population.Species;
            if (list.Count == 0) throw new Exception("Cant speciate, next generation contains no species");
            list.Sort(new SpeciesComparer(Owner));
            if (list[0].OffspringCount == 0) list[0].OffspringCount = 1;
            int total = list.Sum(s => s.OffspringCount);
            int diff = Population.PopulationSize - total;
            if (diff < 0)
            {
                for (int i = list.Count - 1; i > 0 && diff != 0; i--)
                {
                    BasicSpecies species = list[i];
                    int t = Math.Min(species.OffspringCount, Math.Abs(diff));
                    species.OffspringCount -= t;
                    if (species.OffspringCount == 0) list.Remove(species);
                    diff += t;
                }
            }
            else list[0].OffspringCount = list[0].OffspringCount + diff;
        }

        private IList<Genome> ResetSpecies(IList<Genome> inputGenomes)
        {
            BasicSpecies[] speciesArray = Population.Species.ToArray();
            IList<Genome> result = inputGenomes.ToList();
            foreach (BasicSpecies species in speciesArray)
            {
                BasicSpecies s = (BasicSpecies)species;
                s.Purge();
                if (!inputGenomes.Contains(s.Leader) || s.GensNoImprovement > GensAllowedNoImprovement) RemoveSpecies(s);
                result.Remove(s.Leader);
            }
            if (Population.Species.Count == 0) throw new Exception("Cant specitate, population is empty");
            return result;
        }

        private void SpecitateAndCalculateSpawnLevels(IList<Genome> genomes)
        {
            double maxScore = 0;
            if (genomes.Count == 0) throw new Exception("Population is empty");
            IList<BasicSpecies> speciesList = Population.Species;
            if (speciesList.Count == 0) throw new Exception("No Species available");
            AdjustCompatibilityThreshold();
            foreach (Genome genome in genomes)
            {
                BasicSpecies currentSpecies = null;
                if (!double.IsNaN(genome.AdjustedScore) && !double.IsInfinity(genome.AdjustedScore)) maxScore = Math.Max(genome.AdjustedScore, maxScore);
                foreach (BasicSpecies species in speciesList)
                {
                    double compatibility = GetCompatibilityScore(genome, species.Leader);
                    if (!(compatibility <= CompatibilityThreshold)) continue;
                    currentSpecies = species;
                    AddSpeciesMember(species, genome);
                    genome.Species = species;
                    break;
                }

                if (currentSpecies != null) continue;
                currentSpecies = new BasicSpecies(genome);
                Population.Species.Add(currentSpecies);
            }

            double totalSpeciesScore = speciesList.Sum(s => Owner.ScoreFunction.ShouldMinimize ? s.CalculateShare(maxScore) : s.CalculateShare());
            if (speciesList.Count == 0) throw new Exception("No Speceis available");
            if (totalSpeciesScore < 0.0000001) DIvideEven(speciesList);
            else DivideByFittestSpecies(speciesList, totalSpeciesScore);
            LevelOff();
        }

        private double GetCompatibilityScore(Genome a, Genome b)
        {
            double numDisjoint = 0;
            double numExces = 0;
            double numMatched = 0;
            double weightDiff = 0;
            Genome gen1 = (Genome)a;
            Genome gen2 = (Genome)b;
            int g1 = 0;
            int g2 = 0;
            while (g1 < gen1.Links.Count || g2 < gen2.Links.Count)
            {
                if (g1 == gen1.Links.Count)
                {
                    g2++;
                    numExces++;
                    continue;
                }

                if (g2 == gen2.Links.Count)
                {
                    g1++;
                    numExces++;
                    continue;
                }

                if (gen1.Links[g1].InnovationId < gen2.Links[g2].InnovationId)
                {
                    numDisjoint++;
                    g1++;
                }
                else if (gen1.Links[g1].InnovationId > gen2.Links[g2].InnovationId)
                {
                    numDisjoint++;
                    g2++;
                }
                else
                {
                    weightDiff += Math.Abs(gen1.Links[g1].Weight - gen2.Links[g2].Weight);
                    g1++;
                    g2++;
                    numMatched++;
                }
            }
            return ConstExcess * numExces + ConstDisjoint * numDisjoint + ConstMatched * weightDiff / numMatched;
        }
    }
}