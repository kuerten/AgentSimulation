﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Neat.Genes;

namespace Neat.Species
{
    public class BasicSpecies : IList<Genome>
    {
        private Genome _leader;
        public int Age { get; set; }
        public double BestScore => _leader?.AdjustedScore ?? double.NaN;
        public int GensNoImprovement { get; set; }

        private readonly List<Genome> _member = new List<Genome>();
        public bool IsEmpty => _member.Count == 0;
        public Genome Leader
        {
            get => _leader;
            set
            {
                if (value == Leader) return;
                if (value != null && !Contains(value)) throw new Exception("Leader must be part of the species");
                _leader = value;
            }
        }

        public int OffspringCount { get; set; }
        public double OffspringShare { get; set; }

        public BasicSpecies() { }
        public BasicSpecies(Genome leader)
        {
            Leader = leader;
            Add(leader);
        }

        /// <summary>
        /// Calculate share of the Genome(s) in the next population
        /// </summary>
        /// <returns></returns>
        public double CalculateShare()
        {
            double total = 0;
            int count = 0;
            foreach (Genome genome in this)
            {
                if (double.IsNaN(genome.AdjustedScore) || double.IsInfinity(genome.AdjustedScore)) continue;
                total += genome.AdjustedScore;
                count++;
            }
            OffspringShare = count == 0 ? 0 : total / count;
            return OffspringShare;
        }

        /// <summary>
        /// Calculate share of the Genome(s) in the next population
        /// </summary>
        /// <param name="maxScore">The best score</param>
        /// <returns></returns>
        public double CalculateShare(double maxScore)
        {
            double total = 0;
            int count = 0;
            foreach (Genome genome in this)
            {
                if (double.IsNaN(genome.AdjustedScore) || double.IsInfinity(genome.AdjustedScore)) continue;
                total += maxScore - genome.AdjustedScore;
                count++;
            }
            OffspringShare = count == 0 ? 0 : total / count;
            return OffspringShare;
        }

        /// <summary>
        /// Purge all members and start with the last leader
        /// </summary>
        public void Purge()
        {
            Clear();
            if (Leader != null) Add(Leader);
            Age++;
            GensNoImprovement++;
            OffspringCount = 0;
            OffspringShare = 0;
        }

        public void RemoveInvalids() => _member.RemoveAll(genome =>
            double.IsInfinity(genome.Score) || double.IsNaN(genome.Score) || double.IsInfinity(genome.AdjustedScore) || double.IsNaN(genome.AdjustedScore));

        public IEnumerator<Genome> GetEnumerator() => _member.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public void Add(Genome item)
        {
            if (item == null) throw new ArgumentException($"Cannot add null", nameof(item));
            if (Contains(item)) throw new ArgumentException($"Species already contains genome: {item}", nameof(item));
            _member.Add(item);
        }

        public int Merge(IEnumerable<Genome> genomes)
        {
            int added = 0;
            foreach (Genome genome in genomes)
            {
                if (Contains(genome)) continue;
                Add(genome);
                added++;
            }
            return added;
        }

        public void Clear() => _member.Clear();

        [Pure]public bool Contains(Genome item) => _member.Contains(item);

        public void CopyTo(Genome[] array, int arrayIndex) => _member.CopyTo(array, arrayIndex);

        public bool Remove(Genome item) => _member.Remove(item);

        public int Count => _member.Count;
        public bool IsReadOnly => false;
        [Pure] public int IndexOf(Genome item) => _member.IndexOf(item);

        public void Insert(int index, Genome item)
        {
            if (item == null) throw new ArgumentException($"Cannot add null", nameof(item));
            if (Contains(item)) throw new ArgumentException($"Species already contains genome: {item}", nameof(item));
            _member.Insert(index, item);
        }

        public void RemoveAt(int index) => _member.RemoveAt(index);

        public Genome this[int index]
        {
            get => _member[index];
            set
            {
                if (value == null) throw new ArgumentException($"Cannot add null", nameof(value));
                if (Contains(value)) throw new ArgumentException($"Species already contains genome: {value}", nameof(value));
                _member[index] = value;
            }
        }

        public void Sort(SortGenomesForSpecies sortGenomes) => _member.Sort(sortGenomes);

        [Pure] public override string ToString() => $"[BasicSpecies: score={BestScore:F2}, members={Count}, age={Age}, no_improv={GensNoImprovement}, share={OffspringShare}]";
    }
}