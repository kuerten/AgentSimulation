﻿using System.Collections.Generic;
using Neat.Genes;

namespace Neat.Species
{
    public class SortGenomesForSpecies : IComparer<Genome>
    {
        private readonly IEvolutionaryAlgorithm _train;

        public SortGenomesForSpecies(IEvolutionaryAlgorithm train)
        {
            _train = train;
        }

        public int Compare(Genome x, Genome y)
        {
            if (x == null || y == null) return 0;
            int result = _train.SelectionComparer.Compare(x, y);
            return result != 0 ? result : y.BirthGeneration - x.BirthGeneration;
        }
    }
}