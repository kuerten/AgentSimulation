﻿using System;
using System.Collections.Generic;
using Neat.Genes;

namespace Neat.Species
{
    public class SingleSpeciation : ISpeciation
    {
        private IEvolutionaryAlgorithm _owner;
        private SortGenomesForSpecies _sortGenomes;
        public void Init(IEvolutionaryAlgorithm owner)
        {
            _owner = owner;
            _sortGenomes = new SortGenomesForSpecies(_owner);
        }

        public void PerformSpeciation(IList<Genome> genomeList)
        {
            UpdateShare();
            BasicSpecies species = _owner.Population.Species[0];
            species.Merge(genomeList);
            species.Sort(_sortGenomes);
            species.Leader = species[0];
        }

        private void UpdateShare()
        {
            int speciesCount = _owner.Population.Species.Count;
            if (speciesCount != 1) throw new Exception($"SingleSpeciation can only be used with a species count of 1, species count is {speciesCount}");
            _owner.Population.Species[0].OffspringCount = _owner.Population.PopulationSize;
        }
    }
}