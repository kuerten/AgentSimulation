﻿using System.Collections.Generic;
using Neat.Genes;
using Neat.Operators;
using Neat.Operators.Rules;
using Neat.Species;

namespace Neat
{
    public interface IEvolutionaryAlgorithm
    {
        IGenomeComparer BestComparer { get; set; }
        Genome BestGenome { get; set; }
        double Error { get; }
        int IterationNumber { get; }
        int MaxIndividualSIze { get; }
        int MaxTries { get; }
        OperatorList Operators { get; }
        Population Population { get; set; }
        IRuleHolder Rules { get; set; }
        IList<IAdjustScore> ScoreAdjusters { get; }
        IGenomeComparer SelectionComparer { get; set; }
        ISpeciation Speciation { get; set; }
        ICalculateScore ScoreFunction { get; }
        void AddOperation(double probability, IEvolutionaryOperator opp);
        void AddScoreAdjuster(IAdjustScore a);
        void CalculateScore(Genome g);
        void Iteration();
    }

    public interface ICalculateScore
    {
        bool ShouldMinimize { get; }
        double CalculateScore();
    }
}