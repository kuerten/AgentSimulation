﻿namespace Neat
{
    public interface IAdjustScore
    {
        double CalculateAdjustment(IGenome genome);
    }

    public interface IGenome { }
}