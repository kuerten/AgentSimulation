﻿using System;
using System.Collections.Generic;
using Neat.Genes;
using Neat.Operators.Mutation;
using Neat.Operators.Recombination;
using Neat.Operators.Selection;
using Neat.Species;

namespace Neat
{
    public class Trainer
    {
        public List<Genome> NewPopulation { get; } = new List<Genome>();
        private IMutation Mutations { get; }
        private IRecombinator Recombinators { get; }
        private Random _rng = new Random();
        private bool _initialized;
        private Genome _oldBestGenome;
        private ISpeciation _speciation = new NeatSpecitation();
        public Population Population { get; set; }
        public long IterationNumber { get; private set; }
        
        public Trainer(Population population)
        {
            CompoundMutation weightMutation = new CompoundMutation
            {
                { new MutateWeights(new SelectFixed(1), new MutatePerturbLinkWeight(0.002)),0.1125},
                { new MutateWeights(new SelectFixed(2), new MutatePerturbLinkWeight(0.002)),0.1125},
                { new MutateWeights(new SelectFixed(3), new MutatePerturbLinkWeight(0.002)),0.1125},
                { new MutateWeights(new SelectProportion(0.02), new MutatePerturbLinkWeight(0.002)),0.1125},
                { new MutateWeights(new SelectFixed(1), new MutatePerturbLinkWeight(1)),0.1125},
                { new MutateWeights(new SelectFixed(2), new MutatePerturbLinkWeight(1)),0.1125},
                { new MutateWeights(new SelectFixed(3), new MutatePerturbLinkWeight(1)),0.1125},
                { new MutateWeights(new SelectProportion(0.02), new MutatePerturbLinkWeight(1)),0.1125},
                { new MutateWeights(new SelectFixed(1), new MutateResetLinkWeight()),0.03},
                { new MutateWeights(new SelectFixed(2), new MutateResetLinkWeight()),0.03},
                { new MutateWeights(new SelectFixed(3), new MutateResetLinkWeight()),0.03},
                { new MutateWeights(new SelectProportion(0.02), new MutateResetLinkWeight()),0.01}
            };
            Mutations = new CompoundMutation
            {
                {weightMutation, 0.494},
                {new MutateAddNode(), 0.0005 },
                {new MutateAddLink(), 0.005 },
                {new MutateRemoveLink(), 0.0005 }
            };
            Recombinators = new RecombinatorList
            {
                {new Crossover(), 0.5}
            };
        }

        public void Iteration()
        {
            if (Population.Species.Count == 0) throw new Exception("Population is empty");
            IterationNumber++;
            foreach (BasicSpecies species in Population.Species)
            {
                //Recombinators.PerformRecombination( species. )
            }
        }

        private void PerformRecombination()
        {
            if (Recombinators == null) return;
            //Recombinators.PerformRecombination(  )
        }
    }
}