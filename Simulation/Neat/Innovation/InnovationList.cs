﻿using System.Collections.Generic;

namespace Neat.Innovation
{
    public class InnovationList : Dictionary<string, Innovation>
    {
        public Population Population { get; set; }

        public InnovationList() { }

        public InnovationList(Population population)
        {
            Population = population;
            FindOrCreateInnovation(Population.NextGeneId());
            for (int i = 0; i < Population.InputCount; i++) FindOrCreateInnovation(Population.NextGeneId());
            for (int i = 0; i < Population.OutputCount; i++) FindOrCreateInnovation(Population.NextGeneId());
            for (int i = 0; i < Population.InputCount; i++)
            {
                for (int j = 0; j < Population.OutputCount; j++)
                {
                    FindOrCreateInnovation(i, j);
                }
            }
        }


        public static string ProduceKeyNeuron(long id)
        {
            return $"n:{id}";
        }

        public static string ProduceKeyNeuronSplit(long fromId, long toId)
        {
            return $"ns:{fromId}:{toId}";
        }

        public static string ProduceKeyLink(long fromId, long toId)
        {
            return $"l:{fromId}:{toId}";
        }

        public Innovation FindOrCreateInnovationSplit(long fromId, long toId)
        {
            string key = ProduceKeyNeuronSplit(fromId, toId);

            if (ContainsKey(key)) return this[key];
            long neuronId = Population.NextGeneId();
            Innovation innovation = new Innovation
            {
                InnovationId = Population.NextInnovationId(),
                NeuronId = neuronId
            };
            this[key] = innovation;
            FindOrCreateInnovation(fromId, neuronId);
            FindOrCreateInnovation(neuronId, toId);
            return innovation;
        }

        public Innovation FindOrCreateInnovation(long neuronId)
        {
            string key = ProduceKeyNeuron(neuronId);
            if (ContainsKey(key)) return this[key];
            Innovation innovation = new Innovation
            {
                InnovationId = Population.NextInnovationId(),
                NeuronId = neuronId
            };
            this[key] = innovation;
            return innovation;
        }

        public Innovation FindOrCreateInnovation(long fromId, long toId)
        {
            string key = ProduceKeyLink(fromId, toId);
            if (ContainsKey(key)) return this[key];
            Innovation innovation = new Innovation
            {
                InnovationId = Population.NextInnovationId(),
                NeuronId = -1
            };
            this[key] = innovation;
            return innovation;
        }
    }
}