﻿namespace Neat.Innovation
{
    public class Innovation
    {
        public long NeuronId { get; set; }
        public long InnovationId { get; set; }
        public override string ToString() => $"[Innovation: id={InnovationId}, neuron={NeuronId}]";
    }
}