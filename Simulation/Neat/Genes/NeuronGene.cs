﻿using Neat.ActivationFunctions;

namespace Neat.Genes
{
    public enum NeuronType { Bias, Hidden, Input, Output, None, Unkown }
    public class NeuronGene : BaseGene
    {
        public NeuronType NeuronType { get; set; }
        public IActivationFunction ActivationFunction { get; set; }

        public override string ToString() => $"[NeuronGene: id={Id}, type={NeuronType}]";
    }
}