﻿namespace Neat.Genes
{
    public class LinkGene : BaseGene
    {
        public long ToNeuronId { get; set; }
        public long FromNeuronId { get; set; }
        public double Weight { get; set; }
        public bool Enabled { get; set; }

        public override string ToString() => $"[LinkGene: innov={InnovationId}, enabled={Enabled}, from={FromNeuronId}, to={ToNeuronId}]";
    }
}