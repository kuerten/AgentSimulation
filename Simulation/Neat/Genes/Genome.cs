﻿using System;
using System.Collections.Generic;
using System.Linq;
using Neat.ActivationFunctions;
using Neat.Species;

namespace Neat.Genes
{
    public class Genome
    {
        public const double WeightRange = 5;
        public double Score { get; set; }
        public double AdjustedScore { get; set; } = 0;
        public int BirthGeneration { get; set; }
        public int Size { get; set; }
        public BasicSpecies Species { get; set; }

        public int InputCount { get; set; }
        public int OutputCount { get; set; }
        public List<LinkGene> Links { get; set; } = new List<LinkGene>();
        public List<NeuronGene> Neurons { get; set; } = new List<NeuronGene>();
        public Population Population { get; set; }
        public static Genome Random(Random rng, int inputCount, int outputCount, double connectionDensity)
        {
            IActivationFunction af = new ActivationSigmoid();
            int innovationId = 0;
            List<NeuronGene> neurons = new List<NeuronGene>();
            
            for (int i = 0; i < inputCount; i++)
            {
                neurons.Add(new NeuronGene()
                {
                    NeuronType = NeuronType.Input,
                    ActivationFunction = af,
                    Id = i,
                    InnovationId = innovationId++
                });
            }
            for (int i = 0; i < outputCount; i++)
            {
                neurons.Add(new NeuronGene()
                {
                    NeuronType = NeuronType.Output,
                    ActivationFunction = af,
                    Id = i + inputCount + 1,
                    InnovationId = innovationId++
                });
            }
            List<LinkGene> links = new List<LinkGene>();
            for (int i = 0; i < inputCount; i++)
            {
                for (int j = 0; j < outputCount; j++)
                {
                    if (links.Count < 1 || rng.NextDouble() < connectionDensity)
                    {
                        links.Add(new LinkGene
                        {
                            FromNeuronId = neurons[i].Id,
                            ToNeuronId = neurons[j].Id,
                            Weight = WeightRange * 2 * rng.NextDouble() - WeightRange,
                            Enabled = true,
                            InnovationId = innovationId++
                        });
                    }
                }
            }
            Genome g = new Genome()
            {
                InputCount = inputCount,
                OutputCount = outputCount,
                Neurons = neurons,
                Links = links
            };
            return g;
        }
        public void SortGenes() => Links.Sort();
        public NeuronGene FindNeuron(long id) => Neurons.FirstOrDefault(g => g.Id == id);

        public void CreateLink(long neuronFromId, long neuronToId, double weight)
        {
            LinkGene link = Links.Find(l => l.FromNeuronId == neuronFromId && l.ToNeuronId == neuronToId);
            if (!link.Equals(default(LinkGene))) // Link was previously disabled 
            {
                link.Enabled = true;
                link.Weight = weight;
                return;
            }
            // Create new Link
            Links.Add(new LinkGene
            {
                FromNeuronId = neuronFromId,
                ToNeuronId = neuronToId,
                Enabled = true,
                InnovationId = Population.Innovations.FindOrCreateInnovation(neuronFromId, neuronToId).InnovationId,
                Weight = weight
            });
        }
    }
}