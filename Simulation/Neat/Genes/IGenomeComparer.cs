﻿using System.Collections.Generic;

namespace Neat.Genes
{
    public interface IGenomeComparer : IComparer<Genome>
    {
        bool ShouldMinimize { get; }
        double ApplyBonus(double value, double bonus);
        double ApplyPenalty(double value, double bonus);
        bool IsBetterThan(double d1, double d2);
        bool IsBetterTHan(Genome g1, Genome g2);
    }
}