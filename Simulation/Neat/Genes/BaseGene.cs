﻿using System;

namespace Neat.Genes
{
    public abstract class BaseGene : IComparable<BaseGene>
    {
        public long Id { get; set; } = -1;
        public long InnovationId { get; set; } = -1;
        public int CompareTo(BaseGene other) => (int)(InnovationId - other.InnovationId);
    }
}