﻿using System;
using System.Collections.Generic;
using System.Linq;
using Neat.ActivationFunctions;
using Neat.Genes;
using Neat.Network;

namespace Neat
{
    public static class NetworkGenerator
    {
        public static Network.NeatBrain Generate(Genome genome)
        {
            IActivationFunction af = genome.Neurons.First().ActivationFunction;
            Neuron[] neurons = genome.Neurons.Select(n => new Neuron
            {
                NeuronId = n.Id,
                Type = n.NeuronType,
                Value = 0.0
            }).ToArray();
            Array.Sort( neurons );
            long activationId = 0;
            List<Neuron> oldNeurons = neurons.Take(genome.InputCount).ToList();
            List<Neuron> newNeurons = new List<Neuron>();
            foreach (Neuron neuron in oldNeurons) neuron.ActivationId = activationId++;
            List<LinkGene> links = new List<LinkGene>(genome.Links.Where( g => g.Enabled ));
            while (oldNeurons.Any())
            {
                foreach (Neuron neuron in oldNeurons)
                {
                    foreach (LinkGene link in links.Where(l => l.FromNeuronId == neuron.NeuronId).ToArray())
                    {
                        Neuron toNeuron = neurons.First(n => n.NeuronId == link.ToNeuronId);
                        neuron.Links.Add( toNeuron, link.Weight );
                        links.Remove(link);
                        if( toNeuron == neuron ) continue;
                        toNeuron.ActivationId = activationId++;
                        newNeurons.Add(toNeuron);
                    }
                }
                oldNeurons = newNeurons;
                newNeurons = new List<Neuron>();
            }
            if( links.Any() ) Console.WriteLine($"Warning {links.Count} Links are unsued in this network");
            return new Network.NeatBrain
            {
                InputCount = genome.InputCount,
                OutputCount = genome.OutputCount,
                ActivationFunction = af,
                Neurons = neurons
            };
        }
    }
}