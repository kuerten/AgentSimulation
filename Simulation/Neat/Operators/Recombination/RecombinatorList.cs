﻿using System;
using System.Linq;
using Neat.Genes;
using Neat.Operators.Selection;

namespace Neat.Operators.Recombination
{
    public class RecombinatorList : SelectorList<IRecombinator>, IRecombinator
    {
        public int OffspringProduced => this.Select(h => h.Key.OffspringProduced).Concat(new[] { 0 }).Max();
        public int ParentsNeeded => this.Select(h => h.Key.OffspringProduced).Concat(new[] { int.MinValue }).Max();
        public Genome[] PerformRecombination(Random rng, Genome[] parents) => PickMaxParents(rng, parents.Length)?.PerformRecombination(rng, parents) ?? new Genome[0];

        public IRecombinator PickMaxParents(Random rng, int maxParents)
        {
            double r = rng.NextDouble() * this.Where(h => h.Key.ParentsNeeded <= maxParents).Sum(h => h.Value);
            double current = 0;
            foreach ((IRecombinator key, double value) in this)
            {
                if (key.ParentsNeeded > maxParents) continue;
                current += value;
                if (r < current) return key;
            }
            return null;
        }
    }
}