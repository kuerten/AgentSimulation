﻿using System;
using Neat.Genes;

namespace Neat.Operators.Recombination
{
    public interface IRecombinator
    {
        int OffspringProduced { get; }
        int ParentsNeeded { get; }
        Genome[] PerformRecombination(Random rng, Genome[] parents);
    }
}