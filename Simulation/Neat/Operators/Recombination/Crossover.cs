﻿using System;
using System.Collections.Generic;
using System.Linq;
using Neat.Genes;

namespace Neat.Operators.Recombination
{
    public class Crossover : IRecombinator
    {
        public int OffspringProduced => 1;
        public int ParentsNeeded => 2;

        public Genome[] PerformRecombination(Random rng, Genome[] parents)
        {
            Genome mom = parents[0];
            Genome dad = parents[1];
            Genome best = FavorParent(rng, mom, dad);
            Genome notBest = best == mom ? dad : mom;
            List<LinkGene> selectedLinks = new List<LinkGene>();
            List<NeuronGene> selectedNeurons = new List<NeuronGene>();
            int curMom = 0;
            int curDad = 0;
            LinkGene selectedGene = null;
            int alwaysCount = mom.InputCount + mom.OutputCount + 1;
            for (int i = 0; i < alwaysCount; i++) AddNeuronId(i, selectedNeurons, best, notBest);

            while (curMom < mom.Links.Count || curDad < dad.Links.Count)
            {
                LinkGene momGene = null;
                LinkGene dadGene = null;
                long momInnoId = -1;
                long dadInnoId = -1;
                if (curMom < mom.Links.Count)
                {
                    momGene = mom.Links[curMom];
                    momInnoId = momGene.InnovationId;
                }
                if (curDad < dad.Links.Count)
                {
                    dadGene = dad.Links[curDad];
                    dadInnoId = dadGene.InnovationId;
                }

                if (momGene == null && dadGene != null)
                {
                    if (best == dad) selectedGene = dadGene;
                    curDad++;
                }
                else if (dadGene == null && momGene != null)
                {
                    if (best == mom) selectedGene = momGene;
                    curMom++;
                }
                else if (momInnoId < dadInnoId)
                {
                    if (best == mom) selectedGene = momGene;
                    curMom++;
                }
                else if (dadInnoId < momInnoId)
                {
                    if (best == dad) selectedGene = dadGene;
                    curDad++;
                }
                else if (dadInnoId == momInnoId)
                {
                    selectedGene = rng.NextDouble() < 0.5 ? momGene : dadGene;
                    curMom++;
                    curDad++;
                }

                if (selectedGene == null) continue;
                if (selectedLinks.Count == 0 || selectedLinks[selectedLinks.Count - 1].InnovationId != selectedGene.InnovationId) selectedLinks.Add(selectedGene);
                AddNeuronId(selectedGene.FromNeuronId, selectedNeurons, best, notBest);
                AddNeuronId(selectedGene.ToNeuronId, selectedNeurons, best, notBest);
            }

            selectedNeurons.Sort();

            return new[]
            {
                new Genome
                {
                    InputCount = mom.InputCount,
                    OutputCount = mom.OutputCount,
                    Neurons = selectedNeurons,
                    Links = selectedLinks
                }
            };
        }

        public void AddNeuronId(long nodeId, List<NeuronGene> vec, Genome best, Genome notBest)
        {
            if (vec.Any(g => g.Id == nodeId)) return;
            vec.Add(FindBestNeuron(nodeId, best, notBest));
        }

        private static Genome FavorParent(Random rng, Genome mom, Genome dad)
        {
            if (!(Math.Abs(mom.Score - dad.Score) < 0.0000001)) return dad; // Temporary
            if (mom.Links.Count == dad.Links.Count) return rng.NextDouble() > 0.5 ? mom : dad;
            return mom.Links.Count < dad.Links.Count ? mom : dad;

            // ToDo: add Comparer
        }

        private static NeuronGene FindBestNeuron(long id, Genome best, Genome notBest) => best.FindNeuron(id) ?? notBest.FindNeuron(id);
    }
}