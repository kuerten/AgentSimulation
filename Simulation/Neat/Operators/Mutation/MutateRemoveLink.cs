﻿using System;
using System.Linq;
using Neat.Genes;

namespace Neat.Operators.Mutation
{
    public class MutateRemoveLink : IMutation
    {
        public const int MinLink = 5;
        public void PerformOperation(Random rng, Genome target)
        {
            if (target.Links.Count < MinLink) return;
            LinkGene targetGene = target.Links[rng.Next(target.Links.Count - 1)];
            target.Links.Remove(targetGene);
            if (!IsNeuronNeeded(target, targetGene.FromNeuronId)) RemoveNeuron(target, targetGene.FromNeuronId);
            if (!IsNeuronNeeded(target, targetGene.ToNeuronId)) RemoveNeuron(target, targetGene.ToNeuronId);
        }

        public void RemoveNeuron(Genome target, long neurondId) => target.Neurons.RemoveAll(g => g.Id == neurondId);

        public bool IsNeuronNeeded(Genome target, long neuronId) =>
            target.Neurons.Any(n =>
                n.Id == neuronId &&
                n.NeuronType == NeuronType.Bias ||
                n.NeuronType == NeuronType.Input ||
                n.NeuronType == NeuronType.Output) ||
            target.Links.Any(l =>
                l.FromNeuronId == neuronId ||
                l.ToNeuronId == neuronId);
    }
}