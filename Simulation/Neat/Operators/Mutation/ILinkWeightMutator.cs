﻿using System;
using Neat.Genes;

namespace Neat.Operators.Mutation
{
    public interface ILinkWeightMutator
    {
        void MutateWeight(Random rng, LinkGene gene, double weightRange);
    }
}