﻿using System;
using System.Linq;
using Neat.Genes;

namespace Neat.Operators.Mutation
{
    public class MutateAddLink : IMutation
    {
        public Population Owner { get; } // ToDo: Setvalue
        public void PerformOperation(Random rng, Genome target)
        {
            int countTryToAddLink = 10;
            long neuronFromId = -1;
            long neuronToId = -1;
            while (countTryToAddLink-- > 0)
            {
                NeuronGene fromNeuron = ChooseRandomNeuron(rng, target, true);
                NeuronGene toNeuron = ChooseRandomNeuron(rng, target, false);
                if (fromNeuron == null || toNeuron == null) return;
                if (IsDuplicateLink(target, fromNeuron.Id, toNeuron.Id) || Owner.ActivationCycles == 1 && fromNeuron.NeuronType == NeuronType.Output) continue;
                neuronFromId = fromNeuron.Id;
                neuronToId = toNeuron.Id;
                break;
            }

            if (neuronToId < 0 || neuronFromId < 0) return;
            double r = Owner.WeightRange;
            target.CreateLink(neuronFromId, neuronToId, rng.NextDouble() * 2 * r - r);
            target.SortGenes();
        }
        public bool IsDuplicateLink(Genome target, long fromNeuonId, long toNeuronId) => target.Links.Any(l => l.Enabled && l.FromNeuronId == fromNeuonId && l.ToNeuronId == toNeuronId);
        public NeuronGene ChooseRandomNeuron(Random rng, Genome target, bool choosingFrom)
        {
            int start = choosingFrom ? 0 : target.InputCount + 1;
            if (!choosingFrom)
            {
                int ac = target.Population.ActivationCycles;
                if (ac == 1) start += target.OutputCount;
            }
            int end = target.Neurons.Count - 1; // ToDo: check if -1, 0 or +1
            return start > end ? null : target.Neurons[rng.Next(start, end)];
        }
    }
}