﻿using System;
using Neat.Genes;
using Neat.Operators.Selection;

namespace Neat.Operators.Mutation
{
    public class CompoundMutation : SelectorList<IMutation>, IMutation
    {
        public void PerformOperation(Random rng, Genome target) => GetRandom().PerformOperation(rng, target);
    }
}