﻿using System;
using Neat.Genes;
using Neat.Operators.Selection;

namespace Neat.Operators.Mutation
{
    public class MutateWeights : IMutation
    {
        public Population Owner { get; } // ToDo: Setvalue
        public ILinkSelector LinkSelection { get; }
        public ILinkWeightMutator WeightMutation { get; }
        public MutateWeights(ILinkSelector linkSelection, ILinkWeightMutator weightMutation)
        {
            LinkSelection = linkSelection;
            WeightMutation = weightMutation;
        }
        public void PerformOperation(Random rng, Genome target)
        {
            double weightRange = Owner.WeightRange;
            foreach (LinkGene gene in LinkSelection.SelectLinks(rng,target))
            {
                WeightMutation.MutateWeight(rng, gene, weightRange);
            }
        }
    }
}