﻿using System;
using Neat.Genes;

namespace Neat.Operators.Mutation
{
    public class MutateAddNode : IMutation
    {
        public Population Owner { get; } // ToDo: Setvalue
        public void PerformOperation(Random rng, Genome target)
        {
            int countTrysToFindOldLink = 10;
            LinkGene splitLink = null;
            int upperLimit = target.Links.Count - 1;
            if (target.Links.Count < target.InputCount + target.OutputCount + 10) upperLimit -= (int)Math.Sqrt(target.Links.Count); // inputCount + outputCount + 10 => size_bias
            while (countTrysToFindOldLink-- > 0)
            {
                LinkGene link = target.Links[rng.Next(0, upperLimit)];
                if (!link.Enabled || target.Neurons[GetElementPos(target, link.FromNeuronId)].NeuronType == NeuronType.Bias) continue;
                splitLink = link;
                break;
            }

            if (splitLink == null) return;
            splitLink.Enabled = false;
            long from = splitLink.FromNeuronId;
            long to = splitLink.ToNeuronId;
            Innovation.Innovation innovation = Owner.Innovations.FindOrCreateInnovationSplit(from, to);
            target.Neurons.Add(new NeuronGene()
            {
                NeuronType = NeuronType.Hidden,
                ActivationFunction = Owner.ActivationFunctions.GetRandom(),
                InnovationId = innovation.InnovationId,
                Id = innovation.NeuronId
            });
            target.CreateLink(from, innovation.NeuronId, splitLink.Weight);
            target.CreateLink(innovation.NeuronId, to, Owner.WeightRange);
            target.SortGenes();
        }

        public int GetElementPos(Genome target, long neuronId) => target.Neurons.FindIndex(g => g.Id == neuronId);
    }
}