﻿using System;
using Neat.Genes;

namespace Neat.Operators.Mutation
{
    public interface IMutation
    {
        void PerformOperation(Random rng, Genome target);
    }
}