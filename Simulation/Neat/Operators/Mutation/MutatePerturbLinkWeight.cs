﻿using System;
using Neat.Genes;

namespace Neat.Operators.Mutation
{
    public class MutatePerturbLinkWeight : ILinkWeightMutator
    {
        private readonly double _sigma;
        public MutatePerturbLinkWeight(double sigma)
        {
            _sigma = sigma;
        }

        public void MutateWeight(Random rng, LinkGene gene, double weightRange) => gene.Weight = (gene.Weight + rng.NextGaussian() * _sigma).Clamp(weightRange);
    }
}