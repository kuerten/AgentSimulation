﻿using System;
using Neat.Genes;

namespace Neat.Operators.Mutation
{
    public class MutateResetLinkWeight : ILinkWeightMutator
    {
        public void MutateWeight(Random rng, LinkGene gene, double weightRange) => gene.Weight = rng.NextDouble(-weightRange, weightRange);
    }
}