﻿using Neat.Genes;

namespace Neat.Operators.Rules
{
    public interface IRewriteRule
    {
        bool IsValid(Genome genome);
    }
}