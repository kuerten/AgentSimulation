﻿using System.Collections.Generic;
using Neat.Genes;

namespace Neat.Operators.Rules
{
    public interface IRuleHolder
    {
        IList<IConstraintRule> ConstraintRules { get; }
        IList<IRewriteRule> RewriteRules { get; }
        void AddRewriteRule(IRewriteRule rule);
        void AddCOnstraintRule(IConstraintRule rule);
        void Rewrite(Genome genome);
        bool IsValid(Genome genome);
    }
}