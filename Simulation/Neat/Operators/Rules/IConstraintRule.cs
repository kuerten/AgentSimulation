﻿using Neat.Genes;

namespace Neat.Operators.Rules
{
    public interface IConstraintRule
    {
        bool Rewrite(Genome genome);
    }
}