﻿using System;
using Neat.Genes;

namespace Neat.Operators
{
    public interface IEvolutionaryOperator
    {
        int OffspringProduced { get; }
        int ParentsNeeded { get; }
        void PerformOperation(Random rng, Genome[] parents, int parentIndex, Genome[] offspring, int offspringIndex);
        Genome[] PerformOperation(Random rng, Genome[] parents);
    }
}