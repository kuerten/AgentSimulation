﻿using System;
using System.Linq;
using Neat.Operators.Selection;

namespace Neat.Operators
{
    public class OperatorList : SelectorList<IEvolutionaryOperator>
    {
        public int MaxOffspring() => this.Select(h => h.Key.OffspringProduced).Concat(new[] {0}).Max();
        public int MaxParents() => this.Select(h => h.Key.OffspringProduced).Concat(new[] {int.MinValue}).Max();

        public IEvolutionaryOperator PickMaxParents(Random rng, int maxParents)
        {
            double r = rng.NextDouble() * this.Where(h => h.Key.ParentsNeeded <= maxParents).Sum(h => h.Value);
            double current = 0;
            foreach ((IEvolutionaryOperator key, double value) in this)
            {
                if (key.ParentsNeeded > maxParents) continue;
                current += value;
                if (r < current) return key;
            }
            return null;
        }
    }
}