﻿using System;
using Neat.Species;

namespace Neat.Operators.Selection
{
    public class TruncationSelection : ISelectionOperator
    {
        public double Percent { get; }

        public TruncationSelection(double percent)
        {
            Percent = percent;
        }
        public int PerformSelection(Random rng, BasicSpecies species) => rng.Next(Math.Max((int)(species.Count * Percent), 1));
    }

    public interface ISelectionOperator
    {
        int PerformSelection(Random rng, BasicSpecies species);
    }
}