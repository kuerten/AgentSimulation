﻿using System;
using System.Collections.Generic;
using Neat.Genes;

namespace Neat.Operators.Selection
{
    public class SelectProportion : ILinkSelector
    {
        private readonly double _proportion;
        public SelectProportion(double proprotion)
        {
            _proportion = proprotion;
        }


        public IEnumerable<LinkGene> SelectLinks(Random rng, Genome genome)
        {
            IList<LinkGene> result = new List<LinkGene>();
            bool mutated = false;
            foreach (LinkGene link in genome.Links)
            {
                if (!(rng.NextDouble() < _proportion)) continue;
                mutated = true;
                result.Add(link);
            }

            if (!mutated) result.Add( genome.Links[rng.Next(genome.Links.Count)] );
            return result;
        }
    }
}