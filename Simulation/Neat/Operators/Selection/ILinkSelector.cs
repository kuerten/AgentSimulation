﻿using System;
using System.Collections.Generic;
using Neat.Genes;

namespace Neat.Operators.Selection
{
    public interface ILinkSelector
    {
        IEnumerable<LinkGene> SelectLinks(Random rng, Genome genome);
    }
}