﻿using System;
using System.Collections.Generic;
using Neat.Genes;

namespace Neat.Operators.Selection
{
    public class SelectFixed : ILinkSelector
    {
        private readonly int _linkCount;
        public SelectFixed(int linkCount)
        {
            _linkCount = linkCount;
        }

        public IEnumerable<LinkGene> SelectLinks(Random rng, Genome genome)
        {
            IList<LinkGene> result = new List<LinkGene>();
            int cnt = Math.Min(_linkCount, genome.Links.Count);
            while (result.Count < cnt)
            {
                LinkGene link = genome.Links[rng.Next(genome.Links.Count)];
                if( !result.Contains(link) ) result.Add(link);
            }
            return result;
        }
    }
}