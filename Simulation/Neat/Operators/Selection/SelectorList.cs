﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Neat.Operators.Selection
{
    public class SelectorList<T> : Dictionary<T,double>
    {
        private readonly Random _rng = new Random();

        public T GetRandom()
        {
            double r = _rng.NextDouble();
            double sum = 0;
            for (int i = 0; i < Count; i++)
            {
                sum += this[Keys.ElementAt(i)];
                if (r < sum) return Keys.ElementAt(i);
            }

            for (int i = 0; i < Count; i++)
            {
                if (Math.Abs(this[Keys.ElementAt(i)]) > 0) return Keys.ElementAt(i);
            }

            throw  new Exception("Invalid probabilities");
        }

        public T GetRandom(int skip)
        {
            double totalProb = 1 - this[Keys.ElementAt(skip)];
            double value = _rng.NextDouble() * totalProb;
            double accumulator = 0;
            for (int i = 0; i < Count; i++)
            {
                if( i == skip ) continue;
                accumulator += this[Keys.ElementAt(i)];
                if (accumulator > value) return Keys.ElementAt(i);
            }
            for (int i = 0; i < Count; i++)
            {
                if( i == skip ) continue;
                if (Math.Abs(this[Keys.ElementAt(i)]) > 0) return Keys.ElementAt(i);
            }

            throw new Exception("Invalid probabilities");
        }

    }
}