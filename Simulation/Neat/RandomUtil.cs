﻿using System;

namespace Neat
{
    public static class RandomUtil
    {
        public static double NextGaussian(this Random rng) // Marsaglia polar method
        {
            double v1, v2, s;
            do
            {
                v1 = 2 * rng.NextDouble() - 1;
                v2 = 2 * rng.NextDouble() - 1;
                s = v1 * v1 + v2 * v2;
            } while (s >= 1 || s == 0.0);

            s = Math.Sqrt((-2.0f * Math.Log(s)) / s);
            return v1 * s;
        }

        public static double NextDouble(this Random rng, double max) => rng.NextDouble() * max;
        public static double NextDouble(this Random rng, double min, double max) => rng.NextDouble() * (max - min) + min;
    }
}