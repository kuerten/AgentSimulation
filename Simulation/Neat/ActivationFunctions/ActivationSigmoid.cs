﻿using System;
using System.Collections.Generic;

namespace Neat.ActivationFunctions
{
    public class ActivationSigmoid : IActivationFunction
    {
        private readonly Dictionary<string, int> _parameter = new Dictionary<string, int>();

        public IReadOnlyDictionary<string, int> Parameter => _parameter;
        public double Execute(double data) => 1.0 / (1.0 + Math.Exp(-4.9 * data));
    }
}