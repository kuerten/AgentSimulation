﻿using System;
using System.Collections.Generic;

namespace Neat.ActivationFunctions
{
    public class ActivationLog : IActivationFunction
    {
        private readonly Dictionary<string, int> _parameter = new Dictionary<string, int>();

        public IReadOnlyDictionary<string, int> Parameter => _parameter;

        public double Execute(double data) => data >= 0 ? Math.Log(1 + data) : Math.Log(1 - data);
    }
}