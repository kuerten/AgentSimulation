﻿using System.Collections.Generic;

namespace Neat.ActivationFunctions
{
    public interface IActivationFunction
    {
        IReadOnlyDictionary<string, int> Parameter { get; }
        double Execute(double input);
    }
}