﻿using System.Collections.Generic;

namespace Neat.ActivationFunctions
{
    public class ActivationLinear : IActivationFunction
    {
        private readonly Dictionary<string, int> _parameter = new Dictionary<string, int>();

        public IReadOnlyDictionary<string, int> Parameter => _parameter;

        public double Execute(double input)
        {
            return input;
        }
    }
}