﻿using System.Collections.Generic;
using System.Linq;
using Neat.ActivationFunctions;

namespace Neat.Network
{
    /// <summary>
    /// Statefull NEAT-Neural-Network, each Cycle is computed by taking the last Cycle(s) into consideration
    /// </summary>
    public class NeatBrain
    {
        public int InputCount { get; set; }
        public int OutputCount { get; set; }
        public IEnumerable<Neuron> Inputs => Neurons.Take(InputCount);
        public IEnumerable<Neuron> Outputs => Neurons.Skip(InputCount).Take(OutputCount);
        public Neuron[] Neurons { get; set; } = new Neuron[0];
        //public int ActivationCycles { get; set; } = 1;
        public IActivationFunction ActivationFunction { get; set; }

        public void Compute(double[] input)
        {
            for (int i = 0; i < InputCount; i++) Neurons[i].Value = ActivationFunction.Execute(Neurons[i].Value + input[i]);
            List<Neuron> pendingNeurons = new List<Neuron>(Inputs);
            long currentId = -1;
            while (pendingNeurons.Any())
            {
                foreach (Neuron fromNeuron in pendingNeurons)
                {
                    pendingNeurons.Remove(fromNeuron);
                    foreach ((Neuron toNeuron, double weight) in fromNeuron.Links)
                    {
                        toNeuron.Value = ActivationFunction.Execute(toNeuron.Value + fromNeuron.Value * weight);
                        if (toNeuron.ActivationId <= currentId) continue; // Neuron has already been computed in the past
                        pendingNeurons.Add(toNeuron);
                        currentId = toNeuron.ActivationId;
                    }
                }
            }

        }
    }


}
