﻿namespace Neat.Network
{
    public class Link
    {
        public Neuron FromNeuron { get; set; }
        public Neuron ToNeuron { get; set; }
        public double Weight { get; set; }
        
        public Link(Neuron fromNeuron, Neuron toNeuron, double weight)
        {
            FromNeuron = fromNeuron;
            ToNeuron = toNeuron;
            Weight = weight;
        }
        public override string ToString() => $"[Link: fromNeuron={FromNeuron}, toNeuron={ToNeuron}]";
    }
}