﻿using System.Collections.Generic;
using Neat.Genes;

namespace Neat.Network
{
    public class Neuron
    {
        public NeuronType Type { get; set; }
        public long ActivationId { get; set; } // Determines activation order
        public long NeuronId { get; set; }
        public double Value { get; set; } = 0;
        public Dictionary<Neuron, double> Links { get; } = new Dictionary<Neuron, double>(); // uni-directional links
    }
}