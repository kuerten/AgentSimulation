﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace EntitySimulation.Objects.Interpolation
{
    [DataContract]
    public abstract class InterpolationStrategyBase<T> : IInterpolationStrategy
    {
        public abstract T Interpolate(IEnumerable<T> values);
        public object Interpolate(IEnumerable<object> values) => Interpolate(values.Cast<T>());
    }
}