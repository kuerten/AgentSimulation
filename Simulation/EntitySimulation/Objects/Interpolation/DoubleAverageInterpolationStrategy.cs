﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace EntitySimulation.Objects.Interpolation
{
    [DisplayName("Average")]
    public class DoubleAverageInterpolationStrategy : InterpolationStrategyBase<double>
    {
        public override double Interpolate(IEnumerable<double> values) => values.Average();
    }
}