﻿using System.Collections.Generic;
using System.Linq;

namespace EntitySimulation.Objects.Interpolation
{
    public class FloatAverageInterpolationStrategy : InterpolationStrategyBase<float>
    {
        public override float Interpolate(IEnumerable<float> values) => values.Average();
    }
}