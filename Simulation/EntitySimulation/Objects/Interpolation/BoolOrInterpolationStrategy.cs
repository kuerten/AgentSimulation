﻿using System.Collections.Generic;
using System.Linq;

namespace EntitySimulation.Objects.Interpolation
{
    public class BoolOrInterpolationStrategy : InterpolationStrategyBase<bool>
    {
        public override bool Interpolate(IEnumerable<bool> values) => values.Any();
    }
}