﻿using System.Collections.Generic;

namespace EntitySimulation.Objects.Interpolation
{
    public interface IInterpolationStrategy
    {
        object Interpolate(IEnumerable<object> values);
    }
}