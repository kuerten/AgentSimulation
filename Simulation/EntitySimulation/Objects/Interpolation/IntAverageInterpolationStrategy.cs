﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EntitySimulation.Objects.Interpolation
{
    public class IntAverageInterpolationStrategy : InterpolationStrategyBase<int>
    {
        public override int Interpolate(IEnumerable<int> values) => (int)Math.Round(values.Average());
    }
}