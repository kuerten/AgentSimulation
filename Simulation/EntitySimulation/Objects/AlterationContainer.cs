﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using EntitySimulation.Objects.Interpolation;
using Newtonsoft.Json;

namespace EntitySimulation.Objects
{
    public interface IAlterationContainer : ICloneable
    {
        int Capacity { get; set; }
        int Changes { get; }
        IInterpolationStrategy Strategy { get; set; }
        object Value { get; set; }
        void AddChange(object value);
        bool Collect();
        void Trim();
    }

    public class AlterationContainer<T> : IAlterationContainer
    {
        #region Fields

        private T[] _changes;

        #endregion

        #region Constructors

        public AlterationContainer()
        {
            _changes = new T[10];
        }

        public AlterationContainer(T initialValue) : this()
        {
            Value = initialValue;
        }

        public AlterationContainer(AlterationContainer<T> other)
        {
            _changes = new T[other._changes.Length];
            Strategy = other.Strategy;
            Value = other.Value;
        }

        #endregion

        #region Properties

        public int Capacity
        {
            get => _changes.Length;
            set
            {
                if (_changes.Length == value) return;
                if (Changes > value) throw new ArgumentException("Capacity must be larger than current size");
                T[] tmp = new T[value];
                Array.Copy(_changes, tmp, Changes);
                _changes = tmp;
            }
        }
        [JsonIgnore]
        public int Changes { get; private set; }

        [JsonIgnore]
        IInterpolationStrategy IAlterationContainer.Strategy
        {
            get => Strategy;
            set => Strategy = (InterpolationStrategyBase<T>)value;
        }

        public InterpolationStrategyBase<T> Strategy { get; set; }

        public T Value { get; set; }
        [JsonIgnore]
        object IAlterationContainer.Value
        {
            get => Value;
            set => Value = (T)value;
        }

        #endregion

        #region Methods

        public void AddChange(T value)
        {
            lock (_changes)
            {
                if (Changes >= _changes.Length)
                {
                    T[] tmp = new T[_changes.Length * 2];
                    Array.Copy(_changes, tmp, _changes.Length);
                    _changes = tmp;
                }
                _changes[Changes++] = value;
            }
        }

        public void AddChange(object value) => AddChange((T) value);

        public bool Collect()
        {
            if (Changes == 0) return false;
            T result;
            if (Strategy == null)
            {
                if (Changes != 1) throw new InvalidOperationException("Multiple alterations of a value require a interpolation Strategy");
                result = _changes[0];
            }
            else
            {
                result = Strategy.Interpolate(_changes.Take(Changes));
            }
            Changes = 0;
            Value = result;
            return true;
        }

        public void Trim()
        {
            T[] tmp = new T[Changes];
            Array.Copy(_changes, tmp, tmp.Length);
            _changes = tmp;
        }

        public AlterationContainer<T> Clone()
        {
            T value;
            if (Value is ICloneable c) value = (T)c.Clone();
            else if (Value.GetType().IsValueType) value = Value;
            else if (Value.GetType().IsSerializable) value = SerializedClone(Value);
            else throw new Exception($"Value cannot be copied: {Value.GetType()}");
            return new AlterationContainer<T>(value)
            {
                Strategy = Strategy,
                Capacity = Capacity
            };
        }
        object ICloneable.Clone() => Clone();

        private static TOut SerializedClone<TOut>(TOut original)
        {
            using MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, original);
            stream.Position = 0;
            return (TOut)formatter.Deserialize(stream);
        }

        #endregion
    }
}