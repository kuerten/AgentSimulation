﻿using EntitySimulation.Objects.Interpolation;
using Utility;

namespace EntitySimulation.Objects
{
    public static class ObjectHelper
    {
        public static readonly VariableDefinition<int> Type = new VariableDefinition<int>(0);
        public static readonly VariableDefinition<int> Id = new VariableDefinition<int>(0);
        /// <summary>
        /// In Radians.
        /// </summary>
        public static readonly VariableDefinition<double> Rotation = new VariableDefinition<double>(0.0);
        public static readonly VariableDefinition<double> RotationVelocity = new VariableDefinition<double>(0.0);
        public static readonly VariableDefinition<double> Velocity = new VariableDefinition<double>(0.0);
        public static readonly VariableDefinition<Vector2> Position = new VariableDefinition<Vector2>(new Vector2());
        public static readonly VariableDefinition<double> Energy = new VariableDefinition<double>(0.0) { InterpolationStrategy = new DoubleAverageInterpolationStrategy() };
        public static readonly VariableDefinition<bool> Consumed = new VariableDefinition<bool>(false) {InterpolationStrategy = new BoolOrInterpolationStrategy()};

    }
}