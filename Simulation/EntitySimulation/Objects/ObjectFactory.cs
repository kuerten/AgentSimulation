﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using EntitySimulation.Components;

namespace EntitySimulation.Objects
{
    public static class ObjectFactory
    {
        #region Constants

        private static readonly Dictionary<int, ObjectDefinition> EntityTypes = new Dictionary<int, ObjectDefinition>();
        private static readonly Dictionary<string, object> GlobaldData = new Dictionary<string, object>();

        #endregion

        #region Methods

        public static void RegisterType(int id, bool overrideExisting = false)
        {
            if (EntityTypes.ContainsKey(id) && !overrideExisting) throw new Exception("Type alread registered");
            EntityTypes[id] = new ObjectDefinition();
        }

        public static int CreateNewType()
        {
            int i = 0;
            while (EntityTypes.ContainsKey(i)) i++;
            EntityTypes.Add(i, new ObjectDefinition());
            return i;
        }
        public static int GetType(string name)
        {
            foreach ((int key, ObjectDefinition value) in EntityTypes)
            {
                if (string.Equals(value.Name, name)) return key;
            }
            return -1;
        }

        public static int[] GetAllTypes() => EntityTypes.Keys.ToArray();
        public static void ClearTypes() => EntityTypes.Clear();

        public static void AddComponent(int id, IComponent component) => AddComponent(id, component, EntityTypes[id].Components.Select(e => e.Priority).DefaultIfEmpty(-1).Max() + 1);
        public static void AddComponent(int id, IComponent component, int priority) => AddComponent(id, new ComponentEntry(priority, component));
        public static void AddComponent(int id, ComponentEntry entry)
        {
            ObjectDefinition type = EntityTypes[id];
            if (type.Components.Any(e => e.Component == entry.Component)) throw new ArgumentException("Already exists", nameof(entry));
            for (int i = 0; i < type.Components.Count; i++)
            {
                if (type.Components[i].Priority < entry.Priority) continue;
                type.Components.Insert(i, entry);
                return;
            }
            type.Components.Add(entry);
        }

        public static void SetInitialState(int id, ObjectState state) => EntityTypes[id].InitialState = state;
        public static ObjectState GetInitialState(int id) => EntityTypes[id].InitialState;
        public static void SetName(int id, string name) => EntityTypes[id].Name = name;
        public static string GetName(int id) => EntityTypes[id].Name;

        public static ComponentEntry[] GetComponents(int id) => EntityTypes[id].Components.ToArray();

        public static ObjectState GenerateEntity(int id, int seed)
        {
            ObjectState state = EntityTypes[id].InitialState?.Clone();
            ObjectHelper.Id.Set(state,EntityTypes[id].NextId());
            InitializeRng(state, seed);
            return state;
        }

        public static ObjectState GenerateInitialState(int typeId)
        {
            ObjectState state = new ObjectState();
            ObjectHelper.Type.Add(state, typeId);
            ObjectHelper.Id.Add(state);
            foreach (ComponentEntry component in EntityTypes[typeId].Components)
            {
                foreach (IVariableDefinition definition in GetVariables(component.Component))
                {
                    state.AddDefinition(definition);
                    //state.AddDefinition(definition.Name, definition.InitialValue, definition.InterpolationStrategy);
                }
            }
            return state;
        }

        public static IEnumerable<IVariableDefinition> GetVariables(IComponent component) =>
            component.GetType().GetFields(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public)
                .Where(x => typeof(IVariableDefinition).IsAssignableFrom(x.FieldType))
                .Select(x => x.GetValue(component))
                .Cast<IVariableDefinition>();

        public static void Update(TimeSpan delta, ObjectState @object)
        {
            int id = ObjectHelper.Type.Get(@object);
            foreach (ComponentEntry entry in EntityTypes[id].Components) entry.Component.Tick(delta, @object);
        }

        public static void SetGlobal<T>(string key, T data) => GlobaldData[key] = data;
        public static T GetGlobal<T>(string name, T defaultValue = default) => GlobaldData.TryGetValue(name, out object propertyValue) ? (T)propertyValue : defaultValue;
        public static bool RemoveGlobal(string name) => GlobaldData.Remove(name);
        public static void ClearGlobal() => GlobaldData.Clear();

        public static void InitializeRng(ObjectState state, int seed)
        {
            int type = ObjectHelper.Type.Get(state);
            int id = ObjectHelper.Id.Get(state);
            seed = seed ^ type ^ id;
            foreach (ComponentEntry entry in EntityTypes[id].Components)
            {
                int s = seed ^ entry.Component.Identifier;

                foreach (IVariableDefinition definition in GetVariables(entry.Component).Where(x => x.Type == typeof(Utility.Random)))
                {
                    state.Set(definition.Name, new Utility.Random(s));
                }
                
            }
        }

        public static void InitializeComponents()
        {
            foreach ((int _, ObjectDefinition value) in EntityTypes)
            {
                foreach (ComponentEntry component in value.Components)
                {
                    component.Component.Initialize();
                }
            }
        }

        #endregion

        private class ObjectDefinition
        {
            #region Fields

            public readonly List<ComponentEntry> Components = new List<ComponentEntry>();
            private int _idCounter;

            #endregion

            #region Properties

            public string Name { get; set; }
            public ObjectState InitialState { get; set; }

            #endregion

            #region Methods

            public int NextId() => _idCounter++;
            public override string ToString() => string.IsNullOrEmpty(Name) ? base.ToString() : Name;

            #endregion
        }

        public struct ComponentEntry
        {
            #region Constructors

            public ComponentEntry(int priority, IComponent component)
            {
                Priority = priority;
                Component = component;
            }

            #endregion

            #region Properties

            public int Priority { get; set; }
            public IComponent Component { get; set; }

            #endregion
        }
    }
}