﻿using System;
using EntitySimulation.Objects.Interpolation;

namespace EntitySimulation.Objects
{
    public interface IVariableDefinition
    {
        object InitialValue { get; }
        Type Type { get; }
        string Name { get; }
        IInterpolationStrategy InterpolationStrategy { get; set; }

        IAlterationContainer CreateContainer();
    }
}