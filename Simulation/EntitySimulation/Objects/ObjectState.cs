﻿using EntitySimulation.Objects.Interpolation;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace EntitySimulation.Objects
{
    public class ObjectState : ICloneable
    {
        #region Fields

        private readonly ConcurrentDictionary<string, IAlterationContainer> _properties;

        #endregion

        #region Constructors

        public ObjectState() : this(new Dictionary<string, IAlterationContainer>()) { }

        public ObjectState(IDictionary<string, IAlterationContainer> propertyDictionary)
        {
            _properties = new ConcurrentDictionary<string, IAlterationContainer>(propertyDictionary);
        }

        #endregion

        #region Properties

        public bool IsDirty { get; private set; } = true;

        public object this[string name]
        {
            get => Get<object>(name);
            set => Set(name, value);
        }

        #endregion

        #region Methods

        public void Import(Dictionary<string, object> propertiesToImport)
        {
            foreach ((string key, object value) in propertiesToImport) Set(key, value);
        }

        public Dictionary<string, IAlterationContainer> Export() => _properties.ToDictionary(x => x.Key, x => x.Value);

        public bool IsAvailable(string name) => _properties.ContainsKey(name);

        public bool RetrieveDirty()
        {
            if (!IsDirty) return false;
            IsDirty = false;
            return true;
        }

        public string[] GetAllNames() => _properties.Keys.ToArray();

        public TValue Get<TValue>(string name, TValue defaultValue = default) => 
            _properties.TryGetValue(name, out IAlterationContainer propertyValue) ? ((AlterationContainer<TValue>)propertyValue).Value : defaultValue;
        public object Get(string name, object defaultValue = default) => 
            _properties.TryGetValue(name, out IAlterationContainer propertyValue) ? propertyValue.Value : defaultValue;

        public void Set<TValue>(string name, TValue value)
        {
            IAlterationContainer container;
            lock (_properties)
            {
                if(!_properties.TryGetValue(name, out container)) container = new AlterationContainer<TValue>();
            }
            container.AddChange(value);
        }

        public void Update<TValue>(string propertyName, Func<TValue, TValue> update)
        {
            if (!_properties.TryGetValue(propertyName, out IAlterationContainer propertyValue)) return;
            TValue value = ((AlterationContainer<TValue>)propertyValue).Value;
            TValue updatedValue = update(value);
            Set(propertyName, updatedValue);
        }

        public void TransferData()
        {
            foreach ((string _, IAlterationContainer container) in _properties.Where(x => x.Value.Changes > 0))
            {
                IsDirty |= container.Collect();
            }
        }

        public void AddContainer(string name, IAlterationContainer container)
        {
            _properties[name] = container;
        }
        public void AddDefinition(IVariableDefinition definition)
        {
            _properties[definition.Name] = definition.CreateContainer();
        }

        public ObjectState Clone()
        {
            ObjectState newState = new ObjectState();
            foreach ((string key, IAlterationContainer container) in _properties)
            {
                newState._properties[key] = (IAlterationContainer)container.Clone();
            }
            return newState;
        }

        public override string ToString() => $"Type: {ObjectHelper.Type.Get(this)}, Id: {ObjectHelper.Id.Get(this)}";

        object ICloneable.Clone() => Clone();
     
        #endregion

    }
}