﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using EntitySimulation.Objects.Interpolation;

namespace EntitySimulation.Objects
{
    [DataContract]
    public class VariableDefinition<T> : IVariableDefinition, IEquatable<VariableDefinition<T>>
    {
        #region Constructors

        public VariableDefinition(T initialValue, string prefix, [CallerMemberName] string name = "")
        {
            Name = string.Join('.', prefix, name);
            InitialValue = initialValue;
        }

        public VariableDefinition(T initialValue, [CallerMemberName] string name = "")
        {
            Name = name;
            InitialValue = initialValue;
        }

        #endregion

        #region Properties

        [DataMember] public T InitialValue { get; private set; }
        public Type Type { get; } = typeof(T);
        [DataMember] public string Name { get; private set; }
        [DataMember] public InterpolationStrategyBase<T> InterpolationStrategy { get; set; }

        object IVariableDefinition.InitialValue => InitialValue;

        IInterpolationStrategy IVariableDefinition.InterpolationStrategy
        {
            get => InterpolationStrategy;
            set => InterpolationStrategy = (InterpolationStrategyBase<T>) value;
        }

        #endregion

        #region Methods

        public AlterationContainer<T> CreateContainer(T value) => new AlterationContainer<T>(value)
        {
            Strategy = InterpolationStrategy
        };
        public AlterationContainer<T> CreateContainer() => new AlterationContainer<T>(InitialValue)
        {
            Strategy = InterpolationStrategy
        };

        IAlterationContainer IVariableDefinition.CreateContainer() => CreateContainer();

        public T Get(ObjectState state) => state.Get<T>(Name);
        public T Get(ObjectState state, T defaultValue) => state.Get(Name, defaultValue);
        public void Set(ObjectState state, T value) => state.Set(Name, value);

        public void Add(ObjectState state) => state.AddContainer(Name, CreateContainer());
        public void Add(ObjectState state, T value) => state.AddContainer(Name, CreateContainer(value));

        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((VariableDefinition<T>) obj);
        }

        public bool Equals(VariableDefinition<T> other)
        {
            if (other is null) return false;
            return ReferenceEquals(this, other) || string.Equals(Name, other.Name);
        }

        public override int GetHashCode() => Name != null ? Name.GetHashCode() : 0;

        

        public static bool operator ==(VariableDefinition<T> left, VariableDefinition<T> right) => Equals(left, right);

        public static bool operator !=(VariableDefinition<T> left, VariableDefinition<T> right) => !Equals(left, right);

        #endregion
    }
}