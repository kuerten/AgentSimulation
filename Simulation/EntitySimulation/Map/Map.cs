﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EntitySimulation.Objects;
using Utility;

namespace EntitySimulation.Map
{
    public class Map
    {
        #region Constants

        private const int MaxConsumedBufferSize = 5000;

        #endregion

        #region Fields

        private readonly List<ObjectState> _consumedObjects = new List<ObjectState>();
        private readonly object _mapLock = new object();
        private readonly ConcurrentBag<ObjectMoveInfo> _objectMovements = new ConcurrentBag<ObjectMoveInfo>();
        public readonly Vector2Int ChunkCount;

        public readonly Chunk[] Chunks;
        public readonly Vector2 ChunkSize;

        #endregion

        #region Constructors

        public Map(Vector2 chunkSize, Vector2Int chunkCount)
        {
            ChunkSize = chunkSize;
            ChunkCount = chunkCount;
            Chunks = new Chunk[ChunkCount.X * ChunkCount.Y];
            for (int x = 0; x < ChunkCount.X; x++)
            {
                for (int y = 0; y < ChunkCount.Y; y++) Chunks[x * ChunkCount.Y + y] = new Chunk(new Vector2Int(x, y), ChunkSize);
            }
        }

        #endregion

        #region Properties

        public Vector2 Size => new Vector2(ChunkSize.X * ChunkCount.X, ChunkSize.Y * ChunkCount.Y);

        #endregion

        #region Methods
        public void MoveEntity(ObjectState target, Vector2 from, Vector2 to) => MoveEntity(target, GetChunk(from), from, GetChunk(to), to);

        public void MoveEntity(ObjectState target, Vector2Int fromChunk,Vector2 fromPos,  Vector2Int toChunk, Vector2 toPos)
        {
            if (fromChunk == toChunk) return;
            _objectMovements.Add(new ObjectMoveInfo(target, fromChunk, fromPos, toChunk, toPos));
        }

        public void AddEntity(ObjectState state, Vector2Int chunk) => _objectMovements.Add(new ObjectMoveInfo(state, chunk));

        public void AddEntity(ObjectState state, Vector2 pos)
        {
            ObjectHelper.Position.Set(state, pos);
            AddEntity(state, GetChunk(pos));
        }

        private Vector2Int GetChunk(Vector2 pos) => (Vector2Int)(pos / ChunkSize);
        public void LockMap() => Monitor.Enter(_mapLock);
        public void UnlockMap() => Monitor.Exit(_mapLock);

        public void HandleObjectTranfers()
        {
            foreach (ObjectMoveInfo info in _objectMovements)
            {
                if (info.From.HasValue)
                {
                    Vector2Int from = info.From.Value;
                    if (!Chunks.First(c => c.Index == from).Objects.Remove(info.TargetObject)) throw new InvalidOperationException("Object not in chunk");
                }
                if (info.From != null && info.To != (Vector2Int)(ObjectHelper.Position.Get(info.TargetObject) / ChunkSize)) throw new Exception();
                Chunks.First(c => c.Index == info.To).Objects.Add(info.TargetObject);
            }
            _objectMovements.Clear();
        }

        public void CleanUp()
        {
            _consumedObjects.AddRange(Chunks.SelectMany(c => c.CleanupObjects()));
            if (_consumedObjects.Count > MaxConsumedBufferSize) _consumedObjects.RemoveRange(0, _consumedObjects.Count - MaxConsumedBufferSize);
        }

        public ObjectState[] RetrieveConsumed()
        {
            ObjectState[] objects = _consumedObjects.ToArray();
            _consumedObjects.Clear();
            return objects;
        }

        public IEnumerable<T> GetAllValues<T>(IVariableDefinition variable, T defaultValue, params int[] typeFilter) => GetAllValues(variable.Name, defaultValue, typeFilter);
        public IEnumerable<T> GetAllValues<T>(string valueName, T defaultValue, params int[] typeFilter)
        {
            if (typeFilter.Any())
                return Chunks.SelectMany(c => c.Objects
                    .Where(o => typeFilter.Contains(ObjectHelper.Type.Get(o)))
                    .Select(o => o.Get(valueName, defaultValue)));
            return Chunks.SelectMany(c => c.Objects
                .Select(o => o.Get(valueName, defaultValue)));
        }

        public void TransferData()
        {
            Parallel.ForEach(Chunks, c => c.TransferData());
        }

        public void Tick(TimeSpan delta)
        {
            Parallel.ForEach(Chunks, c => c.Tick(delta));
        }
        #endregion

        private struct ObjectMoveInfo
        {
            public readonly Vector2Int? From;
            public readonly Vector2Int To;
            public readonly Vector2 FromPos;
            public readonly Vector2 ToPos;

            public readonly ObjectState TargetObject;

            public ObjectMoveInfo(ObjectState targetObject, Vector2Int from, Vector2 fromPos, Vector2Int to, Vector2 toPos)
            {
                From = from;
                FromPos = fromPos;
                To = to;
                ToPos = toPos;
                TargetObject = targetObject;
            }
            public ObjectMoveInfo(ObjectState targetObject, Vector2Int from, Vector2Int to)
            {
                From = from;
                To = to;
                FromPos = Vector2.Zero;
                ToPos = Vector2.Zero;
                TargetObject = targetObject;
            }

            public ObjectMoveInfo(ObjectState targetObject, Vector2Int to)
            {
                From = null;
                To = to;
                FromPos = Vector2.Zero;
                ToPos = Vector2.Zero;
                TargetObject = targetObject;
            }

            public override string ToString() => $"({TargetObject}): {From} -> {To} / {FromPos} => {ToPos}";
        }
    }
}