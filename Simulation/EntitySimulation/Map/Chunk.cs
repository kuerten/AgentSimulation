﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EntitySimulation.Objects;
using Utility;
using Utility.Shapes;

namespace EntitySimulation.Map
{
    public class Chunk
    {
        public Chunk(Vector2Int index, Vector2 size)
        {
            Index = index;
            Bounds = new Rect( index.X*size.X, (index.X+1)*size.X, index.Y * size.Y, (index.Y + 1) * size.Y);
        }

        public Rect Bounds { get; }
        public Vector2Int Index { get; }
        public List<ObjectState> Objects { get; } = new List<ObjectState>();


        public void Tick(TimeSpan delta)
        {
            Parallel.ForEach(Objects, state => ObjectFactory.Update(delta, state));
        }

        public void TransferData()
        {
            Parallel.ForEach(Objects, state => state.TransferData());
        }
        
        public IEnumerable<ObjectState> CleanupObjects()
        {
            List<ObjectState> consumedObjects = new List<ObjectState>();
            for (int i = Objects.Count - 1; i >= 0; i--)
            {
                ObjectState s = Objects[i];
                if ( !ObjectHelper.Consumed.Get(s) ) continue;
                Objects.RemoveAt(i);
                consumedObjects.Add(s);
            }
            return consumedObjects;
        }

        public override string ToString() => Index.ToString();
    }
}