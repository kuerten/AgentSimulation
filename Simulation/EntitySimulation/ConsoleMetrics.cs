﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EntitySimulation
{
    public static class ConsoleMetrics
    {
        #region Constants

        private static readonly List<(string, Func<string>)> Entries = new List<(string, Func<string>)>();
        private static TimeSpan _currentDelta;
        private static int _padding;

        public static TimeSpan UpdateInterval = TimeSpan.FromMilliseconds(200);
        public static int Offset = 0;

        #endregion

        #region Properties

        public static int LineCount => Entries.Count;

        #endregion

        #region Methods

        public static void Display(TimeSpan delta)
        {
            _currentDelta = _currentDelta.Add(delta);
            if (_currentDelta < UpdateInterval) return;
            _currentDelta = new TimeSpan();
            Display();
        }

        public static void Display()
        {
            int cp = Console.CursorTop;
            Console.SetCursorPosition(0, Offset);
            foreach ((string, Func<string>) entry in Entries) Console.WriteLine(entry.Item1.PadRight(_padding) + entry.Item2());
            Console.SetCursorPosition(0, cp);
        }

        public static void AddEntry(string prefix, Func<string> value)
        {
            Entries.Add((prefix, value));
            _padding = Entries.Max(x => x.Item1.Length) + 1;
        }

        public static string ToCustomString(this TimeSpan t) => $"{t.Days * 24 + t.Hours:D2}:{t.Minutes:D2}:{t.Seconds:D2}";

        #endregion
    }
}