﻿using EntitySimulation.Components.Networks;
using EntitySimulation.Data;
using EntitySimulation.Data.MapLayout;
using EntitySimulation.Data.Transmitter.Interfaces;
using EntitySimulation.Networks;
using EntitySimulation.Objects;
using EntitySimulation.Serialization;
using NDesk.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EntitySimulation.Data.Transmitter;
using Utility;
using Utility.RandomHelper;
using Utility.Shapes;

namespace EntitySimulation
{
    public class Program
    {
        #region Constants

        public static TimeSpan TickLength = TimeSpan.FromMilliseconds(50);
        private static readonly RollingAverage TickAvg = new RollingAverage(50);
        private static readonly RollingAverage SpeedAvg = new RollingAverage(50);

        public static int EntityType;
        public static int SpawnerType;
        public static int FoodType;

        public static Map.Map Map;
        private static DateTime _startTime;
        private static TimeSpan _simulatedTime = TimeSpan.Zero;
        private static TimeSpan _maxTickLength = TimeSpan.Zero;
        private static string _inputPath;
        private static string _outputPath;
        private static TimeSpan _simulationLength = TimeSpan.MaxValue;
        private static readonly ManualResetEvent Reset = new ManualResetEvent(false);
        private static bool _isSilent = false;

        #endregion

        #region Events

        public static event EventHandler TickElapsed;

        #endregion

        #region Methods

        public static void Main(string[] args)
        {
            bool showHelp = false;
            int seed = 1234;
            string logConfigPath = null;
            string mapLayoutPath = null;
            string loggingPath = string.Empty;
            var p = new OptionSet
            {
                {"t|tps=", "set tickrate limit", tps => _maxTickLength = TimeSpan.FromMilliseconds(1000.0 / double.Parse(tps))},
                {"h|?|help", "show this message and exit", v => showHelp = v != null},
                {"d|delta=", "simulated time per tick in ms", v => TickLength = TimeSpan.FromMilliseconds(double.Parse(v))},
                {"s", "Silent mode", v =>  _isSilent = v != null},
                {"seed=", "seed for initalization", v => seed = int.Parse(v)},
                {"input=", "path to input file", v => _inputPath = v},
                {"output=", "file path to write current setup into", v => _outputPath = v},
                {"l|logging=", "file path to the current logging configuration", v => logConfigPath = v},
                {"length=", "maximum duration of the simulation in seconds or endless if not specified.", v => _simulationLength = TimeSpan.FromSeconds(double.Parse(v))},
                {"m|map=", "file path to the current may layout configuration", v => mapLayoutPath = v},
                {"loggingPath=", "only relevant in local logging mode, overwrites the path that logs are written to", v => loggingPath = v}
            };
            List<string> extra;
            try
            {
                extra = p.Parse(args);
            }
            catch (OptionException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Try --help' for more information.");
                return;
            }

            if (extra.Count > 0)
            {
                Console.WriteLine($"Unkown parameter: {string.Join(',', extra)}");
                Console.WriteLine("Try --help' for more information.");
                return;
            }

            if (showHelp)
            {
                ShowHelp(p);
                return;
            }

            Console.WriteLine("Starting simulation, press space to toggle realtime or any key to stop...");
            Console.WriteLine($"Tick Length: {TickLength.TotalMilliseconds}ms");
            if (_simulationLength != TimeSpan.MaxValue) Console.WriteLine($"Maximum duration: {_simulationLength.ToCustomString()}");

            ConsoleMetrics.Offset = Console.CursorTop;
            SetupMetrics();
            Console.CursorTop += ConsoleMetrics.LineCount;

            if (string.IsNullOrEmpty(_inputPath))
            {
                int foodType = SimulationGenerator.CreateFoodType();
                SimulationGenerator.CreateFoodSpawnerType(foodType);
                //SimulationGenerator.CreateNeatEntityType();
                SimulationGenerator.CreateBasicEntityType();
            }
            else
            {
                LoadObjects(_inputPath);
            }

            if (!string.IsNullOrEmpty(_outputPath)) SaveObjects(_outputPath);

            if (!string.IsNullOrEmpty(logConfigPath))
            {
                LoggingConfig.CurrentConfig = LoggingConfig.Parse(File.ReadAllText(logConfigPath));
                File.WriteAllText(logConfigPath, LoggingConfig.CurrentConfig.Serialize());
            }

            EntityType = ObjectFactory.GetType("entity");
            SpawnerType = ObjectFactory.GetType("spawner");
            FoodType = ObjectFactory.GetType("food");

            MapLayoutConfig layout = !string.IsNullOrEmpty(mapLayoutPath) ? MapLayoutConfig.Parse(File.ReadAllText(mapLayoutPath)) : GenerateDefaultLayout(seed);
            Map = new Map.Map(layout.ChunkSize, layout.ChunkCount);

            ObjectFactory.SetGlobal("map", Map);
            ObjectFactory.SetGlobal("seed", seed);

            ObjectFactory.InitializeComponents();
            layout.Populate(Map, seed);

            var tokenSource = new CancellationTokenSource();
            CancellationToken token = tokenSource.Token;

            ITransmitter transmitter = LoggingConfig.CurrentConfig.Transmitter;
            transmitter.Map = Map;
            if (transmitter is ISyncTransmitter st) TickElapsed += async (sender, eventArgs) => await st.Update();
            if (!string.IsNullOrEmpty(loggingPath) && transmitter is FileTransmitter ft) ft.OverrideLoggingPath(loggingPath);

            transmitter.SendReset().GetAwaiter().GetResult();

            SimulationInformation data = new SimulationInformation
            {
                Seed = seed,
                Duration = _simulationLength,
                Logging = LoggingConfig.CurrentConfig,
                MapLayout = layout,
                Objects = ObjectFactory.GetAllTypes().Select(JsonConverter.GetObjectDefinition).ToArray()
            };

            transmitter.SendConfig(data).GetAwaiter().GetResult();
            _startTime = DateTime.Now;
            Task t = Task.Run(() => Simulate(token), token);

            if (transmitter is IAsyncTransmitter at) at.Start();

            Task x = new Task(WaitForInput, token);
            x.Start();

            Reset.WaitOne();
            tokenSource.Cancel();
            t.Wait(1000);
            transmitter.Dispose();
            Console.WriteLine("Simulation stopped");
        }

        private static void WaitForInput()
        {
            while (Console.ReadKey().Key == ConsoleKey.Spacebar) _maxTickLength = _maxTickLength == TimeSpan.Zero ? TickLength : TimeSpan.Zero;
            Reset.Set();
        }

        private static void ShowHelp(OptionSet p)
        {
            Console.WriteLine("Usage: [OPTIONS]+ message");
            Console.WriteLine("Conducts a simulation.");
            Console.WriteLine("If no configuration is specifid, default values will be used.");
            Console.WriteLine();
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);
        }

        public static MapLayoutConfig GenerateDefaultLayout(int seed)
        {
            MapLayoutConfig layout = new MapLayoutConfig();
            layout.SpawnDefinitions.Add(new SingleSpawnDefinition
            {
                ObjectId = SpawnerType,
                Position = new Vector2(layout.MapSize.X / 2, layout.MapSize.Y / 2)
            });
            layout.SpawnDefinitions.Add(new MultiSpawnDefinition
            {
                ObjectId = EntityType,
                SpawnArea = new Rect(Vector2.Zero, layout.MapSize),
                Distribution = new RandomGenerator(),
                Amount = 30,
                RandomizeRotation = true
            });
            return layout;
        }

        private static void SaveObjects(string path)
        {
            JsonConverter.WriteObjects(path, ObjectFactory.GetAllTypes());
        }

        private static void LoadObjects(string path)
        {
            JsonConverter.ReadObjects(path);
        }

        public static void Simulate(CancellationToken token)
        {
            if (token.IsCancellationRequested) return;
            DateTime currentTick = DateTime.Now;
            Map.HandleObjectTranfers(); // Transfer objects once to place them into the map
            while (!token.IsCancellationRequested)
            {
                DateTime lastTick = currentTick;
                currentTick = DateTime.Now;
                TimeSpan delta = currentTick - lastTick;

                if (_maxTickLength != TimeSpan.Zero)
                    while (delta < _maxTickLength) // Task.Delay und Thread.Sleep sind leider zu ungenau, daher Schleifen (auf kosten des Prozessors)
                    {
                        currentTick = DateTime.Now;
                        delta = currentTick - lastTick;
                    }
                Tick(delta);
                _simulatedTime += TickLength;
                TickElapsed?.Invoke(null, EventArgs.Empty);
                if (_simulatedTime < _simulationLength) continue;
                Console.WriteLine("Time limit reached, ending simulation...");
                Reset.Set();
                return;
            }
        }

        public static void Tick(TimeSpan delta)
        {
            Map.LockMap();
            Map.TransferData();
            Map.HandleObjectTranfers();
            Map.TransferData();
            Map.CleanUp();
            Map.UnlockMap();

            Map.Tick(TickLength);

            SpeedAvg.Push(TickLength / delta);
            TickAvg.Push(delta.TotalMilliseconds);
            ConsoleMetrics.Display(delta);
        }

        private static void SetupMetrics()
        {
            ConsoleMetrics.AddEntry("Runtime:", () => (DateTime.Now - _startTime).ToCustomString());
            ConsoleMetrics.AddEntry("Simulation Time:", () => _simulatedTime.ToCustomString());
            ConsoleMetrics.AddEntry("TPS:", () => $"{1000.0 / TickAvg.Average:F1} ({TickAvg.Average:F3}ms)");
            ConsoleMetrics.AddEntry("Simulation Speed:", () => $"x{SpeedAvg.Average:F3}");
            if (_isSilent) return;
            ConsoleMetrics.AddEntry("Average complexity:",
                () => Map.GetAllValues<NetworkDefinition>(NetworkTrainer.Network, default, EntityType).Select(n => n.LinkCount).Average().ToString("F3"));
            ConsoleMetrics.AddEntry("Average energy:", () => Map.GetAllValues<double>(ObjectHelper.Energy, 0, EntityType).DefaultIfEmpty().Average().ToString("F3"));
            ConsoleMetrics.AddEntry("Max energy:", () => Map.GetAllValues<double>(ObjectHelper.Energy, 0, EntityType).DefaultIfEmpty().Max().ToString("F3"));
            ConsoleMetrics.AddEntry("Iteration:", () => Map.GetAllValues("NetworkTrainer.Iteration", 0, EntityType).DefaultIfEmpty().Max().ToString());
        }

        #endregion
    }
}