﻿using System.Collections.Generic;
using EntitySimulation.Objects;

namespace EntitySimulation.Serialization
{
    public class ObjectDefinition
    {
        public int TypeId { get; set; }
        public string Name { get; set; }
        public ObjectFactory.ComponentEntry[] Components { get; set; }
        public Dictionary<string, IAlterationContainer> State { get; set; }
    }
}