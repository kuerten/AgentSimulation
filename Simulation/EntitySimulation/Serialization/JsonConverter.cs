﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using EntitySimulation.Objects;
using Newtonsoft.Json;
using ErrorEventArgs = Newtonsoft.Json.Serialization.ErrorEventArgs;

namespace EntitySimulation.Serialization
{
    public static class JsonConverter
    {
        #region Constants

        private static readonly JsonSerializer Serializer;

        #endregion

        #region Constructors

        static JsonConverter()
        {
            Serializer = new JsonSerializer
            {
                TypeNameHandling = TypeNameHandling.Auto,
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                Formatting = Formatting.Indented,
                MissingMemberHandling = MissingMemberHandling.Error
            };
            Serializer.Error += SerializerOnError;
        }

        #endregion

        #region Methods

        private static void SerializerOnError(object sender, ErrorEventArgs e)
        {
            ((ObjectDefinition) e.ErrorContext.OriginalObject).TypeId = -1;
            e.ErrorContext.Handled = true;
        }

        public static int[] ReadObjects(string path)
        {
            using StreamReader sr = new StreamReader(path);
            using JsonReader reader = new JsonTextReader(sr);
            ObjectDefinition[] definitions = Serializer.Deserialize<ObjectDefinition[]>(reader);
            foreach (ObjectDefinition definition in definitions)
            {
                if (definition.TypeId == -1) continue;
                ObjectFactory.RegisterType(definition.TypeId);
                if (!string.IsNullOrEmpty(definition.Name)) ObjectFactory.SetName(definition.TypeId, definition.Name);
                foreach (ObjectFactory.ComponentEntry entry in definition.Components) ObjectFactory.AddComponent(definition.TypeId, entry);
                ObjectFactory.SetInitialState(definition.TypeId, new ObjectState(definition.State));
            }
            return definitions.Select(x => x.TypeId).ToArray();
        }

        public static void WriteObjects(string path, IEnumerable<int> objects) => Serialize(path, objects.Select(GetObjectDefinition));

        public static ObjectDefinition GetObjectDefinition(int objectId) => new ObjectDefinition
        {
            TypeId = objectId,
            Name = ObjectFactory.GetName(objectId),
            Components = ObjectFactory.GetComponents(objectId),
            State = ObjectFactory.GetInitialState(objectId)?.Export()
        };

        public static void Serialize(string path, object data)
        {
            using StreamWriter sw = new StreamWriter(path);
            using JsonWriter writer = new JsonTextWriter(sw);
            Serializer.Serialize(writer, data);
        }

        #endregion
    }
}