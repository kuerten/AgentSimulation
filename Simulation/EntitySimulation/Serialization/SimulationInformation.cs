﻿using System;
using EntitySimulation.Data;
using EntitySimulation.Data.MapLayout;

namespace EntitySimulation.Serialization
{
    public class SimulationInformation
    {
        public int Seed { get; set; }
        public TimeSpan Duration { get; set; }
        public ObjectDefinition[] Objects { get; set; }
        public LoggingConfig Logging { get; set; }
        public MapLayoutConfig MapLayout { get; set; }
    }
}