﻿using EntitySimulation.Components;
using EntitySimulation.Components.Detection;
using EntitySimulation.Components.Food;
using EntitySimulation.Components.Map;
using EntitySimulation.Components.Networks;
using EntitySimulation.Networks;
using EntitySimulation.Networks.Fitness;
using EntitySimulation.Networks.Mutation;
using EntitySimulation.Networks.Training;
using EntitySimulation.Objects;
using System;
using EntitySimulation.Networks.Neat.Operations;
using EntitySimulation.Networks.Neat.Selection;
using Utility.RandomHelper;
using Utility.Shapes;

namespace EntitySimulation
{
    public static class SimulationGenerator
    {
        public static int CreateBasicEntityType()
        {
            int entityType = ObjectFactory.CreateNewType();
            ObjectFactory.SetName(entityType, "entity");
            ObjectFactory.AddComponent(entityType, new RadialDetector());
            ObjectFactory.AddComponent(entityType, new FoodConsumer());
            ObjectFactory.AddComponent(entityType, new Metabolism());
            ObjectFactory.AddComponent(entityType, new NeuralNetwork());
            ObjectFactory.AddComponent(entityType, new Movement());

            NetworkTrainer trainer = new NetworkTrainer()
            {
                Fitness = new EnergyFitness
                {
                    EnergyTreshold = 300
                },
                Mutator = new MeshMutator(),
                //Mutator = new LayerMutator(5),
                Trainer = new SimpleTrainer()
            };
            ObjectFactory.AddComponent(entityType, trainer);

            ObjectFactory.SetInitialState(entityType, GenerateEntityState(entityType, trainer));
            return entityType;
        }

        public static int CreatePointBasedEntityType()
        {
            int entityType = ObjectFactory.CreateNewType();
            ObjectFactory.SetName(entityType, "entity");
            ObjectFactory.AddComponent(entityType, new RadialDetector());
            ObjectFactory.AddComponent(entityType, new FoodConsumer());
            ObjectFactory.AddComponent(entityType, new Metabolism());
            ObjectFactory.AddComponent(entityType, new NeuralNetwork());
            ObjectFactory.AddComponent(entityType, new Movement());

            NetworkTrainer trainer = new NetworkTrainer
            {
                Fitness = new EnergyFitness(),
                Mutator = new MeshMutator(),
                Trainer = new PointBasedTrainer()
            };
            ObjectFactory.AddComponent(entityType, trainer);

            ObjectFactory.SetInitialState(entityType, GenerateEntityState(entityType, trainer));
            return entityType;
        }

        public static int CreateNeatEntityType()
        {
            int entityType = ObjectFactory.CreateNewType();
            ObjectFactory.SetName(entityType, "entity");
            ObjectFactory.AddComponent(entityType, new RadialDetector());
            ObjectFactory.AddComponent(entityType, new FoodConsumer());
            ObjectFactory.AddComponent(entityType, new Metabolism());
            ObjectFactory.AddComponent(entityType, new NeuralNetwork());
            ObjectFactory.AddComponent(entityType, new Movement());

            CompoundMutation weightMutation = new CompoundMutation
            {
                { new MutateWeights(new SelectFixed(1), new MutatePerturbLinkWeight(0.002)),0.1125},
                { new MutateWeights(new SelectFixed(2), new MutatePerturbLinkWeight(0.002)),0.1125},
                { new MutateWeights(new SelectFixed(3), new MutatePerturbLinkWeight(0.002)),0.1125},
                { new MutateWeights(new SelectProportion(0.02), new MutatePerturbLinkWeight(0.002)),0.1125},
                { new MutateWeights(new SelectFixed(1), new MutatePerturbLinkWeight(1)),0.1125},
                { new MutateWeights(new SelectFixed(2), new MutatePerturbLinkWeight(1)),0.1125},
                { new MutateWeights(new SelectFixed(3), new MutatePerturbLinkWeight(1)),0.1125},
                { new MutateWeights(new SelectProportion(0.02), new MutatePerturbLinkWeight(1)),0.1125},
                { new MutateWeights(new SelectFixed(1), new MutateResetLinkWeight()),0.03},
                { new MutateWeights(new SelectFixed(2), new MutateResetLinkWeight()),0.03},
                { new MutateWeights(new SelectFixed(3), new MutateResetLinkWeight()),0.03},
                { new MutateWeights(new SelectProportion(0.02), new MutateResetLinkWeight()),0.01}
            };

            NetworkTrainer trainer = new NetworkTrainer
            {
                Fitness = new EnergyFitness
                {
                    EnergyTreshold = 300
                },
                Mutator = new MeshMutator(),
                Trainer = new NeatTrainer
                {
                    Mutation = new CompoundMutation
                    {
                        {weightMutation, 0.700},
                        {new MutateAddLink(), 0.250},
                        {new MutateRemoveLink(), 0.050}
                    }
                }
            };
            ObjectFactory.AddComponent(entityType, trainer);

            ObjectFactory.SetInitialState(entityType, GenerateEntityState(entityType, trainer));
            return entityType;
        }

        private static ObjectState GenerateEntityState(int entityType, RecurringAction trainer)
        {
            ObjectState state = ObjectFactory.GenerateInitialState(entityType);

            ObjectHelper.Position.Add(state);
            ObjectHelper.Rotation.Add(state);

            Movement.Mass.Set(state, 5.0);// kg

            NetworkDefinition network = new NetworkDefinition(3, 2, 10);
            network.MapInput(0, RadialDetector.DetectedDistance.Name);
            network.MapInput(1, RadialDetector.Detected.Name);
            network.MapInput(2, RadialDetector.DetectedAngle.Name);
            network.MapOutput(0, Movement.RotationForce.Name);
            network.MapOutput(1, Movement.LinearForce.Name);

            NeuralNetwork.Network.Set(state, network);

            trainer.Interval.Set(state, TimeSpan.FromSeconds(0.5));

            LinearDetector.DetectionRange.Set(state, 250.0);

            FoodConsumer.ConsumeRadius.Set(state, 20.0);
            FoodConsumer.MaxEnergy.Set(state, 1000.0);
            Metabolism.ConsumptionRate.Set(state, 2.5);

            state.TransferData();
            return state;
        }

        public static int CreateFoodSpawnerType(int foodType)
        {
            int spawnerType = ObjectFactory.CreateNewType();
            ObjectFactory.SetName(spawnerType, "spawner");
            FoodSpawner foodSpawner = new FoodSpawner();
            ObjectFactory.AddComponent(spawnerType, foodSpawner);

            ObjectFactory.SetInitialState(spawnerType, GenerateSpawnerState(spawnerType, foodType, foodSpawner));
            return spawnerType;
        }

        private static ObjectState GenerateSpawnerState(int spawnerType, int foodType, FoodSpawner spawner)
        {
            ObjectState state = ObjectFactory.GenerateInitialState(spawnerType);
            ObjectHelper.Position.Add(state);

            spawner.Interval.Set(state, TimeSpan.FromMilliseconds(200));
            spawner.EnergyMaximum.Set(state, 1000.0);
            //spawner.Distribution.Set(state, new GaussGenerator { Mu = 0, Sigma = 30.0 });
            spawner.Distribution.Set(state, new RandomGenerator());
            spawner.SpawnArea.Set(state, new Rect(0, 500, 0, 500));
            spawner.SpawnType.Set(state, foodType);

            state.TransferData();
            return state;
        }

        public static int CreateFoodType()
        {
            int foodType = ObjectFactory.CreateNewType();
            ObjectFactory.SetName(foodType, "food");
            ObjectFactory.SetInitialState(foodType, GenerateFoodState(foodType));
            return foodType;
        }

        private static ObjectState GenerateFoodState(int foodType)
        {
            ObjectState state = ObjectFactory.GenerateInitialState(foodType);
            ObjectHelper.Position.Add(state);
            ObjectHelper.Consumed.Add(state, false);
            ObjectHelper.Energy.Add(state, 15.0);
            state.TransferData();
            return state;
        }
    }
}