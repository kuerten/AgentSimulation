﻿using System;
using System.Collections.Generic;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Selection
{
    public class SelectorList<T> : List<SelectorListEntry<T>>
    {
        private readonly Random _rng = new Random();
        public T GetRandom()
        {
            double r = _rng.NextDouble();
            double sum = 0;
            foreach (SelectorListEntry<T> entry in this)
            {
                sum += entry.Probability;
                if (r < sum) return entry.Element;
            }

            foreach (SelectorListEntry<T> entry in this)
            {
                if (Math.Abs(entry.Probability) > 0) return entry.Element;
            }

            throw new Exception("Invalid probabilities");
        }

        public double this[T key]
        {
            get => Find(x => Equals(x.Element, key)).Probability;
            set => Find(x => Equals(x.Element, key)).Probability = value;
        }

        public void Add(T key, double value) => Add(new SelectorListEntry<T> { Element = key, Probability = value });

        public T GetRandom(int skip)
        {
            double totalProb = 1 - this[skip].Probability;
            double value = _rng.NextDouble() * totalProb;
            double accumulator = 0;
            for (int i = 0; i < Count; i++)
            {
                if (i == skip) continue;
                accumulator += this[i].Probability;
                if (accumulator > value) return this[i].Element;
            }
            for (int i = 0; i < Count; i++)
            {
                if (i == skip) continue;
                if (Math.Abs(this[i].Probability) > 0) return this[i].Element;
            }

            throw new Exception("Invalid probabilities");
        }

    }

    public class SelectorListEntry<T>
    {
        public double Probability { get; set; }
        public T Element { get; set; }
    }
}