﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utility.RandomHelper;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Selection
{
    public class SelectFixed : ILinkSelector
    {
        private readonly int _linkCount;
        public SelectFixed(int linkCount)
        {
            _linkCount = linkCount;
        }

        public IEnumerable<Link> SelectLinks(Random rng, NetworkDefinition network)
        {
            IList<Link> result = new List<Link>();
            int cnt = Math.Min(_linkCount, network.Links.Count);
            while (result.Count < cnt)
            {
                Link l = network.Links.Except(result).SelectRandom(rng);
                result.Add(l);
                yield return l;
            }
        }
    }
}