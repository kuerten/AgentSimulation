﻿using System.Collections.Generic;
using Utility.RandomHelper;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Selection
{
    public class SelectProportion : ILinkSelector
    {
        private readonly double _proportion;
        public SelectProportion(double proprotion)
        {
            _proportion = proprotion;
        }

        public IEnumerable<Link> SelectLinks(Random rng, NetworkDefinition network)
        {
            if (network.LinkCount == 0) yield break;
            bool mutated = false;
            foreach (Link link in network.Links)
            {
                if (!(rng.NextDouble() < _proportion)) continue;
                mutated = true;
                yield return link;
            }

            if (!mutated) yield return network.Links.SelectRandom(rng);
        }
    }
}