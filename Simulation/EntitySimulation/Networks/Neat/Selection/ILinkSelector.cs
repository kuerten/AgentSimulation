﻿using System.Collections.Generic;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Selection
{
    public interface ILinkSelector
    {
        IEnumerable<Link> SelectLinks(Random rng, NetworkDefinition network);
    }
}