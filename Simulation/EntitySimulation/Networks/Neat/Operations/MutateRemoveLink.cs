﻿using EntitySimulation.Networks.Mutation;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Operations
{
    public class MutateRemoveLink : IMutation
    {
        public const int MinLink = 5;

        public void PerformOperation(Random rng, NetworkDefinition target, IMutator mutator)
        {
            if (target.Links.Count < MinLink) return;
            mutator.RemoveRandomLink(target, rng);
        }
    }
}