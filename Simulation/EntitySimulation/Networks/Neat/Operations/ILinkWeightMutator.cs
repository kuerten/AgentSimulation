﻿using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Operations
{
    public interface ILinkWeightMutator
    {
        double MutateWeight(Random rng, double weight, double weightRange);
    }
}