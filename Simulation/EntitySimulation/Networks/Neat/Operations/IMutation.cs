﻿using EntitySimulation.Networks.Mutation;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Operations
{
    public interface IMutation
    {
        void PerformOperation(Random rng, NetworkDefinition target, IMutator mutator);
    }
}