﻿using System.Linq;
using EntitySimulation.Networks.Mutation;
using EntitySimulation.Networks.Neat.Selection;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Operations
{
    public class MutateWeights : IMutation
    {
        public ILinkSelector LinkSelection { get; }
        public ILinkWeightMutator WeightMutation { get; }
        public double WeightRange { get; set; } = 1.0;
        public MutateWeights(ILinkSelector linkSelection, ILinkWeightMutator weightMutation)
        {
            LinkSelection = linkSelection;
            WeightMutation = weightMutation;
        }

        public void PerformOperation(Random rng, NetworkDefinition target, IMutator mutator)
        {
            if (target.LinkCount == 0) return;
            foreach (Link link in LinkSelection.SelectLinks(rng, target).ToArray())
            {
                Link l = link;
                l.Weight = WeightMutation.MutateWeight(rng, link.Weight, WeightRange);
                target.Links[target.Links.IndexOf(link)] = l;
            }
        }
    }
}