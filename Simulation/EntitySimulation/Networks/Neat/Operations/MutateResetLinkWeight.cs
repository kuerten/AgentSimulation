﻿using Utility.RandomHelper;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Operations
{
    public class MutateResetLinkWeight : ILinkWeightMutator
    {
        public double MutateWeight(Random rng, double weight, double weightRange) => rng.NextDouble(-weightRange, weightRange);
    }
}