﻿using EntitySimulation.Networks.Mutation;
using EntitySimulation.Networks.Neat.Selection;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Operations
{
    public class CompoundMutation : SelectorList<IMutation>, IMutation
    {
        public void PerformOperation(Random rng, NetworkDefinition target, IMutator mutator) => 
            GetRandom().PerformOperation(rng, target, mutator);
    }
}