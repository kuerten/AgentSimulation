﻿using Utility;
using Utility.RandomHelper;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Operations
{
    public class MutatePerturbLinkWeight : ILinkWeightMutator
    {
        private readonly double _sigma;
        public MutatePerturbLinkWeight(double sigma)
        {
            _sigma = sigma;
        }

        public double MutateWeight(Random rng, double weight, double weightRange) => 
            (weight + rng.NextGaussian() * _sigma).Clamp(weightRange);
    }
}