﻿using EntitySimulation.Networks.Mutation;
using Utility.RandomHelper;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Neat.Operations
{
    public class MutateAddLink : IMutation
    {
        public int MaxLinkCount { get; set; } = 100;
        public void PerformOperation(Random rng, NetworkDefinition target, IMutator mutator)
        {
            if (target.LinkCount >= MaxLinkCount) return;
            mutator.AddNewRandomLink(target, rng, rng.NextDouble(-1.0, 1.0));
        }
        
    }
}