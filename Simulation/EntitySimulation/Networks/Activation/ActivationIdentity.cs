﻿namespace EntitySimulation.Networks.Activation
{
    public class ActivationIdentity : IActivationFunction
    {
        public double Execute(double input) => input;
        public object Clone() => new ActivationIdentity();
    }
}