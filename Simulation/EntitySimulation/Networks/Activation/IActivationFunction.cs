﻿using System;

namespace EntitySimulation.Networks.Activation
{
    public interface IActivationFunction : ICloneable
    {
        double Execute(double input);
    }
}