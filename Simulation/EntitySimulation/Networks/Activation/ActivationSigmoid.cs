﻿using System;

namespace EntitySimulation.Networks.Activation
{
    public class ActivationSigmoid : IActivationFunction
    {
        public double Execute(double data) => 2.0 / (1.0 + Math.Exp(-4.9 * data)) - 1;
        public object Clone() => new ActivationSigmoid();
    }
}