﻿using System;

namespace EntitySimulation.Networks.Activation
{
    public class ActivationLog : IActivationFunction
    {
        public double Execute(double data) => data >= 0 ? Math.Log(1 + data) : -Math.Log(1 - data);
        public object Clone() => new ActivationLog();
    }
}