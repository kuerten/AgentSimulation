﻿namespace EntitySimulation.Networks.Activation
{
    public class ActivationBinary : IActivationFunction
    {
        public double EdgePosition { get; set; } = 0.0;
        public double Execute(double input) => input >= EdgePosition ? 1.0 : -1.0;
        public object Clone() => new ActivationBinary {EdgePosition = EdgePosition};
    }
}