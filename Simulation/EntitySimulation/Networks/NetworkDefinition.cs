﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Newtonsoft.Json;
using Utility;

namespace EntitySimulation.Networks
{
    public class NetworkDefinition : ICloneable
    {
        #region Enums

        public enum ELinkAddingRule
        {
            Average,
            TakeNew,
            KeepOld,
            Interpolate
        }

        #endregion

        #region Constructors

        private NetworkDefinition(NetworkDefinition network)
        {
            State = (double[])network.State.Clone();
            LastState = (double[])network.LastState.Clone();
            Links = new List<Link>(network.Links);
            InputCount = network.InputCount;
            OutputCount = network.OutputCount;
            InputMappings = new List<MappedValue>(network.InputMappings);
            OutputMappings = new List<MappedValue>(network.OutputMappings);
            LinkRule = network.LinkRule;
            InterpolationFactor = network.InterpolationFactor;
            LastLinkId = network.LastLinkId;
        }

        public NetworkDefinition(int inputCount, int outputCount, int neuronCount)
        {
            InputCount = inputCount;
            OutputCount = outputCount;
            InputMappings = new List<MappedValue>();
            OutputMappings = new List<MappedValue>();
            LinkRule = ELinkAddingRule.Average;
            InterpolationFactor = 0.5;

            State = new double[inputCount + outputCount + neuronCount];
            LastState = new double[inputCount + outputCount + neuronCount];
            Links = new List<Link>();
        }
        #endregion

        #region Properties

        public int LinkCount => Links.Count;
        public List<MappedValue> InputMappings { get; }
        public List<MappedValue> OutputMappings { get; }
        public int InputCount { get; }
        public int OutputCount { get; }
        public int NeuronCount => State.Length - InputCount - OutputCount;

        [JsonIgnore] public int[] InputIds => Enumerable.Range(0, InputCount).ToArray();
        [JsonIgnore] public int[] OutputIds => Enumerable.Range(InputCount, OutputCount).ToArray();
        [JsonIgnore] public int[] NeuronIds => Enumerable.Range(InputCount + OutputCount, State.Length - InputCount - OutputCount).ToArray();

        public double[] State { get; private set; }
        public double[] LastState { get; private set; }
        public List<Link> Links { get; set; }
        public ELinkAddingRule LinkRule { get; set; }
        public double InterpolationFactor { get; set; }
        private int LastLinkId { get; set; }
        #endregion

        #region Methods

        public void AddLink(int fromNeuronId, int toNeuronId, double weight)
        {
            Argument.IsNotOutOfRange(() => fromNeuronId, 0, State.Length - 1);
            Argument.IsNotOutOfRange(() => toNeuronId, 0, State.Length - 1);
            int linkIndex = Links.FindIndex(l => l.FromNeuronId == fromNeuronId && l.ToNeuronId == toNeuronId);
            if (linkIndex >= 0)
            {
                Link l = Links[linkIndex];
                switch (LinkRule)
                {
                    case ELinkAddingRule.Average:
                        l.Weight = (l.Weight + weight) / 2;
                        break;
                    case ELinkAddingRule.TakeNew:
                        l.Weight = weight;
                        break;
                    case ELinkAddingRule.KeepOld:
                        break;
                    case ELinkAddingRule.Interpolate:
                        l.Weight = MathUtil.Interpolate(l.Weight, weight, InterpolationFactor);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                Links[linkIndex] = l;
                return;
            }
            Links.Add(new Link(++LastLinkId, fromNeuronId, toNeuronId, weight));
        }

        [Pure] public bool IsInputId(int neuronId) => neuronId >= 0 && neuronId < InputCount;

        [Pure] public bool IsOutputId(int neuronId) => neuronId >= InputCount && neuronId < InputCount + OutputCount;

        [Pure] public bool IsNeuronId(int neuronId) => neuronId >= InputCount + OutputCount && neuronId < State.Length;

        [Pure]
        public int GetInputIndex(int neuronId)
        {
            if (!IsInputId(neuronId)) throw new IndexOutOfRangeException();
            return neuronId;
        }

        [Pure]
        public int GetOutputIndex(int neuronId)
        {
            if (!IsOutputId(neuronId)) throw new IndexOutOfRangeException();
            return neuronId - InputCount;
        }

        [Pure]
        public int GetNeuronIndex(int neuronId)
        {
            if (!IsNeuronId(neuronId)) throw new IndexOutOfRangeException();
            return neuronId - InputCount - OutputCount;
        }

        public int[] GetConnectableNeuronIds(bool excludeOutputs = false) =>
            (excludeOutputs
                ? Links.Select(l => l.ToNeuronId).Except(OutputIds)
                : Links.Select(l => l.ToNeuronId))
            .Union(InputIds).Distinct().ToArray();

        public int[] GetLeafNeuronIds(bool excludeOutputs = false) => (excludeOutputs
                ? Links.Select(l => l.ToNeuronId).Except(OutputIds)
                : Links.Select(l => l.ToNeuronId))
            .Except(Links.Select(x => x.FromNeuronId))
            .Union(InputIds).Distinct().ToArray();

        public void TrasferState()
        {
            double[] temp = State;
            State = LastState;
            LastState = temp;
            Array.Copy(LastState, State, State.Length);
        }
        object ICloneable.Clone() => Clone();
        public NetworkDefinition Clone() => new NetworkDefinition(this);

        public void MapInput(int inputId, string valueName)
        {
            Argument.IsNotOutOfRange(() => inputId, 0, InputCount - 1);
            MappedValue mappedValue = new MappedValue
            {
                NeuronId = inputId,
                ValueId = valueName
            };
            InputMappings.Add(mappedValue);
        }

        public void MapOutput(int outputId, string valueName)
        {
            Argument.IsNotOutOfRange(() => outputId, 0, OutputCount - 1);
            MappedValue mappedValue = new MappedValue
            {
                NeuronId = InputCount + outputId,
                ValueId = valueName
            };
            OutputMappings.Add(mappedValue);
        }

        #endregion
    }

    public struct MappedValue
    {
        #region Fields

        public int NeuronId;
        public string ValueId;

        #endregion

        #region Methods
        
        public override string ToString() => $"ID {NeuronId} <=> {ValueId}";
        
        #endregion
    }
}