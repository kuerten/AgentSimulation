﻿using System;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Mutation
{
    public interface IMutator : ICloneable
    {
        void AddRandomLink(NetworkDefinition network, Random rng, double weight);
        void AddNewRandomLink(NetworkDefinition network, Random rng, double weight);
        void ModifiyRandomLink(NetworkDefinition network, Random rng, Func<double, double> operation);
        void RemoveRandomLink(NetworkDefinition network, Random rng);
    }
}