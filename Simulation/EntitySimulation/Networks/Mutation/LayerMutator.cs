﻿using System;
using System.Linq;
using Utility.RandomHelper;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Mutation
{
    public class LayerMutator : IMutator
    {
        #region Constructors

        public LayerMutator(int layerSize)
        {
            LayerSize = layerSize;
        }

        public LayerMutator(LayerMutator other)
        {
            LayerSize = other.LayerSize;
        }

        #endregion

        #region Properties

        public int LayerSize { get; }

        #endregion

        #region Methods

        public LayerMutator Clone() => new LayerMutator(this);
        object ICloneable.Clone() => Clone();
        
        public void AddRandomLink(NetworkDefinition network, Random rng, double weight)
        {
            LayerContext context = new LayerContext(network, LayerSize);
            int fromNeuronId = network.GetConnectableNeuronIds(true).SelectRandom(rng);
            int fromLayerNumber = context.GetLayerNumber(fromNeuronId);
            if (fromLayerNumber == context.LayerCount) throw new InvalidOperationException("Source cannot be output layer");

            int[] toLayer = context.GetLayer(fromLayerNumber + 1);
            int toNeuronId = toLayer.SelectRandom(rng);

            network.AddLink(fromNeuronId, toNeuronId, weight);
        }

        public void AddNewRandomLink(NetworkDefinition network, Random rng, double weight)
        {
            LayerContext context = new LayerContext(network, LayerSize);
            int fromNeuronId = network.GetLeafNeuronIds(true).SelectRandom(rng);
            int fromLayerNumber = context.GetLayerNumber(fromNeuronId);
            if (fromLayerNumber == context.LayerCount) throw new InvalidOperationException("Source cannot be output layer");

            int[] toLayer = context.GetLayer(fromLayerNumber + 1);
            Link[] existingLinks = network.Links.Where(x => x.FromNeuronId == fromNeuronId).ToArray();
            int toNeuronId = toLayer.Where(x => existingLinks.All(y => y.ToNeuronId != x)).SelectRandom(rng);

            network.AddLink(fromNeuronId, toNeuronId, weight);
        }

        public void ModifiyRandomLink(NetworkDefinition network, Random rng, Func<double, double> operation)
        {
            if (network.Links.Count == 0) return;
            int i = rng.Next(0, network.Links.Count);
            Link l = network.Links[i];
            l.Weight = operation(l.Weight);
            network.Links[i] = l;
        }

        public void RemoveRandomLink(NetworkDefinition network, Random rng)
        {
            if (network.Links.Count == 0) return;
            network.Links.RemoveAt(rng.Next(0, network.Links.Count));
        }
        
        #endregion

        private class LayerContext
        {
            #region Fields

            private readonly int _layerSize;
            private readonly NetworkDefinition _network;

            #endregion

            #region Constructors

            public LayerContext(NetworkDefinition network, int layerSize)
            {
                _network = network;
                _layerSize = layerSize;
                int layerCount = Math.DivRem(network.NeuronCount, _layerSize, out int rest);
                if (rest != 0) throw new InvalidOperationException("Network size invalid");
                LayerCount = layerCount + 2; // +2 to include input- and output-layer
            }

            #endregion

            #region Properties

            public int LayerCount { get; }

            #endregion

            #region Methods

            public int[] GetLayer(int layer)
            {
                int layerCount = LayerCount;
                if (layer < 0 || layer > layerCount) throw new IndexOutOfRangeException("Invalid layer");
                if (layer == 0) return _network.InputIds;
                if (layer == layerCount - 1) return _network.OutputIds;
                int[] l = new int[_layerSize];
                Array.Copy(_network.NeuronIds, (layer-1) * _layerSize, l, 0, _layerSize);
                return l;
            }

            public int GetLayerNumber(int neuronId)
            {
                if (_network.IsInputId(neuronId)) return 0;
                if (_network.IsOutputId(neuronId)) return LayerCount;
                if (_network.IsNeuronId(neuronId)) return _network.GetNeuronIndex(neuronId) / _layerSize + 1;
                throw new InvalidOperationException();
            }

            #endregion
        }

    }
}