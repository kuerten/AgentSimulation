﻿using System;
using System.Linq;
using Utility.RandomHelper;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Mutation
{
    public class MeshMutator : IMutator
    {
        #region Methods

        object ICloneable.Clone() => Clone();
        public MeshMutator Clone() => new MeshMutator();

        public void Mutate(NetworkDefinition network, Random rng)
        {
            int[] fromNeurons = network.GetConnectableNeuronIds();
            int fromNeuronId = fromNeurons.SelectRandom(rng);
            int[] neurons = network.NeuronIds;
            int[] outputs = network.OutputIds;
            int toNeuronIndex = rng.Next(outputs.Length + neurons.Length);
            int toNeuronId = toNeuronIndex < neurons.Length ? neurons[toNeuronIndex] : outputs[toNeuronIndex - neurons.Length];
            double weight = rng.NextDouble(-1.0, 1.0);
            network.AddLink(fromNeuronId, toNeuronId, weight);
        }
        #endregion

        public void AddRandomLink(NetworkDefinition network, Random rng, double weight)
        {
            int[] fromNeurons = network.GetConnectableNeuronIds();
            int fromNeuronId = fromNeurons.SelectRandom(rng);
            int toNeuronId =
                network
                    .OutputIds
                    .Union(network.NeuronIds)
                    .SelectRandom(rng);
            network.AddLink(fromNeuronId, toNeuronId, weight);
        }

        public void AddNewRandomLink(NetworkDefinition network, Random rng, double weight)
        {
            int[] fromNeurons = network.GetLeafNeuronIds();
            int fromNeuronId = fromNeurons.SelectRandom(rng);

            Link[] existingLinks = network.Links.Where(x => x.FromNeuronId == fromNeuronId).ToArray();

            int[] targetNeurons = network
               .OutputIds
               .Union(network.NeuronIds)
               .Where(x => existingLinks.All(y => y.ToNeuronId != x))
               .ToArray();
            if( targetNeurons.Length == 0) return;
            int toNeuronId = targetNeurons.SelectRandom(rng);
            network.AddLink(fromNeuronId, toNeuronId, weight);
        }

        public void ModifiyRandomLink(NetworkDefinition network, Random rng, Func<double, double> operation)
        {
            if (network.Links.Count == 0) return;
            int i = rng.Next(0, network.Links.Count);
            Link l = network.Links[i];
            l.Weight = operation(l.Weight);
            network.Links[i] = l;
        }

        public void RemoveRandomLink(NetworkDefinition network, Random rng)
        {
            if (network.Links.Count == 0) return;
            network.Links.RemoveAt(rng.Next(0, network.Links.Count));
        }
    }
}