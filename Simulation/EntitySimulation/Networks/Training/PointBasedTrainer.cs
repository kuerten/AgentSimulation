﻿using EntitySimulation.Networks.Fitness;
using EntitySimulation.Networks.Mutation;
using EntitySimulation.Objects;
using System;
using System.Collections.Generic;
using Utility;
using Utility.RandomHelper;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Training
{
    public class PointBasedTrainer : ITrainer
    {
        #region Constants

        public const string PointsContainer = "pointBasedTrainer.PointsContainer";

        #endregion

        #region Fields

        private int _cycle;
        public int MaxLinkCount = 80;
        public int MaxNewLinksPerCycle = 1;
        public int MaxPoints = 10000;
        public int MinPointThreshold = 0;
        public int PointDecrease = 5;
        public int PointIncrease = 25;
        public int PointStartAmount = 500;

        #endregion

        #region Methods

        private static Dictionary<int, PointValue> GetPoints(ObjectState state) => state.Get<Dictionary<int, PointValue>>(PointsContainer);

        public bool Train(ObjectState state, Random rng, NetworkDefinition network, IMutator mutator, IFitness fitness)
        {
            int pointChange = fitness.GetFitness(state) < 1f ? -PointDecrease : PointIncrease;
            Dictionary<int, PointValue> points = GetPoints(state) ?? new Dictionary<int, PointValue>();

            for (int i = network.LinkCount - 1; i >= 0; i--)
            {
                PointValue value = points.GetOrCreate(network.Links[i].LinkId, () => new PointValue(PointStartAmount));
                value.Points = Math.Min(value.Points + pointChange, MaxPoints);
                if (value.Points < MinPointThreshold) 
                    network.Links.RemoveAt(i);
                else value.Cycle = _cycle;

            }
            
            int changed = points.RemoveAll(x => x.Cycle != _cycle || x.Points < MinPointThreshold);
            state.Set(PointsContainer, points);

            if (network.Links.Count < MaxLinkCount)
            {
                int added = 0;
                while (network.Links.Count < MaxLinkCount && added < MaxNewLinksPerCycle)
                {
                    mutator.AddNewRandomLink(network, rng, rng.NextDouble(-1.0, 1.0));
                    added++;
                }
                changed += added;
            }
            _cycle++;
            return changed > 0;
        }

        #endregion

        private class PointValue
        {
            #region Constructors

            public PointValue(int points)
            {
                Points = points;
            }

            #endregion

            #region Properties

            public int Points { get; set; }
            public int Cycle { get; set; }

            #endregion

            #region Methods

            public override string ToString() => $"{Cycle} => {Points}";

            #endregion
        }
    }
}