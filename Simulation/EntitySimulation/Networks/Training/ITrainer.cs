﻿using EntitySimulation.Networks.Fitness;
using EntitySimulation.Networks.Mutation;
using EntitySimulation.Objects;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Training
{
    public interface ITrainer
    {
        /// <summary>
        /// Trains the given Network
        /// </summary>
        /// <param name="state"></param>
        /// <param name="rng"></param>
        /// <param name="network"></param>
        /// <param name="mutator"></param>
        /// <param name="fitness"></param>
        /// <returns><see langword="true"/> when the network has been modified, otherwise <see langword="false"/>.</returns>
        bool Train(ObjectState state, Random rng, NetworkDefinition network, IMutator mutator, IFitness fitness);
    }
}