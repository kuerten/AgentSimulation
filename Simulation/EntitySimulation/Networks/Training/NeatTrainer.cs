﻿using EntitySimulation.Networks.Fitness;
using EntitySimulation.Networks.Mutation;
using EntitySimulation.Networks.Neat.Operations;
using EntitySimulation.Objects;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Training
{
    public class NeatTrainer : ITrainer
    {
        public IMutation Mutation { get; set; }
        public bool Train(ObjectState state, Random rng, NetworkDefinition network, IMutator mutator, IFitness fitness)
        {
            if (fitness.GetFitness(state) >= 1f) return false;
            Mutation.PerformOperation(rng, network, mutator);
            return true;
        }
    }
}