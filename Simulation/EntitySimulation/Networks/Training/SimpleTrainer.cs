﻿using EntitySimulation.Networks.Fitness;
using EntitySimulation.Networks.Mutation;
using EntitySimulation.Objects;
using Utility.RandomHelper;
using Random = Utility.Random;

namespace EntitySimulation.Networks.Training
{
    public class SimpleTrainer : ITrainer
    {
        public bool Train(ObjectState state, Random rng, NetworkDefinition network, IMutator mutator, IFitness fitness)
        {
            if (!(fitness.GetFitness(state) < 1f)) return false;
            mutator.AddRandomLink(network, rng, rng.NextDouble(-1.0, 1.0));
            return true;
        }
    }
}