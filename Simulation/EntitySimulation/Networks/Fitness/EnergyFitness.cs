﻿using EntitySimulation.Objects;

namespace EntitySimulation.Networks.Fitness
{
    public class EnergyFitness : IFitness
    {
        public float EnergyTreshold { get; set; } = 10;
        public double GetFitness(ObjectState target) => ObjectHelper.Energy.Get(target) >= EnergyTreshold ? 1 : 0;
    }
}