﻿using EntitySimulation.Objects;

namespace EntitySimulation.Networks.Fitness
{
    public interface IFitness
    {
        /// <summary>
        /// Determines the Fitness for the give <paramref name="target"/>.
        /// </summary>
        /// <param name="target"></param>
        /// <returns>Value of the range 0..1 where 1 equals the best and 0 the worst fitness.</returns>
        double GetFitness(ObjectState target);
    }
}