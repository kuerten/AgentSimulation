﻿namespace EntitySimulation.Networks
{
    public struct Link
    {
        #region Fields

        public readonly int LinkId;
        public int FromNeuronId;
        public int ToNeuronId;
        public double Weight;
        #endregion

        #region Constructors

        public Link(int id, int fromNeuronId, int toNeuronId, double weight)
        {
            LinkId = id;
            FromNeuronId = fromNeuronId;
            ToNeuronId = toNeuronId;
            Weight = weight;
        }

        #endregion

        #region Methods

        public override string ToString() => $"[Link: fromNeuron={FromNeuronId}, toNeuron={ToNeuronId}]";

        #endregion
    }
}