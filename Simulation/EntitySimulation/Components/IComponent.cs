﻿using System;
using System.Threading.Tasks;
using EntitySimulation.Objects;

namespace EntitySimulation.Components
{
    public interface IComponent
    {
        Task Tick(TimeSpan delta, ObjectState state);
        int Identifier { get; }
        void Initialize();
    }
}