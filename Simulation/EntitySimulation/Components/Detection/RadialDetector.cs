﻿using System;
using System.Threading.Tasks;
using EntitySimulation.Objects;
using Utility;
using Utility.Shapes;

namespace EntitySimulation.Components.Detection
{
    public class RadialDetector : MapComponent
    {
        #region Constants

        private static readonly Vector2 InvalidVector = new Vector2(double.NaN, double.NaN);
        public static readonly VariableDefinition<double> DetectionRange = new VariableDefinition<double>(1.0);
        public static readonly VariableDefinition<double> DetectedDistance = new VariableDefinition<double>(1.0);
        public static readonly VariableDefinition<double> DetectedAngle = new VariableDefinition<double>(0.0);
        public static readonly VariableDefinition<double> Detected = new VariableDefinition<double>(0.0);
        public static readonly VariableDefinition<Vector2> Position = ObjectHelper.Position;
        public static readonly VariableDefinition<double> Rotation = ObjectHelper.Rotation;

        #endregion

        #region Properties

        public override int Identifier { get; } = 1101493307;

        #endregion

        #region Methods

        private static double GetRange(Vector2 position, ObjectState target)
        {
            Vector2 targetPos = ObjectHelper.Position.Get(target, InvalidVector);
            return targetPos == InvalidVector ? double.MaxValue : Vector2.Distance(targetPos, position);
        }

        public override Task Tick(TimeSpan delta, ObjectState state)
        {
            Vector2 position = Position.Get(state);
            double detectionRange = DetectionRange.Get(state);
            ObjectState[] objects = DetectorHelper.Detect(Map.Chunks, new Circle(position, detectionRange), new[] {state});
            double closestDistance = double.MaxValue;
            ObjectState closest = null;
            foreach (ObjectState o in objects)
            {
                double distance = GetRange(position, o);
                if (distance >= closestDistance) continue;
                closest = o;
                closestDistance = distance;
            }
            if (closest == null)
            {
                DetectedDistance.Set(state, 1.0);
                DetectedAngle.Set(state, 0.0);
                Detected.Set(state, 0.0);
                return Task.CompletedTask;
            }
            double angle = Vector2.Angle(Vector2.Right, ObjectHelper.Position.Get(closest) - position);
            double rotation = Rotation.Get(state);
            DetectedAngle.Set(state, (angle + rotation) % 360 / 360);
            DetectedDistance.Set(state, Math.Min(closestDistance / detectionRange, 0));
            Detected.Set(state, 1.0);
            return Task.CompletedTask;
        }

        #endregion
    }
}