﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EntitySimulation.Objects;
using Utility;
using Utility.Shapes;

namespace EntitySimulation.Components.Detection
{
    public class LinearDetector : MapComponent
    {
        #region Constants

        private static readonly Vector2 InvalidVector = new Vector2(double.NaN, double.NaN);
        public static readonly VariableDefinition<double> DetectionRange = new VariableDefinition<double>(1.0);
        public static readonly VariableDefinition<double> DetectedDistance = new VariableDefinition<double>(1.0);
        public static readonly VariableDefinition<double> Detected = new VariableDefinition<double>(0.0);
        public static readonly VariableDefinition<Vector2> Position = ObjectHelper.Position;

        #endregion

        #region Properties

        public override int Identifier { get; } = 563913476;

        #endregion

        #region Methods

        private static double GetRange(Vector2 position, ObjectState target)
        {
            Vector2 targetPos = ObjectHelper.Position.Get(target, InvalidVector);
            return targetPos == InvalidVector ? double.MaxValue : Vector2.Distance(targetPos, position);
        }

        public override Task Tick(TimeSpan delta, ObjectState state)
        {
            Vector2 position = Position.Get(state);
            double detectionRange = DetectionRange.Get(state);
            ObjectState[] objects = DetectorHelper.Detect(Map.Chunks, new Circle(position, detectionRange), new[] {state});
            if (!objects.Any())
            {
                DetectedDistance.Set(state, 1.0);
                Detected.Set(state, 0.0);
                return Task.CompletedTask;
            }
            double closest = objects.Min(e => GetRange(position, e)); // if only 1 element = self
            DetectedDistance.Set(state, Math.Min(closest / detectionRange, 0));
            Detected.Set(state, 1.0);
            return Task.CompletedTask;
        }

        #endregion
    }
}