﻿using System.Collections.Generic;
using System.Linq;
using EntitySimulation.Map;
using EntitySimulation.Objects;
using Utility.Shapes;

namespace EntitySimulation.Components.Detection
{
    public static class DetectorHelper
    {
        public static ObjectState[] DetectAll(IEnumerable<Chunk> chunks) => chunks.SelectMany(c => c.Objects).ToArray();

        public static ObjectState[] Detect(IEnumerable<Chunk> chunks, IShape searchArea, IEnumerable<ObjectState> except = null) =>
            chunks
                .Where(c => c.Bounds.IsOverlapping(searchArea))
                .SelectMany(c => c.Objects.Where(e => searchArea.IsInBounds(ObjectHelper.Position.Get(e))))
                .Except(except ?? Enumerable.Empty<ObjectState>())
                .ToArray();

    }
}