﻿using System;
using System.Threading.Tasks;
using EntitySimulation.Objects;
using Utility;

namespace EntitySimulation.Components.Map
{
    public class Movement : MapComponent
    {
        #region Constants

        public static readonly VariableDefinition<double> RotationForce = new VariableDefinition<double>(0.0);
        public static readonly VariableDefinition<double> LinearForce = new VariableDefinition<double>(0.0);
        public static readonly VariableDefinition<double> Mass = new VariableDefinition<double>(1.0);
        public static readonly VariableDefinition<double> RotationVelocity = ObjectHelper.RotationVelocity;
        public static readonly VariableDefinition<double> Velocity = ObjectHelper.Velocity;
        public static readonly VariableDefinition<double> Rotation = ObjectHelper.Rotation;
        public static readonly VariableDefinition<Vector2> Position = ObjectHelper.Position;

        #endregion

        #region Fields

        private Vector2 _mapSize;

        #endregion

        #region Properties

        public double RotationVelocityMax { get; set; } = Math.PI / 16; // in rad/s
        public double RotationForceMax { get; set; } = 0.01; // in N

        public double VelocityMax { get; set; } = 60.0; // in m/s
        public double LinearForceMax { get; set; } = 5.0; // in N

        public double EnergyLoss { get; set; } = 0.01; // in %/s

        public override int Identifier { get; } = 1555655117;

        #endregion

        #region Methods

        public override Task Tick(TimeSpan delta, ObjectState state)
        {
            double mass = Mass.Get(state); // kg
            double lossFactor = 1.0 - EnergyLoss * delta.TotalSeconds;

            // Rotation accleration
            double rotationaVelocity = RotationVelocity.Get(state); // rad/s
            rotationaVelocity *= lossFactor;
            double rotationForceFactor = RotationForce.Get(state); // N

            double rotationalAcceleration = rotationForceFactor * RotationForceMax / mass;
            rotationaVelocity = (rotationaVelocity + rotationalAcceleration * delta.TotalSeconds).Clamp(RotationVelocityMax);
            RotationVelocity.Set(state, rotationaVelocity);

            // Forward acceleration
            double velocity = Velocity.Get(state); // m/s
            velocity *= lossFactor;
            double linearForce = LinearForce.Get(state).Clamp(1.0); // N

            double acceleration = linearForce * LinearForceMax / mass;
            velocity = Math.Clamp(velocity + acceleration * delta.TotalSeconds, 0, VelocityMax);
            Velocity.Set(state, velocity);

            // Move
            double angle = Rotation.Get(state);
            angle = (angle + rotationaVelocity) % (Math.PI * 2);
            Rotation.Set(state, angle);

            Vector2 oldPos = Position.Get(state);
            Vector2 movementVector = new Vector2(velocity, angle).ToCartesian();
            Vector2 newPos = new Vector2(
                (oldPos.X + movementVector.X * delta.TotalSeconds + _mapSize.X) % _mapSize.X,
                (oldPos.Y + movementVector.Y * delta.TotalSeconds + _mapSize.Y) % _mapSize.Y);
            Position.Set(state, newPos);

            Map.MoveEntity(state, oldPos, newPos);
            return Task.CompletedTask;
        }

        public override void Initialize()
        {
            base.Initialize();
            _mapSize = Map.Size;

        }

        #endregion
    }
}