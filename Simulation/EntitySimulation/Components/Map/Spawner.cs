﻿using System;
using EntitySimulation.Objects;
using Utility;
using Utility.RandomHelper;
using Utility.Shapes;
using Random = Utility.Random;

namespace EntitySimulation.Components.Map
{
    public abstract class Spawner : RecurringAction
    {
        #region Fields

        public readonly VariableDefinition<INumberGenerator> Distribution;
        public readonly VariableDefinition<IShape> SpawnArea;
        public readonly VariableDefinition<Random> Rng;

        #endregion

        #region Constructors

        protected Spawner(string prefix) : base(prefix)
        {
            SpawnArea = new VariableDefinition<IShape>(new Circle(), prefix, nameof(SpawnArea));
            Distribution = new VariableDefinition<INumberGenerator>(new RandomGenerator(), prefix, nameof(Distribution));
            Rng = new VariableDefinition<Random>(new Random(), prefix, nameof(Rng));
        }

        #endregion

        #region Properties
        protected EntitySimulation.Map.Map Map { get; private set; }

        #endregion

        #region Methods

        protected abstract bool CanSpawn(ObjectState state);

        protected virtual ObjectState Generate(ObjectState state) => ObjectFactory.GenerateEntity(ObjectHelper.Id.Get(state), ObjectFactory.GetGlobal<int>("seed"));

        protected sealed override void PerformAction(ObjectState state)
        {
            if (!CanSpawn(state)) return;
            ObjectState spawnedObject = Generate(state);
            Vector2 pos = GetPosition(state);
            Map.AddEntity(spawnedObject, pos);
        }

        private Vector2 GetPosition(ObjectState state) => SpawnArea.Get(state).RandomPosition(Rng.Get(state), Distribution.Get(state));

        public override void Initialize()
        {
            Map = ObjectFactory.GetGlobal<EntitySimulation.Map.Map>("map");
        }

        #endregion
    }
}