﻿using EntitySimulation.Objects;
using System;
using System.Threading.Tasks;

namespace EntitySimulation.Components.Map
{
    public class MovementTester : MapComponent
    {
        public static readonly VariableDefinition<double> RotationForce = Movement.RotationForce;
        public static readonly VariableDefinition<double> LinearForce = Movement.LinearForce;
        public static readonly VariableDefinition<double> Rotation = ObjectHelper.Rotation;
        public static readonly VariableDefinition<EMovementStep> MovementStep = new VariableDefinition<EMovementStep>(EMovementStep.Accelerating1);
        public static readonly VariableDefinition<double> Velocity = ObjectHelper.Velocity;
        public static readonly VariableDefinition<double> TargetAngle = new VariableDefinition<double>(double.NaN);
        public enum EMovementStep
        {
            Accelerating1,
            Decelerating,
            PreparingForTurn,
            TurningAround,
            Accelerating2,
            Done
        }
        public override int Identifier => 2044124217;

        public double VelocityTolerance = 0.01;
        public double AngleTolerance = Math.PI / 64;
        public double VelocityMax { get; set; } = 60.0; // in m/s
        
        public override Task Tick(TimeSpan delta, ObjectState state)
        {
            double rotation = Rotation.Get(state);
            EMovementStep step = MovementStep.Get(state);
            double targetRotaion = TargetAngle.Get(state, double.NaN);
            while (step != EMovementStep.Done)
            {
                switch (step)
                {
                    case EMovementStep.Accelerating1:
                        if (VelocityMax - Velocity.Get(state) < VelocityTolerance)
                        {
                            step = EMovementStep.Decelerating;
                            break;
                        }
                        LinearForce.Set(state, 1.0);
                        break;
                    case EMovementStep.Decelerating:
                        if (Velocity.Get(state) < VelocityTolerance)
                        {
                            step = EMovementStep.PreparingForTurn;
                            break;
                        }
                        LinearForce.Set(state, -1.0);
                        break;
                    case EMovementStep.PreparingForTurn:
                        LinearForce.Set(state, 0.0);
                        RotationForce.Set(state, 1.0);
                        targetRotaion = (Rotation.Get(state) - Math.PI) % (Math.PI * 2); // 360° = 2*PI
                        TargetAngle.Set(state, targetRotaion);
                        step = EMovementStep.TurningAround;
                        break;
                    case EMovementStep.TurningAround:
                        double rotationDiff = rotation - targetRotaion;
                        if (Math.Abs(rotationDiff) < AngleTolerance)
                        {
                            step = EMovementStep.Accelerating2;
                            RotationForce.Set(state, 0);
                            break;
                        }
                        double rotationFactor = Math.PI / ((rotationDiff) % Math.PI);
                        RotationForce.Set(state, rotationFactor);
                        break;
                    case EMovementStep.Accelerating2:
                        if (VelocityMax - Velocity.Get(state) < VelocityTolerance)
                        {
                            step = EMovementStep.Done;
                            break;
                        }
                        LinearForce.Set(state, 1.0);
                        break;
                    case EMovementStep.Done:
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            return Task.CompletedTask;
        }

    }
}