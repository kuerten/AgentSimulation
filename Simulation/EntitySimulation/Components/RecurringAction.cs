﻿using System;
using System.Threading.Tasks;
using EntitySimulation.Objects;

namespace EntitySimulation.Components
{
    public abstract class RecurringAction : ComponentBase
    {
        #region Fields

        public readonly VariableDefinition<TimeSpan> Cooldown;
        public readonly VariableDefinition<TimeSpan> Interval;

        #endregion

        #region Constructors

        protected RecurringAction(string prefix)
        {
            Cooldown = new VariableDefinition<TimeSpan>(TimeSpan.Zero, prefix, nameof(Cooldown));
            Interval = new VariableDefinition<TimeSpan>(TimeSpan.FromSeconds(1), prefix, nameof(Interval));
        }

        #endregion

        #region Methods

        protected abstract void PerformAction(ObjectState state);

        public override Task Tick(TimeSpan delta, ObjectState state)
        {
            TimeSpan spawnCooldown = Cooldown.Get(state);
            while (spawnCooldown <= delta)
            {
                delta -= spawnCooldown;
                spawnCooldown = Interval.Get(state);
                PerformAction(state);
            }
            Cooldown.Set(state, spawnCooldown - delta);
            return Task.CompletedTask;
        }

        #endregion
    }
}