﻿using System;
using System.Threading.Tasks;
using EntitySimulation.Objects;

namespace EntitySimulation.Components
{
    public class Metabolism : ComponentBase
    {
        #region Constants

        public static readonly VariableDefinition<double> ConsumptionRate = new VariableDefinition<double>(0.0); // Energy per second
        public static readonly VariableDefinition<double> Energy = ObjectHelper.Energy;

        #endregion

        #region Properties

        public override int Identifier { get; } = 302596119;

        #endregion

        #region Methods

        public override Task Tick(TimeSpan delta, ObjectState state)
        {
            double energy = Energy.Get(state);
            double consumptionRate = ConsumptionRate.Get(state);
            Energy.Set(state, Math.Max(0, energy - consumptionRate * delta.TotalSeconds));
            return Task.CompletedTask;
        }

        #endregion
    }
}