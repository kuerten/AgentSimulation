﻿using EntitySimulation.Networks;
using EntitySimulation.Networks.Fitness;
using EntitySimulation.Networks.Mutation;
using EntitySimulation.Networks.Training;
using EntitySimulation.Objects;
using Newtonsoft.Json;
using Utility;

namespace EntitySimulation.Components.Networks
{
    public class NetworkTrainer : RecurringAction
    {
        #region Constants

        private static readonly string DefaultPrefix = typeof(NetworkTrainer).Name;
        public static readonly VariableDefinition<NetworkDefinition> Network = NeuralNetwork.Network;
        public static readonly VariableDefinition<Random> Rng = new VariableDefinition<Random>(new Random());

        #endregion

        #region Fields

        public readonly VariableDefinition<int> Iteration;

        #endregion

        #region Constructors

        public NetworkTrainer() : this(DefaultPrefix) { }

        public NetworkTrainer(string prefix) : base(prefix)
        {
            Iteration = new VariableDefinition<int>(0, prefix, nameof(Iteration));
        }

        #endregion

        #region Properties

        [JsonProperty(TypeNameHandling = TypeNameHandling.All)]
        public IFitness Fitness { get; set; }

        public IMutator Mutator { get; set; }
        public ITrainer Trainer { get; set; }

        public override int Identifier { get; } = 68479241;

        #endregion

        #region Methods

        protected override void PerformAction(ObjectState state)
        {
            NetworkDefinition network = Network.Get(state).Clone();
            if (Trainer.Train(state, Rng.Get(state), network, Mutator, Fitness)) Network.Set(state, network);
            Iteration.Set(state, Iteration.Get(state) + 1);
        }

        #endregion
    }
}