﻿using System;
using System.Threading.Tasks;
using EntitySimulation.Networks;
using EntitySimulation.Networks.Activation;
using EntitySimulation.Objects;
using Utility;

namespace EntitySimulation.Components.Networks
{
    public class NeuralNetwork : ComponentBase
    {
        #region Constants

        public static readonly VariableDefinition<NetworkDefinition> Network = new VariableDefinition<NetworkDefinition>(null);
        public static readonly VariableDefinition<IActivationFunction> ActivationFunction = new VariableDefinition<IActivationFunction>(new ActivationSigmoid());

        #endregion

        #region Properties

        public override int Identifier { get; } = 1122627734;

        #endregion

        #region Methods

        private static void Compute(ObjectState state)
        {
            NetworkDefinition network = Network.Get(state);
            IActivationFunction activationFunction = ActivationFunction.Get(state);

            foreach (MappedValue inputValue in network.InputMappings)
            {
                double value = state.Get<double>(inputValue.ValueId);
                network.LastState[inputValue.NeuronId] = activationFunction.Execute(value.Clamp(1.0)); // apply to LastState so that changes are applied in this iteration
            }

            foreach (Link link in network.Links) network.State[link.ToNeuronId] += network.LastState[link.FromNeuronId] * link.Weight;

            // Apply activation afterwards so that multiple input links of a neuron can be summarized beforehand.
            for (int i = 0; i < network.State.Length; i++) network.State[i] = activationFunction.Execute(network.State[i]).Clamp(1.0);

            foreach (MappedValue outputValue in network.OutputMappings) state.Set(outputValue.ValueId, network.State[outputValue.NeuronId]);

            network.TrasferState();
        }


        public override Task Tick(TimeSpan delta, ObjectState state)
        {
            Compute(state);
            return Task.CompletedTask;
        }

        #endregion
    }
}