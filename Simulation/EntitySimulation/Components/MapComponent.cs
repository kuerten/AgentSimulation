﻿using EntitySimulation.Objects;

namespace EntitySimulation.Components
{
    public abstract class MapComponent : ComponentBase
    {
        #region Properties

        protected EntitySimulation.Map.Map Map { get; private set; }

        #endregion

        #region Methods


        public override void Initialize()
        {
            Map = ObjectFactory.GetGlobal<EntitySimulation.Map.Map>("map");
        }

        #endregion
    }
}