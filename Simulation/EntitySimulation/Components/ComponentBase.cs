﻿using System;
using System.Threading.Tasks;
using EntitySimulation.Objects;

namespace EntitySimulation.Components
{
    public abstract class ComponentBase : IComponent
    {
        #region Properties

        public abstract int Identifier { get; }

        #endregion

        #region Methods

        public abstract Task Tick(TimeSpan delta, ObjectState state);
        public virtual void Initialize() { }

        #endregion
    }
}