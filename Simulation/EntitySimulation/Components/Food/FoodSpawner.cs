﻿using System.Linq;
using EntitySimulation.Components.Detection;
using EntitySimulation.Components.Map;
using EntitySimulation.Objects;
using Utility.Shapes;

namespace EntitySimulation.Components.Food
{
    public class FoodSpawner : Spawner
    {
        #region Constants

        private static readonly string DefaultPrefix = typeof(FoodSpawner).Name;

        #endregion

        #region Fields

        public readonly VariableDefinition<double> EnergyMaximum;

        public readonly VariableDefinition<int> SpawnType;

        #endregion

        #region Properties
        
        public override int Identifier { get; } = 269548474;

        #endregion

        #region Constructors

        public FoodSpawner() : this(DefaultPrefix) { }

        public FoodSpawner(string prefix) : base(prefix)
        {
            SpawnType = new VariableDefinition<int>(0, prefix, nameof(SpawnType));
            EnergyMaximum = new VariableDefinition<double>(0.0, prefix, nameof(EnergyMaximum));
        }

        #endregion

        #region Methods

        protected override bool CanSpawn(ObjectState state)
        {
            IShape detectionArea = SpawnArea.Get(state);
            int foodType = SpawnType.Get(state);
            double current = DetectorHelper.Detect(Map.Chunks, detectionArea).Where(s => ObjectHelper.Type.Get(s) == foodType).Select(s => ObjectHelper.Energy.Get(s)).Sum();
            double max = EnergyMaximum.Get(state);
            return current < max;
        }

        #endregion
    }
}