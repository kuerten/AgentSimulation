﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntitySimulation.Components.Detection;
using EntitySimulation.Objects;
using Utility;
using Utility.Shapes;

namespace EntitySimulation.Components.Food
{
    public class FoodConsumer : MapComponent
    {
        #region Constants

        public static readonly VariableDefinition<Vector2> Position = ObjectHelper.Position;
        public static readonly VariableDefinition<double> Energy = ObjectHelper.Energy;
        public static readonly VariableDefinition<double> MaxEnergy = new VariableDefinition<double>(500.0);
        public static readonly VariableDefinition<double> ConsumeRadius = new VariableDefinition<double>(1.0);

        #endregion

        #region Properties

        public override int Identifier { get; } = 361709742;

        #endregion

        #region Methods

        public override Task Tick(TimeSpan delta, ObjectState state)
        {
            Vector2 pos = Position.Get(state);
            double consumeRadius = ConsumeRadius.Get(state);
            double currentEnergy = Energy.Get(state);
            double maxEnergy = MaxEnergy.Get(state);
            if (currentEnergy >= maxEnergy) return Task.CompletedTask;

            Dictionary<ObjectState, double> targets = DetectorHelper.Detect(Map.Chunks, new Circle(pos, consumeRadius), new[] {state})
                .Where(e => !ObjectHelper.Consumed.Get(e, true)) // Default is true so that objects without "Consumable" are not targeted
                .ToDictionary(s => s, s => Vector2.Distance(pos, ObjectHelper.Position.Get(s)));
            if (targets.Count == 0) return Task.CompletedTask;

            do
            {
                ObjectState closestObject = null;
                double closestDistance = double.MaxValue;
                foreach ((ObjectState s, double distance) in targets)
                {
                    if (distance >= closestDistance) continue;
                    closestDistance = distance;
                    closestObject = s;
                }
                if (closestObject == null) break;
                currentEnergy += ObjectHelper.Energy.Get(closestObject);
                ObjectHelper.Consumed.Set(closestObject, true);
                targets.Remove(closestObject);
            } while (currentEnergy < maxEnergy);
            Energy.Set(state, currentEnergy);
            return Task.CompletedTask;
        }

        #endregion
    }
}