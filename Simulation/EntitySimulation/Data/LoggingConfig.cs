﻿using System.Collections.Generic;
using System.Linq;
using EntitySimulation.Data.Transmitter;
using EntitySimulation.Data.Transmitter.Interfaces;
using EntitySimulation.Objects;
using Newtonsoft.Json;

namespace EntitySimulation.Data
{
    public enum LoggingType
    {
        Asynchronus,
        Synchronus
    }
    public class LoggingConfig
    {
        #region Constants

        private static readonly JsonSerializerSettings SerializationSettings = new JsonSerializerSettings
        {
            Formatting = Formatting.Indented,
            NullValueHandling = NullValueHandling.Ignore,
            TypeNameHandling = TypeNameHandling.Auto
        };

        #endregion

        #region Properties

        public static LoggingConfig CurrentConfig { get; set; } = new LoggingConfig();
        public List<DataDefinition> LoggingDefinition { get; } = new List<DataDefinition>();
        public List<int> IgnoredTypes { get; } = new List<int>();
        public string[] DefaultLoggedValues { get; set; } =
        {
            ObjectHelper.Position.Name,
            ObjectHelper.Rotation.Name
        };

        public ITransmitter Transmitter { get; set; } = new AsyncUdpTransmitter();
        #endregion

        #region Methods

        public string[] GetLoggedValues(int typeId) => LoggingDefinition.FirstOrDefault(x => x.TypeId == typeId)?.LoggedValues.ToArray() ?? DefaultLoggedValues;

        public string Serialize() => JsonConvert.SerializeObject(this, SerializationSettings);
        public static LoggingConfig Parse(string s) => JsonConvert.DeserializeObject<LoggingConfig>(s, SerializationSettings);

        #endregion
    }

    public class DataDefinition
    {
        #region Properties

        public int TypeId { get; set; }
        public List<string> LoggedValues { get; set; } = new List<string>();

        #endregion
    }
}