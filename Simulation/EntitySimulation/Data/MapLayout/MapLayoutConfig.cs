﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Utility;

namespace EntitySimulation.Data.MapLayout
{
    [DataContract, Serializable]
    public class MapLayoutConfig
    {
        #region Constants

        private static readonly JsonSerializerSettings SerializationSettings = new JsonSerializerSettings
        {
            Formatting = Formatting.Indented,
            TypeNameHandling = TypeNameHandling.Auto
        };

        #endregion

        #region Properties

        [DataMember] public Vector2Int ChunkCount { get; set; } = new Vector2Int(5, 5);
        [DataMember] public Vector2 ChunkSize { get; set; } = new Vector2(100, 100);
        public Vector2 MapSize => new Vector2(ChunkSize.X * ChunkCount.X, ChunkSize.Y * ChunkCount.Y);
        [DataMember] public List<ISpawnDefinition> SpawnDefinitions { get; } = new List<ISpawnDefinition>();

        #endregion

        #region Methods

        public void Populate(Map.Map map, int seed) => SpawnDefinitions.ForEach(x => x.Populate(map, seed));

        public string Serialize() => JsonConvert.SerializeObject(this, SerializationSettings);
        public static MapLayoutConfig Parse(string s) => JsonConvert.DeserializeObject<MapLayoutConfig>(s, SerializationSettings);

        #endregion
    }
}