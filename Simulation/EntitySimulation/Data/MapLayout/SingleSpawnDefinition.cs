﻿using EntitySimulation.Objects;
using Utility;

namespace EntitySimulation.Data.MapLayout
{
    public class SingleSpawnDefinition : ISpawnDefinition
    {
        #region Properties

        public int ObjectId { get; set; }
        public Vector2 Position { get; set; }

        #endregion

        #region Methods

        public void Populate(Map.Map map, int seed)
        {
            ObjectState spawner = ObjectFactory.GenerateEntity(ObjectId, seed);
            map.AddEntity(spawner, Position);
        }

        #endregion
    }
}