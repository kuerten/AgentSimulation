﻿namespace EntitySimulation.Data.MapLayout
{
    public interface ISpawnDefinition
    {
        void Populate(Map.Map map, int seed);
    }
}