﻿using System;
using EntitySimulation.Objects;
using Utility.RandomHelper;
using Utility.Shapes;
using Random = Utility.Random;

namespace EntitySimulation.Data.MapLayout
{
    public class MultiSpawnDefinition : ISpawnDefinition
    {
        #region Properties

        public int ObjectId { get; set; }
        public IShape SpawnArea { get; set; }
        public INumberGenerator Distribution { get; set; }
        public int Amount { get; set; }
        public bool RandomizeRotation { get; set; }

        #endregion

        #region Methods

        public void Populate(Map.Map map, int seed)
        {
            Random rng = new Random(seed);
            for (int i = 0; i < Amount; i++)
            {
                ObjectState entity = ObjectFactory.GenerateEntity(ObjectId, seed);
                if (RandomizeRotation) ObjectHelper.Rotation.Set(entity, rng.NextDouble() * 2 * Math.PI);
                map.AddEntity(entity, SpawnArea.RandomPosition(rng, Distribution));
            }
        }

        #endregion
    }
}