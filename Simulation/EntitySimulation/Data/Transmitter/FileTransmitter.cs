﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EntitySimulation.Data.Transmitter.Interfaces;
using EntitySimulation.Serialization;
using Newtonsoft.Json;
using JsonConverter = EntitySimulation.Serialization.JsonConverter;

namespace EntitySimulation.Data.Transmitter
{
    public class FileTransmitter : ISyncTransmitter, IAsyncTransmitter
    {
        #region Constants

        public const string DataFileName = "data.txt";
        public const string ConfigFileName = "config.json";
        public static string EntrySeparator = Environment.NewLine;

        #endregion

        #region Fields

        private readonly object _lockObject = new object();
        private readonly TimeSpan _minDelay = TimeSpan.FromMilliseconds(50);
        private Queue<byte[]> _backDataBuffer = new Queue<byte[]>();
        private string _currentDirectory;
        private int _currentTick;
        private Queue<byte[]> _dataBuffer = new Queue<byte[]>();
        private DateTime _lastCall;
        private Task _processingTask;
        private CancellationTokenSource _tokenSource;
        private string _overridePath = string.Empty;
        #endregion

        #region Properties

        public string DirectoryBasePath { get; set; } = Path.Combine(Directory.GetCurrentDirectory(), "Data");
        [JsonIgnore] public bool Running { get; private set; }
        public int TransmissionInterval { get; set; } = 20;
        [JsonIgnore] public Map.Map Map { get; set; }
        [JsonIgnore] public long Cycle { get; private set; }

        #endregion

        #region Methods

        public void Start()
        {
            if (Running) return;
            _tokenSource = new CancellationTokenSource();
            CancellationToken token = _tokenSource.Token;
            _lastCall = DateTime.Now;
            _processingTask = Task.Run(() => ProcessData(token), token);
            Running = true;
        }

        public void Stop()
        {
            if (!Running) return;
            _tokenSource.Cancel();
            _processingTask.Wait();
            Running = false;
        }

        private async Task ProcessData(CancellationToken token)
        {
            // ReSharper disable MethodSupportsCancellation
            while (!token.IsCancellationRequested)
            {
                DateTime now = DateTime.Now;
                TimeSpan diff = now - _lastCall;
                if (diff < _minDelay)
                {
                    await Task.Delay(_minDelay - diff);
                    continue;
                }
                _lastCall = now;

                lock (_lockObject)
                {
                    Queue<byte[]> temp = _dataBuffer;
                    _backDataBuffer = _dataBuffer;
                    _dataBuffer = temp;
                }

                if (_backDataBuffer.Count == 0) continue;
                if (string.IsNullOrEmpty(_currentDirectory))
                {
                    Console.WriteLine("No directory to write, entry will be skipped");
                    break;
                }
                await using FileStream fs = File.Open(Path.Combine(_currentDirectory, DataFileName), FileMode.Append, FileAccess.Write);
                while (_backDataBuffer.Count > 0)
                {
                    await fs.WriteAsync(_backDataBuffer.Dequeue());
                    await fs.WriteAsync(Encoding.UTF8.GetBytes(EntrySeparator));
                }
                _dataBuffer.Clear();
            }
            // ReSharper restore MethodSupportsCancellation
        }

        public void Dispose() => Stop();

        public async Task Update()
        {
            if (++_currentTick < TransmissionInterval) return;
            _currentTick = 0;
            await SendMapData();
            ++Cycle;
        }

        public Task SendReset()
        {
            _currentDirectory = !string.IsNullOrEmpty(_overridePath) ? 
                _overridePath : 
                Path.Combine(DirectoryBasePath, DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
            Directory.CreateDirectory(_currentDirectory);
            return Task.CompletedTask;
        }

        public Task SendConfig(SimulationInformation data)
        {
            JsonConverter.Serialize(Path.Combine(_currentDirectory, ConfigFileName), data);
            return Task.CompletedTask;
        }

        public Task SendMapData()
        {
            foreach (byte[] message in TransmitterBase.GatherData(Map, Cycle)) _dataBuffer.Enqueue(message);
            return Task.CompletedTask;
        }

        public void OverrideLoggingPath(string newPath)
        {
            _overridePath = newPath;
        }

        #endregion
    }
}