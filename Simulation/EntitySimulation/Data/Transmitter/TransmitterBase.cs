﻿using EntitySimulation.Data.Transmitter.Interfaces;
using EntitySimulation.Objects;
using EntitySimulation.Serialization;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitySimulation.Data.Transmitter
{
    public enum EMessageType
    {
        Delete,
        Update,
        Reset,
        Config
    }

    public abstract class TransmitterBase : ITransmitter
    {
        #region Properties

        [JsonIgnore] public Map.Map Map { get; set; }

        [JsonIgnore] public long Cycle { get; protected set; }

        #endregion

        #region Methods

        public async Task SendReset() => await WriteData(GenerateReset());
        public async Task SendConfig(SimulationInformation data) => await WriteData(GenerateConfig(data));
        public async Task SendMapData() => await Task.WhenAll(GatherData(Map, Cycle).Select(WriteData));
        
        protected static byte[] GenerateMessage(object o) => Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(o));
        protected static byte[] GenerateUpdate(ObjectState obj, long cycle)
        {
            int type = ObjectHelper.Type.Get(obj);
            if (LoggingConfig.CurrentConfig.IgnoredTypes.Contains(type)) return null;
            Dictionary<string, object> data = new Dictionary<string, object>
            {
                ["MessageType"] = EMessageType.Update,
                ["Cycle"] = cycle,
                ["Id"] = ObjectHelper.Id.Get(obj),
                ["DataType"] = type
            };
            foreach (string id in LoggingConfig.CurrentConfig.GetLoggedValues(type)) data[id] = obj.Get(id);
            return GenerateMessage(data);
        }
        protected static byte[] GenerateDelete(ObjectState obj, long cycle)
        {
            int type = ObjectHelper.Type.Get(obj);
            if (LoggingConfig.CurrentConfig.IgnoredTypes.Contains(type)) return null;
            Dictionary<string, object> data = new Dictionary<string, object>
            {
                ["MessageType"] = EMessageType.Delete,
                ["Cycle"] = cycle,
                ["Id"] = ObjectHelper.Id.Get(obj),
                ["DataType"] = type
            };
            return GenerateMessage(data);
        }
        protected static byte[] GenerateReset()
        {
            Dictionary<string, object> data = new Dictionary<string, object>
            {
                ["MessageType"] = EMessageType.Reset
            };
            return GenerateMessage(data);
        }
        protected static byte[] GenerateConfig(SimulationInformation data)
        {
            Dictionary<string, object> message = new Dictionary<string, object>
            {
                ["MessageType"] = EMessageType.Config,
                ["Config"] = data
            };
            return GenerateMessage(message);
        }

        public static byte[][] GatherData(Map.Map map, long cycle)
        {
            map.LockMap();
            byte[][] data = map.Chunks
                .SelectMany(c => c.Objects.Where(x => x.RetrieveDirty()))
                .Select(x => GenerateUpdate(x, cycle))
                .Concat(map.RetrieveConsumed().Select(x => GenerateDelete(x, cycle)))
                .Where(x => x != null)
                .ToArray();
            map.UnlockMap();
            return data;
        }

        protected abstract Task WriteData(byte[] data);

        public abstract void Dispose();

        #endregion
    }
}