﻿using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace EntitySimulation.Data.Transmitter
{
    public abstract class UdpTransmitter : TransmitterBase
    {
        #region Fields

        private readonly UdpClient _client = new UdpClient();
        private readonly IPEndPoint _endpoint;

        #endregion

        #region Constructors

        protected UdpTransmitter()
        {
            IPAddress multicastAddress = IPAddress.Parse("239.0.1.2");
            _endpoint = new IPEndPoint(multicastAddress, 12345);
        }

        #endregion

        #region Methods

        protected override async Task WriteData(byte[] data) => await _client.SendAsync(data, data.Length, _endpoint);

        public override void Dispose()
        {
            _client?.Dispose();
        }
        #endregion
    }
}