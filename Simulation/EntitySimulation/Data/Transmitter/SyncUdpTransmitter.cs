﻿using System.Threading.Tasks;
using EntitySimulation.Data.Transmitter.Interfaces;

namespace EntitySimulation.Data.Transmitter
{
    public class SyncUdpTransmitter : UdpTransmitter, ISyncTransmitter
    {
        #region Fields

        private int _currentTick;

        #endregion

        #region Properties

        public int TransmissionInterval { get; set; } = 20;

        #endregion

        #region Methods

        public async Task Update()
        {
            if (++_currentTick < TransmissionInterval) return;
            _currentTick = 0;
            await SendMapData();
            ++Cycle;
        }

        #endregion
    }
}