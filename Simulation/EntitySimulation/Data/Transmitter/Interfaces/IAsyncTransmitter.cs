﻿namespace EntitySimulation.Data.Transmitter.Interfaces
{
    public interface IAsyncTransmitter : ITransmitter
    {
        void Start();
        void Stop();
    }
}