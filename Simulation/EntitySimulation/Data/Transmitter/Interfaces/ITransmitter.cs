﻿using System;
using System.Threading.Tasks;
using EntitySimulation.Serialization;

namespace EntitySimulation.Data.Transmitter.Interfaces
{
    public interface ITransmitter : IDisposable
    {
        Map.Map Map { get; set; }
        long Cycle { get; }
        Task SendReset();
        Task SendConfig(SimulationInformation data);
        Task SendMapData();
    }
}