﻿using System.Threading.Tasks;

namespace EntitySimulation.Data.Transmitter.Interfaces
{
    public interface ISyncTransmitter : ITransmitter
    {
        Task Update();
    }
}