﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using EntitySimulation.Data.Transmitter.Interfaces;
using Newtonsoft.Json;
using Timer = System.Timers.Timer;

namespace EntitySimulation.Data.Transmitter
{
    public class AsyncUdpTransmitter : UdpTransmitter, IAsyncTransmitter
    {
        #region Fields

        private readonly ManualResetEvent _resetEvent = new ManualResetEvent(false);
        private readonly Timer _stateTimer;
        private double _interval = 1000 / 30.0; // 30 hz
        private Task _sendTask;
        private volatile int _timerCalls;

        #endregion

        #region Constructors

        public AsyncUdpTransmitter()
        {
            _stateTimer = new Timer(_interval);
            _stateTimer.Elapsed += StateTimerOnElapsed;
        }

        #endregion

        #region Properties

        [JsonIgnore] public bool Running { get; private set; }

        /// <summary>
        ///     Updates per second.
        /// </summary>
        public double Heartbeat
        {
            get => 1000.0 / _interval;
            set
            {
                value = 1000.0 / value;
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                if (value == _interval) return;
                _interval = value;
                _stateTimer.Interval = value;
            }
        }

        #endregion

        #region Methods

        private void StateTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            _timerCalls++;
            _resetEvent.Set();
        }

        public void Start()
        {
            if (Running) return;
            _timerCalls = 0;

            Running = true;
            _sendTask = Task.Run(SendRoutine);
            _stateTimer.Start();
        }

        public void Stop()
        {
            if (!Running) return;
            Running = false;
            _sendTask.Wait();
            _stateTimer.Stop();
        }

        public async Task SendRoutine()
        {
            while (Running)
            {
                _resetEvent.WaitOne();
                _timerCalls--;
                _resetEvent.Reset();
                if (_timerCalls != 0)
                {
                    Console.WriteLine($"Warning: Transmitter routine is behind by {_timerCalls} Ticks ({_timerCalls * _interval:F2}ms), skipping Tick");
                    _timerCalls = 0;
                    continue;
                }

                await SendMapData();
                ++Cycle;
            }
        }

        public override void Dispose()
        {
            Stop();
            base.Dispose();
        }

        #endregion
    }
}