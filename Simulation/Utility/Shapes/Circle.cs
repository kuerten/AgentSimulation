﻿using System;
using Newtonsoft.Json;

namespace Utility.Shapes
{
    public struct Circle : IShape
    {
        public double Radius { get; set; }
        public Vector2 Center { get; set; }
        [JsonIgnore]
        public double Diameter => Radius * 2;
        public Circle(Vector2 center, double radius)
        {
            Radius = radius;
            Center = center;
        }

        public bool IsInBounds(Vector2 position) => (position - Center).Magnitude <= Radius;

        public bool IsOverlapping(IShape other) =>
            other switch
            {
                Circle circle => GeometryHelper.CheckOverlap(this, circle),
                Rect rect => GeometryHelper.CheckOverlap(rect, this),
                _ => throw new ArgumentException("Cannot compare shapes", nameof(other)),
            };

        public override string ToString() => $"Center:{Center}, Radius:{Radius}";
    }
}