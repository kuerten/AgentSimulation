﻿namespace Utility.Shapes
{
    public interface IShape
    {
        bool IsInBounds(Vector2 position);

        bool IsOverlapping(IShape other);
    }
}