﻿using System;
using Newtonsoft.Json;

namespace Utility.Shapes
{
    public struct Rect : IShape
    {
        public Vector2 TopLeft { get; set; }
        public Vector2 BottomRight { get; set; }

        public Rect(Vector2 topLeft, Vector2 bottomRight)
        {
            TopLeft = topLeft;
            BottomRight = bottomRight;
        }
        public Rect(double x1, double x2, double y1, double y2) : this(new Vector2(x1, y1), new Vector2(x2, y2)) { }
        
        [JsonIgnore]
        public double Left => TopLeft.X;
        [JsonIgnore]
        public double Right => BottomRight.X;
        [JsonIgnore]
        public double Top => TopLeft.Y;
        [JsonIgnore]
        public double Bottom => BottomRight.Y;
        [JsonIgnore]
        public double Width => Right - Left;
        [JsonIgnore]
        public double Height => Bottom - Top;

        [JsonIgnore]
        public Vector2 Center => TopLeft + new Vector2(Width / 2, Height / 2);

        public bool IsInBounds(Vector2 pos) => pos.X >= Left && pos.X <= Right && pos.Y >= Top && pos.Y <= Bottom;

        public bool IsOverlapping(IShape other) =>
            other switch
            {
                Circle circle => GeometryHelper.CheckOverlap(this, circle),
                Rect rect => GeometryHelper.CheckOverlap(rect, this),
                _ => throw new ArgumentException("Cannot compare shapes", nameof(other)),
            };

        public override string ToString() => $"TopLeft:{TopLeft}, Size:{BottomRight - TopLeft}";
    }
}