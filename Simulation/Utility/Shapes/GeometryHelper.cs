﻿using System;
using Utility.RandomHelper;

namespace Utility.Shapes
{
    public static class GeometryHelper
    {
        public static bool CheckOverlap(Rect a, Circle b)
        {
            Vector2 closestPoint = new Vector2(Math.Clamp(b.Center.X, a.Left, a.Right), Math.Clamp(b.Center.Y, a.Top, a.Bottom));
            return (closestPoint - b.Center).Magnitude <= b.Radius;
        }

        public static bool CheckOverlap(Rect a, Rect b) => !(b.Left > a.Right || b.Right < a.Left || b.Top > a.Bottom || b.Bottom < a.Top);

        public static bool CheckOverlap(Circle a, Circle b) => Vector2.Distance(a.Center, b.Center) <= a.Radius + b.Radius;

        /// <summary>
        /// Returns a random position that is inside the <paramref name="shape"/>
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="rng"></param>
        /// <param name="distribution"></param>
        /// <returns></returns>
        public static Vector2 RandomPosition(this IShape shape, Random rng, INumberGenerator distribution)
        {
            return shape switch
            {
                Circle circle => circle.Center + new Vector2(Math.Abs(distribution.Generate(rng, circle.Radius, -circle.Radius)), rng.NextDouble() * Math.PI * 2).ToCartesian(),// Randomizing Angle based on distirbution makes not much sense here
                Rect rect => rect.TopLeft + new Vector2(distribution.Generate(rng) * rect.Width, distribution.Generate(rng) * rect.Height),
                _ => throw new NotSupportedException("Shape not supported"),
            };
        }
    }
}