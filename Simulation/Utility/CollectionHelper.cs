﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utility
{
    public static class CollectionHelper
    {
        public static TValue GetOrCreate<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, Func<TValue> valueFunc)
        {
            lock (dict)
            {
                if (dict.TryGetValue(key, out TValue val)) return val;
                val = valueFunc();
                dict.Add(key, val);
                return val;
            }
        }
        public static TValue GetOrCreate<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key) where TValue : new()
        {
            lock (dict)
            {
                if (dict.TryGetValue(key, out TValue val)) return val;
                val = new TValue();
                dict.Add(key, val);
                return val;
            }
        }

        public static int RemoveAll<TKey, TValue>(this IDictionary<TKey, TValue> dict, Func<TValue, bool> predicate)
        {
            TKey[] keys = dict.Keys.Where(k => predicate(dict[k])).ToArray();
            foreach (TKey key in keys) dict.Remove(key);
            return keys.Length;
        }
    }
}