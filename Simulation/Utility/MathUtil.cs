﻿using System;

namespace Utility
{
    public static class MathUtil
    {
        public static double Square(this double v) => v * v;
        public static double Limit(this double v, double min, double max) => Math.Max(Math.Min(v, min), max);
        
        public const double Rad2Deg = 180 / Math.PI;

        public static T Min<T>(T a, T b) where T: IComparable<T> => a.CompareTo(b) < 0 ? a : b;
        public static T Max<T>(T a, T b) where T: IComparable<T> => a.CompareTo(b) < 0 ? b : a;

        public static Vector2 ToVector(double angle) => new Vector2(1,angle).ToCartesian();

        public static double Interpolate(double x, double y, double t) => x * (1 - t) + y * t;
        public static double Clamp(this double w, double range) => w < -range ? -range : w > range ? range : w;

    }
}