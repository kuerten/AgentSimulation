﻿namespace Utility
{
    /// <summary>
    /// The value validator interface. Based on <see href="https://github.com/Catel/Catel/blob/develop/src/Catel.Core/Data/Interfaces/IValueValidator.cs">this</see> implementation.
    /// </summary>
    /// <typeparam name="TValue">The type of the value</typeparam>
    public interface IValueValidator<in TValue>
    {
        #region Methods

        /// <summary>
        /// Determines whether the specified value is valid.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if is valid, otherwise <c>false</c>.
        /// </returns>
        bool IsValid(TValue @value);
        #endregion
    }
}