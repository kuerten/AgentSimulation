﻿namespace Utility.RandomHelper
{
    public class RandomGenerator : INumberGenerator
    {
        #region Methods

        public double Generate(Random rng) => rng.NextDouble();

        public object Clone() => new RandomGenerator();

        #endregion
    }
}