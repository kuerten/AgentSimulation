﻿namespace Utility.RandomHelper
{
    public static class NumberGeneratorExtensions
    {
        #region Methods

        public static double Generate(this INumberGenerator s, Random rng, double max) => s.Generate(rng) * max;
        public static double Generate(this INumberGenerator s, Random rng, double max, double min) => min + s.Generate(rng) * (max - min);

        #endregion
    }
}