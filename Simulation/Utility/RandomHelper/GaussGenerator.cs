﻿using System;

namespace Utility.RandomHelper
{
    public class GaussGenerator : INumberGenerator
    {
        #region Properties

        public double Mu { get; set; } = 0.5;
        public double Sigma { get; set; } = 1;

        #endregion

        #region Methods

        public double Generate(Random rng) => Math.Clamp(rng.NextGaussian(Mu, Sigma), 0.0, 1.0);

        public object Clone() => new GaussGenerator {Mu = Mu, Sigma = Sigma};

        #endregion
    }
}