﻿using System;

namespace Utility.RandomHelper
{
    public interface INumberGenerator : ICloneable
    {
        /// <summary>
        /// Generates a value between 0 and 1.
        /// </summary>
        /// <param name="rng"></param>
        /// <returns></returns>
        double Generate(Random rng);
    }
}