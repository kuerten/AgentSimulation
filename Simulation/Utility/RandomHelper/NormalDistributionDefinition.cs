﻿namespace Utility.RandomHelper
{
    public struct NormalDistributionDefinition
    {
        public double Mu;
        public double Sigma;
        public NormalDistributionDefinition(double mu, double sigma)
        {
            Mu = mu;
            Sigma = sigma;
        }
    }
}