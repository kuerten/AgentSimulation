﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Utility.RandomHelper
{
    /// <summary>
    /// This class has been adapted from <see href="https://bitbucket.org/Superbest/superbest-random/src/master/Superbest%20random/RandomExtensions.cs">this</see> File.
    /// </summary>
    public static class RandomExtensions
    {
        /// <summary>
        /// Generates normally distributed numbers. Each operation makes two Gaussians for the price of one, and apparently they can be cached or something for better performance, but who cares.
        /// </summary>
        /// <param name="r"></param>
        /// <param name = "mu">Mean of the distribution</param>
        /// <param name = "sigma">Standard deviation</param>
        /// <returns></returns>
        public static double NextGaussian(this Random r, double mu = 0, double sigma = 1)
        {
            double u1 = r.NextDouble();
            double u2 = r.NextDouble();
            double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
            double randNormal = mu + sigma * randStdNormal;
            return randNormal;
        }

        /// <summary>
        ///   Generates values from a triangular distribution.
        /// </summary>
        /// <remarks>
        /// See http://en.wikipedia.org/wiki/Triangular_distribution for a description of the triangular probability distribution and the algorithm for generating one.
        /// </remarks>
        /// <param name="r"></param>
        /// <param name = "a">Minimum</param>
        /// <param name = "b">Maximum</param>
        /// <param name = "c">Mode (most frequent value)</param>
        /// <returns></returns>
        public static double NextTriangular(this Random r, double a, double b, double c)
        {
            double u = r.NextDouble();
            return u < (c - a) / (b - a)
                ? a + Math.Sqrt(u * (b - a) * (c - a))
                : b - Math.Sqrt((1 - u) * (b - a) * (b - c));
        }

        /// <summary>
        /// Equally likely to return true or false. Uses <see cref="NEATSimulator.Random.Next()"/>.
        /// </summary>
        /// <returns></returns>
        public static bool NextBoolean(this Random r) => r.Next(2) > 0;

        /// <summary>
        /// Shuffles a list in O(n) time by using the Fisher-Yates/Knuth algorithm.
        /// </summary>
        /// <param name="r"></param>
        /// <param name = "list"></param>
        public static void Shuffle(this Random r, IList list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                int j = r.Next(0, i + 1);
                object temp = list[j];
                list[j] = list[i];
                list[i] = temp;
            }
        }

        /// <summary>
        /// Returns n unique random numbers in the range [1, n], inclusive. 
        /// This is equivalent to getting the first n numbers of some random permutation of the sequential numbers from 1 to max. 
        /// Runs in O(k^2) time.
        /// </summary>
        /// <param name="rand"></param>
        /// <param name="n">Maximum number possible.</param>
        /// <param name="k">How many numbers to return.</param>
        /// <returns></returns>
        public static int[] Permutation(this Random rand, int n, int k)
        {
            List<int> result = new List<int>();
            SortedSet<int> sorted = new SortedSet<int>();

            for (int i = 0; i < k; i++)
            {
                int r = rand.Next(1, n + 1 - i);

                foreach (int q in sorted)
                {
                    if (r >= q) r++;
                }

                result.Add(r);
                sorted.Add(r);
            }

            return result.ToArray();
        }

        public static double NextDouble(this System.Random rng, double min, double max) => rng.NextDouble() * (max - min) + min;
        public static double NextDouble(this Random rng, double min, double max) => rng.NextDouble() * (max - min) + min;

        public static T SelectRandom<T>(this IList<T> list, Random rng) => list[rng.Next(0, list.Count)];
        public static T SelectRandom<T>(this IEnumerable<T> list, Random rng) => (list as IList<T> ?? list.ToArray()).SelectRandom(rng);

    }
}