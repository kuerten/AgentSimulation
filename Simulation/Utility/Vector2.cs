﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Utility
{
    [DataContract]
    public struct Vector2 : IEquatable<Vector2>, IFormattable
    {
        #region Constants

        public const double KEpsilon = 0.00001;
        public const double KEpsilonNormalSqrt = 1e-15;

        #endregion

        #region Constructors

        public Vector2(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        #endregion

        #region Properties
        [DataMember] public double X { get; set; }

        [DataMember] public double Y { get; set; }

        /// <summary>
        ///     Access the <see cref="X" /> or <see cref="Y" /> component using [0] or [1] respectively.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public double this[int index]
        {
            get =>
                index switch
                {
                    0 => X,
                    1 => Y,
                    _ => throw new IndexOutOfRangeException("Invalid Vector2 index!"),
                };

            set
            {
                switch (index)
                {
                    case 0:
                        X = value;
                        break;
                    case 1:
                        Y = value;
                        break;
                    default:
                        throw new IndexOutOfRangeException("Invalid Vector2 index!");
                }
            }
        }

        /// <summary>
        ///     Returns this vector with a <see cref="Magnitude" /> of 1 (RO).
        /// </summary>
        [JsonIgnore]
        public Vector2 Normalized
        {
            get
            {
                Vector2 v = new Vector2(X, Y);
                v.Normalize();
                return v;
            }
        }

        /// <summary>
        ///     Returns the length of this vector (RO).
        /// </summary>
        [JsonIgnore]
        public double Magnitude => Math.Sqrt(X * X + Y * Y);

        /// <summary>
        ///     Returns the squared length of this vector (RO).
        /// </summary>
        [JsonIgnore]
        public double SqrMagnitude => X * X + Y * Y;


        /// <summary>
        ///     Shorthand for writing <c>Vector2(0, 0)</c>.
        /// </summary>
        public static Vector2 Zero { get; } = new Vector2(0.0, 0.0);

        /// <summary>
        ///     Shorthand for writing <c>Vector2(1, 1)</c>
        /// </summary>
        public static Vector2 One { get; } = new Vector2(1.0, 1.0);

        /// <summary>
        ///     Shorthand for writing <c>Vector2(0, 1)</c>
        /// </summary>
        public static Vector2 Up { get; } = new Vector2(0.0, 1.0);

        /// <summary>
        ///     Shorthand for writing <c>Vector2(0, -1)</c>
        /// </summary>
        public static Vector2 Down { get; } = new Vector2(0.0, -1.0);

        /// <summary>
        ///     Shorthand for writing <c>Vector2(-1, 0)</c>
        /// </summary>
        public static Vector2 Left { get; } = new Vector2(-1.0, 0.0);

        /// <summary>
        ///     Shorthand for writing <c>Vector2(1, 0)</c>
        /// </summary>
        public static Vector2 Right { get; } = new Vector2(1.0, 0.0);

        /// <summary>
        ///     Shorthand for writing <c>Vector2(double.PositiveInfinity, double.PositiveInfinity)</c>
        /// </summary>
        public static Vector2 PositiveInfinity { get; } = new Vector2(double.PositiveInfinity, double.PositiveInfinity);

        /// <summary>
        ///     Shorthand for writing <c>Vector2(double.NegativeInfinity, double.NegativeInfinity)</c>
        /// </summary>
        public static Vector2 NegativeInfinity { get; } = new Vector2(double.NegativeInfinity, double.NegativeInfinity);


        #endregion

        #region Methods

        /// <summary>
        ///     Set x and y components of an existing Vector2.
        /// </summary>
        /// <param name="newX"></param>
        /// <param name="newY"></param>
        public void Set(double newX, double newY)
        {
            X = newX;
            Y = newY;
        }

        /// <summary>
        ///     Linearly interpolates between two vectors.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static Vector2 Lerp(Vector2 a, Vector2 b, double t) => LerpUnclamped(a, b, Math.Clamp(t, 0.0, 1.0));

        /// <summary>
        ///     Linearly interpolates between two vectors without clamping the interpolant
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static Vector2 LerpUnclamped(Vector2 a, Vector2 b, double t) => new Vector2(a.X + (b.X - a.X) * t, a.Y + (b.Y - a.Y) * t);

        /// <summary>
        ///     Moves a point <paramref name="current" /> towards <paramref name="target" />.
        /// </summary>
        /// <param name="current"></param>
        /// <param name="target"></param>
        /// <param name="maxDistanceDelta"></param>
        /// <returns></returns>
        public static Vector2 MoveTowards(Vector2 current, Vector2 target, double maxDistanceDelta)
        {
            double toVectorX = target.X - current.X;
            double toVectorY = target.Y - current.Y;

            double sqDist = toVectorX * toVectorX + toVectorY * toVectorY;

            if (sqDist == 0 || maxDistanceDelta >= 0 && sqDist <= maxDistanceDelta * maxDistanceDelta)
                return target;

            double dist = Math.Sqrt(sqDist);

            return new Vector2(current.X + toVectorX / dist * maxDistanceDelta,
                current.Y + toVectorY / dist * maxDistanceDelta);
        }

        /// <summary>
        ///     Multiplies two vectors component-wise.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector2 Scale(Vector2 a, Vector2 b) => new Vector2(a.X * b.X, a.Y * b.Y);

        /// <summary>
        ///     Multiplies every component of this vector by the same component of <paramref name="scale" />.
        /// </summary>
        /// <param name="scale"></param>
        public void Scale(Vector2 scale)
        {
            X *= scale.X;
            Y *= scale.Y;
        }

        /// <summary>
        ///     Makes this vector have <see cref="Magnitude" /> of 1.
        /// </summary>
        public void Normalize()
        {
            double mag = Magnitude;
            if (mag > KEpsilon)
                this /= mag;
            else
                this = Zero;
        }

        public override string ToString() => ToString(null, CultureInfo.InvariantCulture.NumberFormat);

        /// <summary>
        ///     Returns a nicely formatted string for this vector.
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public string ToString(string format) => ToString(format, CultureInfo.InvariantCulture.NumberFormat);

        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode() => X.GetHashCode() ^ (Y.GetHashCode() << 2);


        public override bool Equals(object other) => other is Vector2 o && Equals(o);

        public static Vector2 Reflect(Vector2 inDirection, Vector2 inNormal)
        {
            double factor = -2 * Dot(inNormal, inDirection);
            return new Vector2(factor * inNormal.X + inDirection.X, factor * inNormal.Y + inDirection.Y);
        }

        public static Vector2 Perpendicular(Vector2 inDirection) => new Vector2(-inDirection.Y, inDirection.X);

        /// <summary>
        ///     Dot Product of two vectors.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static double Dot(Vector2 lhs, Vector2 rhs) => lhs.X * rhs.X + lhs.Y * rhs.Y;

        /// <summary>
        ///     Returns the angle in degrees between <paramref name="from" /> and <paramref name="to" />.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static double Angle(Vector2 from, Vector2 to)
        {
            // sqrt(a) * sqrt(b) = sqrt(a * b) -- valid for real numbers
            double denominator = Math.Sqrt(from.SqrMagnitude * to.SqrMagnitude);
            if (denominator < KEpsilonNormalSqrt) return 0.0;

            double dot = Math.Clamp(Dot(from, to) / denominator, -1.0, 1.0);
            return Math.Acos(dot) * MathUtil.Rad2Deg;
        }

        /// <summary>
        ///     Returns the signed angle in degrees between <paramref name="from" /> and <paramref name="to" />.
        ///     Always returns the smallest possible angle.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static double SignedAngle(Vector2 from, Vector2 to)
        {
            double unsignedAngle = Angle(from, to);
            double sign = Math.Sign(from.X * to.Y - from.Y * to.X);
            return unsignedAngle * sign;
        }

        /// <summary>
        ///     Returns the distance between <paramref name="a" /> and <paramref name="b" />.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double Distance(Vector2 a, Vector2 b)
        {
            double diffX = a.X - b.X;
            double diffY = a.Y - b.Y;
            return Math.Sqrt(diffX * diffX + diffY * diffY);
        }

        /// <summary>
        ///     Returns a copy of <paramref name="vector" /> with its magnitude clamped to <paramref name="maxLength" />.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static Vector2 ClampMagnitude(Vector2 vector, double maxLength)
        {
            double sqrMagnitude = vector.SqrMagnitude;
            if (!(sqrMagnitude > maxLength * maxLength)) return vector;
            double mag = Math.Sqrt(sqrMagnitude);

            //these intermediate variables force the intermediate result to be
            //of double precision. without this, the intermediate result can be of higher
            //precision, which changes behavior.
            double normalizedX = vector.X / mag;
            double normalizedY = vector.Y / mag;
            return new Vector2(normalizedX * maxLength, normalizedY * maxLength);
        }

        /// <summary>
        ///     Returns a vector that is made from the smallest components of two vectors.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static Vector2 Min(Vector2 lhs, Vector2 rhs) => new Vector2(Math.Min(lhs.X, rhs.X), Math.Min(lhs.Y, rhs.Y));

        /// <summary>
        ///     Returns a vector that is made from the largest components of two vectors.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static Vector2 Max(Vector2 lhs, Vector2 rhs) => new Vector2(Math.Max(lhs.X, rhs.X), Math.Max(lhs.Y, rhs.Y));

        /// <summary>
        ///     Adds two vectors.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector2 operator +(Vector2 a, Vector2 b) => new Vector2(a.X + b.X, a.Y + b.Y);

        /// <summary>
        ///     Subtracts one vector from another.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector2 operator -(Vector2 a, Vector2 b) => new Vector2(a.X - b.X, a.Y - b.Y);

        /// <summary>
        ///     Multiplies one vector by another.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector2 operator *(Vector2 a, Vector2 b) => new Vector2(a.X * b.X, a.Y * b.Y);

        /// <summary>
        ///     Divides one vector over another.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector2 operator /(Vector2 a, Vector2 b) => new Vector2(a.X / b.X, a.Y / b.Y);

        /// <summary>
        ///     Negates a vector.
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Vector2 operator -(Vector2 a) => new Vector2(-a.X, -a.Y);

        /// <summary>
        ///     Multiplies a vector by a number.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static Vector2 operator *(Vector2 a, double d) => new Vector2(a.X * d, a.Y * d);

        /// <summary>
        ///     Multiplies a vector by a number.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Vector2 operator *(double d, Vector2 a) => new Vector2(a.X * d, a.Y * d);

        /// <summary>
        ///     Divides a vector by a number.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static Vector2 operator /(Vector2 a, double d) => new Vector2(a.X / d, a.Y / d);

        /// <summary>
        ///     Returns true if the vectors are equal.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        /// <remarks>Returns false in the presence of NaN values.</remarks>
        public static bool operator ==(Vector2 lhs, Vector2 rhs)
        {
            double diffX = lhs.X - rhs.X;
            double diffY = lhs.Y - rhs.Y;
            return diffX * diffX + diffY * diffY < KEpsilon * KEpsilon;
        }

        /// <summary>
        ///     Returns true if vectors are different.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        /// <remarks>Returns true in the presence of NaN values.</remarks>
        public static bool operator !=(Vector2 lhs, Vector2 rhs) => !(lhs == rhs);

        public static explicit operator Vector2Int(Vector2 v) => new Vector2Int((int)v.X, (int)v.Y);

        /*// Converts a [[Vector3]] to a Vector2.
            public static implicit operator Vector2(Vector3 v)
            {
                return new Vector2(v.x, v.y);
            }

            // Converts a Vector2 to a [[Vector3]].
            public static implicit operator Vector3(Vector2 v)
            {
                return new Vector3(v.x, v.y, 0);
            }*/
        #endregion

        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
        public bool Equals(Vector2 other) => X == other.X && Y == other.Y;

        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (string.IsNullOrEmpty(format)) format = "F1";
            return $"({X.ToString(format, formatProvider)}, {Y.ToString(format, formatProvider)})";
        }
        /// <summary>
        /// Converts from polar to cartesian coordinates. <see cref="X"/> equals the length and <see cref="Y"/> the angle (in radians).
        /// </summary>
        /// <returns></returns>
        public Vector2 ToCartesian() => new Vector2(X * Math.Cos(Y), X * Math.Sin(Y));
        public Vector2 ToPolar() => new Vector2(Magnitude, Math.Atan2(Y, X));
    }
}