﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.Serialization;

namespace Utility
{
    [DataContract]
    public struct Vector2Int : IEquatable<Vector2Int>, IFormattable
    {
        #region Constants

        public const double KEpsilonNormalSqrt = 1e-15;

        #endregion

        #region Constructors

        public Vector2Int(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        #endregion

        #region Properties
        [DataMember] public int X { get; set; }

        [DataMember] public int Y { get; set; }
        /// <summary>
        ///     Access the <see cref="X" /> or <see cref="Y" /> component using [0] or [1] respectively.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int this[int index]
        {
            get =>
                index switch
                {
                    0 => X,
                    1 => Y,
                    _ => throw new IndexOutOfRangeException("Invalid Vector2Int index!"),
                };

            set
            {
                switch (index)
                {
                    case 0:
                        X = value;
                        break;
                    case 1:
                        Y = value;
                        break;
                    default:
                        throw new IndexOutOfRangeException("Invalid Vector2Int index!");
                }
            }
        }

        /// <summary>
        ///     Returns the length of this vector (RO).
        /// </summary>
        public double Magnitude => Math.Sqrt(X * X + Y * Y);

        /// <summary>
        ///     Returns the squared length of this vector (RO).
        /// </summary>
        public int SqrMagnitude => X * X + Y * Y;


        /// <summary>
        ///     Shorthand for writing <c>Vector2Int(0, 0)</c>.
        /// </summary>
        public static Vector2Int Zero { get; } = new Vector2Int(0, 0);

        /// <summary>
        ///     Shorthand for writing <c>Vector2Int(1, 1)</c>
        /// </summary>
        public static Vector2Int One { get; } = new Vector2Int(1, 1);

        /// <summary>
        ///     Shorthand for writing <c>Vector2Int(0, 1)</c>
        /// </summary>
        public static Vector2Int Up { get; } = new Vector2Int(0, 1);

        /// <summary>
        ///     Shorthand for writing <c>Vector2Int(0, -1)</c>
        /// </summary>
        public static Vector2Int Down { get; } = new Vector2Int(0, -1);

        /// <summary>
        ///     Shorthand for writing <c>Vector2Int(-1, 0)</c>
        /// </summary>
        public static Vector2Int Left { get; } = new Vector2Int(-1, 0);

        /// <summary>
        ///     Shorthand for writing <c>Vector2Int(1, 0)</c>
        /// </summary>
        public static Vector2Int Right { get; } = new Vector2Int(1, 0);

        #endregion

        #region Methods

        /// <summary>
        ///     Set x and y components of an existing Vector2Int.
        /// </summary>
        /// <param name="newX"></param>
        /// <param name="newY"></param>
        public void Set(int newX, int newY)
        {
            X = newX;
            Y = newY;
        }

        /// <summary>
        ///     Multiplies two vectors component-wise.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector2Int Scale(Vector2Int a, Vector2Int b) => new Vector2Int(a.X * b.X, a.Y * b.Y);

        /// <summary>
        ///     Multiplies every component of this vector by the same component of <paramref name="scale" />.
        /// </summary>
        /// <param name="scale"></param>
        public void Scale(Vector2Int scale)
        {
            X *= scale.X;
            Y *= scale.Y;
        }

        public override string ToString() => ToString(null, CultureInfo.InvariantCulture.NumberFormat);

        /// <summary>
        ///     Returns a nicely formatted string for this vector.
        /// </summary>
        /// <param name="format"></param>
        /// <returns></returns>
        public string ToString(string format) => ToString(format, CultureInfo.InvariantCulture.NumberFormat);

        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode() => X.GetHashCode() ^ (Y.GetHashCode() << 2);


        public override bool Equals(object other) => other is Vector2Int o && Equals(o);

        public static Vector2Int Reflect(Vector2Int inDirection, Vector2Int inNormal)
        {
            int factor = -2 * Dot(inNormal, inDirection);
            return new Vector2Int(factor * inNormal.X + inDirection.X, factor * inNormal.Y + inDirection.Y);
        }

        public static Vector2Int Perpendicular(Vector2Int inDirection) => new Vector2Int(-inDirection.Y, inDirection.X);

        /// <summary>
        ///     Dot Product of two vectors.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static int Dot(Vector2Int lhs, Vector2Int rhs) => lhs.X * rhs.X + lhs.Y * rhs.Y;

        /// <summary>
        ///     Returns the angle in degrees between <paramref name="from" /> and <paramref name="to" />.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static double Angle(Vector2Int from, Vector2Int to)
        {
            // sqrt(a) * sqrt(b) = sqrt(a * b) -- valid for real numbers
            double denominator = Math.Sqrt(from.SqrMagnitude * to.SqrMagnitude);
            if (denominator < KEpsilonNormalSqrt) return 0.0;

            double dot = Math.Clamp(Dot(from, to) / denominator, -1.0, 1.0);
            return Math.Acos(dot) * MathUtil.Rad2Deg;
        }

        /// <summary>
        ///     Returns the signed angle in degrees between <paramref name="from" /> and <paramref name="to" />.
        ///     Always returns the smallest possible angle.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static double SignedAngle(Vector2Int from, Vector2Int to)
        {
            double unsignedAngle = Angle(from, to);
            int sign = Math.Sign(@from.X * to.Y - @from.Y * to.X);
            return unsignedAngle * sign;
        }

        /// <summary>
        ///     Returns the distance between <paramref name="a" /> and <paramref name="b" />.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double Distance(Vector2Int a, Vector2Int b)
        {
            int diffX = a.X - b.X;
            int diffY = a.Y - b.Y;
            return Math.Sqrt(diffX * diffX + diffY * diffY);
        }

        /// <summary>
        ///     Returns a vector that is made from the smallest components of two vectors.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static Vector2Int Min(Vector2Int lhs, Vector2Int rhs) => new Vector2Int(Math.Min(lhs.X, rhs.X), Math.Min(lhs.Y, rhs.Y));

        /// <summary>
        ///     Returns a vector that is made from the largest components of two vectors.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static Vector2Int Max(Vector2Int lhs, Vector2Int rhs) => new Vector2Int(Math.Max(lhs.X, rhs.X), Math.Max(lhs.Y, rhs.Y));

        /// <summary>
        ///     Adds two vectors.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector2Int operator +(Vector2Int a, Vector2Int b) => new Vector2Int(a.X + b.X, a.Y + b.Y);

        /// <summary>
        ///     Subtracts one vector from another.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector2Int operator -(Vector2Int a, Vector2Int b) => new Vector2Int(a.X - b.X, a.Y - b.Y);

        /// <summary>
        ///     Multiplies one vector by another.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector2Int operator *(Vector2Int a, Vector2Int b) => new Vector2Int(a.X * b.X, a.Y * b.Y);

        /// <summary>
        ///     Divides one vector over another.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Vector2Int operator /(Vector2Int a, Vector2Int b) => new Vector2Int(a.X / b.X, a.Y / b.Y);

        /// <summary>
        ///     Negates a vector.
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Vector2Int operator -(Vector2Int a) => new Vector2Int(-a.X, -a.Y);

        /// <summary>
        ///     Multiplies a vector by a number.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static Vector2Int operator *(Vector2Int a, int d) => new Vector2Int(a.X * d, a.Y * d);

        /// <summary>
        ///     Multiplies a vector by a number.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Vector2Int operator *(int d, Vector2Int a) => new Vector2Int(a.X * d, a.Y * d);

        /// <summary>
        ///     Divides a vector by a number.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static Vector2Int operator /(Vector2Int a, int d) => new Vector2Int(a.X / d, a.Y / d);

        /// <summary>
        ///     Returns true if the vectors are equal.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        /// <remarks>Returns false in the presence of NaN values.</remarks>
        public static bool operator ==(Vector2Int lhs, Vector2Int rhs) => lhs.X == rhs.X && lhs.Y == rhs.Y;

        /// <summary>
        ///     Returns true if vectors are different.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        /// <remarks>Returns true in the presence of NaN values.</remarks>
        public static bool operator !=(Vector2Int lhs, Vector2Int rhs) => !(lhs == rhs);

        public static explicit operator Vector2(Vector2Int v) => new Vector2(v.X, v.Y);

        /*// Converts a [[Vector3]] to a Vector2Int.
            public static implicit operator Vector2Int(Vector3 v)
            {
                return new Vector2Int(v.x, v.y);
            }

            // Converts a Vector2Int to a [[Vector3]].
            public static implicit operator Vector3(Vector2Int v)
            {
                return new Vector3(v.x, v.y, 0);
            }*/
        #endregion

        [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
        public bool Equals(Vector2Int other) => X == other.X && Y == other.Y;

        public string ToString(string format, IFormatProvider formatProvider)
        {
            if (string.IsNullOrEmpty(format)) format = "F1";
            return $"({X.ToString(format, formatProvider)}, {Y.ToString(format, formatProvider)})";
        }
    }
}