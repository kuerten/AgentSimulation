﻿using System.Linq;

namespace Utility
{
    public class RollingAverage
    {
        private double[] _valueStorage;
        private int _writePointer = 0;
        private int _capacity;

        public int Capacity
        {
            get => _capacity;
            set
            {
                if (value == Capacity) return;
                _capacity = value;
                _valueStorage = new double[Capacity];
                _writePointer = 0;
            }
        }
        private int WritePointer
        {
            get => _writePointer;
            set
            {
                value %= Capacity;
                if (value == WritePointer) return;
                _writePointer = value;
            }
        }

        public double Average { get; private set; }

        public RollingAverage(int capacity)
        {
            Capacity = capacity;
        }

        public void Push(double value)
        {
            _valueStorage[WritePointer++] = value;
            Average = _valueStorage.Average();
        }
    }
}