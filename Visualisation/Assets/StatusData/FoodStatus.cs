﻿using SimpleJSON;

namespace Assets.StatusData
{
    public class FoodStatus : StatusBase
    {
        public double Energy { get; set; }

        public override bool SetValues(JSONNode node, long cycle)
        {
            if (!base.SetValues(node, cycle)) return false;
            Energy = node[nameof(Energy)].AsDouble;
            return true;
        }
    }
}
