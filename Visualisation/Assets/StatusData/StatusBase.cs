﻿using SimpleJSON;
using UnityEngine;

namespace Assets.StatusData
{
    public class StatusBase
    {
        public long Cycle { get; set; } = -1;
        public Vector2 Position { get; set; }

        public virtual bool SetValues(JSONNode node, long cycle)
        {
            if (Cycle >= 0 && cycle <= Cycle)
            {
                Debug.Log(cycle == Cycle ? $"Received same Cycle" : $"Received older Cycle {Cycle} - {cycle}");
                return false;
            }
            Cycle = cycle;
            Position = new Vector2(node["Position"]["X"] / 10f, node["Position"]["Y"] / 10f);
            return true;
        }
    }
}
