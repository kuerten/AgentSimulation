﻿using SimpleJSON;

namespace Assets.StatusData
{
    public class EntityStatus : StatusBase
    {
        public float Rotation { get; set; }
        public double? EnergyRatio { get; set; }
        public double TargetRatio { get; set; }
        public override bool SetValues(JSONNode node, long cycle)
        {
            if (!base.SetValues(node, cycle)) return false;
            Rotation = node["Rotation"].AsFloat;
            if (node.HasKey("Energy") && node.HasKey("MaxEnergy"))
            {
                EnergyRatio = node["Energy"].AsDouble / node["MaxEnergy"].AsDouble;
                TargetRatio = 300.0 / node["MaxEnergy"].AsDouble; //ToDo: hardcoded for now
            }
            return true;
        }
    }
}
