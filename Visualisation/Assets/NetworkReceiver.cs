﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using SimpleJSON;
using UnityEngine;

namespace Assets
{
    public enum EMessageType
    {
        Delete,
        Update,
        Reset
    }

    public enum EObjecType
    {
        Spawner = 1,
        Food = 0, 
        Entity = 2
    }

    [RequireComponent(typeof(ObjectManagementBehaviourScript))]
    public class NetworkReceiver : MonoBehaviour
    {
        private UdpClient _client;
        public ObjectManagementBehaviourScript ObjectManager;

        private List<JSONNode> _messages = new List<JSONNode>();
        private readonly object _lockObject = new object();
        // Start is called before the first frame update
        void Start()
        {
            ObjectManager = GetComponent<ObjectManagementBehaviourScript>();

            _client = new UdpClient(12345);
            IPAddress multicastAddress = IPAddress.Parse("239.0.1.2");
            IPEndPoint e = new IPEndPoint(IPAddress.Any, 0);
            _client.JoinMulticastGroup(multicastAddress);
            UdpState state = new UdpState { u = _client, e = e };
            _client.BeginReceive(ReceiveCallback, state);
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            UdpClient u = ((UdpState)(ar.AsyncState)).u;
            IPEndPoint e = ((UdpState)(ar.AsyncState)).e;

            byte[] receiveBytes = u.EndReceive(ar, ref e);
            string receiveString = Encoding.ASCII.GetString(receiveBytes);

            //Debug.Log($"Received: {receiveString}");
            lock (_lockObject)
            {
                _messages.Add(JSON.Parse(receiveString));
            }
            //ParseMessage(JSON.Parse(receiveString));
            u.BeginReceive(ReceiveCallback, ar.AsyncState);
        }

        private void ParseMessage(JSONNode node)
        {
            switch ((EMessageType)node["MessageType"].AsInt)
            {
                case EMessageType.Delete:
                    ParseDelete(node);
                    break;
                case EMessageType.Update:
                    ParseUpdate(node);
                    break;
                case EMessageType.Reset:
                    ParseReset(node);
                    break;
                default:
                    return;//throw new ArgumentOutOfRangeException();
            }
        }

        private void ParseUpdate(JSONNode node)
        {
            switch ((EObjecType)node["DataType"].AsInt)
            {
                case EObjecType.Entity: // Entity
                    ObjectManager.UpdateOrCreateEntity(node);
                    break;
                case EObjecType.Food: // Food
                    ObjectManager.UpdateOrCreateFood(node);
                    break;
                case EObjecType.Spawner: // FoodSpawner
                    //ToDo: Implement
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ParseDelete(JSONNode node)
        {
            switch ((EObjecType)node["DataType"].AsInt)
            {
                case EObjecType.Entity: // Entity
                    ObjectManager.DeleteEntity(node);
                    break;
                case EObjecType.Food: // Food
                    ObjectManager.DeleteFood(node);
                    break;
                case EObjecType.Spawner: // FoodSpawner
                    //ToDo: Implement
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ParseReset(JSONNode node)
        {
            ObjectManager.Clear();
        }

        // Update is called once per frame
        void Update()
        {
            List<JSONNode> localMessages;
            lock (_lockObject)
            {
                localMessages = _messages;
                _messages = new List<JSONNode>();
            }
            foreach (JSONNode node in localMessages) ParseMessage(node);
        }
    }

    public struct UdpState
    {
        public UdpClient u;
        public IPEndPoint e;
    }
}