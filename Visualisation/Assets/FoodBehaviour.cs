﻿using Assets.StatusData;
using UnityEngine;

namespace Assets
{
    public class FoodBehaviour : MonoBehaviour
    {
        public FoodStatus Status { get; } = new FoodStatus();
        // Start is called before the first frame update
        void Start()
        {
            transform.position = new Vector3(Status.Position.x, 0, Status.Position.y);
        }

        // Update is called once per frame
        void Update()
        {
            transform.position = new Vector3(Status.Position.x, Status.Position.y, 0);
        }
    }
}
