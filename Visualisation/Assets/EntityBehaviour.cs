﻿using Assets.StatusData;
using UnityEngine;

namespace Assets
{
    public class EntityBehaviour : MonoBehaviour
    {
        public EntityStatus Status { get; } = new EntityStatus();

        public Renderer Renderer;
        // Start is called before the first frame update
        void Start()
        {
            Renderer = GetComponent<Renderer>();
            transform.position = new Vector3(Status.Position.x, 0, Status.Position.y);
            transform.rotation = Quaternion.Euler(0, 0, Status.Rotation);
        }

        // Update is called once per frame
        void Update()
        {
            transform.position = new Vector3(Status.Position.x, Status.Position.y, 0);
            transform.rotation = Quaternion.Euler(0, 0, Mathf.Rad2Deg * Status.Rotation);
            if (Status.EnergyRatio != null)
            {
                Renderer.material.color = Color.Lerp(Color.red, Color.green, (float)(Status.EnergyRatio / Status.TargetRatio));
            }
        }
    }
}
