﻿using System;
using System.Collections.Generic;
using System.Linq;
using SimpleJSON;
using UnityEditor;
using UnityEngine;

namespace Assets
{
    public class ObjectManagementBehaviourScript : MonoBehaviour
    {
        public Dictionary<long, EntityBehaviour> Entities = new Dictionary<long, EntityBehaviour>();

        public Dictionary<long, FoodBehaviour> Food = new Dictionary<long, FoodBehaviour>();

        private GameObject _foodObject;
        private GameObject _foodPrefab;
        private GameObject _entityObject;
        private GameObject _entityPrefab;

        void Start()
        {
            _entityObject = new GameObject("Entity");
            _entityPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Entity.prefab");
            _entityObject.transform.parent = transform;
            _foodObject = new GameObject("Food");
            _foodPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Prefabs/Food.prefab");
            _foodObject.transform.parent = transform;
        }

        public void UpdateOrCreateEntity(JSONNode status)
        {
            int id = status["Id"].AsInt;
            int cycle = status["Cycle"].AsInt;
            if (Entities.TryGetValue(id, out EntityBehaviour existing))
            {
                existing.Status.SetValues(status, cycle);
                return;
            }
            GameObject obj = Instantiate(_entityPrefab, Vector3.zero, Quaternion.identity, _entityObject.transform);
            obj.name = "Entity Nr: " + id;
            EntityBehaviour behaviour = obj.GetComponent<EntityBehaviour>();
            behaviour.Status.SetValues(status, cycle);
            Entities.Add(id, behaviour);
        }

        public void DeleteEntity(JSONNode status)
        {
            int id = status["Id"].AsInt;
            int cycle = status["Cycle"].AsInt;
            if (!Entities.TryGetValue(id, out EntityBehaviour existing)) return;
            if (cycle <= existing.Status.Cycle)
            {
                Debug.Log("Received older Cycle");
                return;
            }
            Entities.Remove(id);
            Destroy(existing.gameObject);
        }

        public void UpdateOrCreateFood(JSONNode status)
        {
            int id = status["Id"].AsInt;
            int cycle = status["Cycle"].AsInt;
            if (Food.TryGetValue(id, out FoodBehaviour existing))
            {
                existing.Status.SetValues(status, cycle);
                return;
            }
            GameObject obj = Instantiate(_foodPrefab, Vector3.zero, Quaternion.identity, _foodObject.transform);
            obj.name = "Food Nr: " + id;
            FoodBehaviour food = obj.GetComponent<FoodBehaviour>();
            food.Status.SetValues(status, cycle);
            Food.Add(id, food);
        }

        public void DeleteFood(JSONNode status)
        {
            int id = status["Id"].AsInt;
            int cycle = status["Cycle"].AsInt;
            if (!Food.TryGetValue(id, out FoodBehaviour existing)) return;
            if (cycle <= existing.Status.Cycle)
            {
                Debug.Log("Received older Cycle");
                return;
            }
            Food.Remove(id);
            Destroy(existing.gameObject);
        }

        public void Clear()
        {
            foreach (MonoBehaviour obj in Entities.Values.Concat<MonoBehaviour>(Food.Values).ToArray()) DestroyImmediate(obj.gameObject);
            Entities.Clear();
            Food.Clear();
            Debug.Log("Simulation cleared");
        }
    }
}
