import functools
import re
import fnmatch
import os
import pandas
import json
import ipyparallel as ipp
from ipyparallel import require
import itertools
from collections.abc import Callable
import math
import glob


def get_value(obj, val: str):
    match = re.search(r'\[(\d*)]', val)
    if match is None:
        return obj[val]
    else:
        return obj[int(match.group().strip('[]'))]


def get_json_value(obj, attr: str):
    return functools.reduce(get_value, attr.split('.'), obj)


def get_json_values(obj, attr: list[str]):
    return {a.split('.')[-1]: get_json_value(obj, a) for a in attr}


def set_json_value(obj, attr: str, val):
    *h, t = attr.split('.')
    functools.reduce(get_value, h, obj)[t] = val


def find_files(base, pattern):
    return [n for n in fnmatch.filter(os.listdir(base), pattern) if
            os.path.isfile(os.path.join(base, n))]


def non_blank_lines(lines):
    for line in lines:
        line = line.rstrip()
        if line:
            yield line


def extract_data(path: str):
    data = []
    with open(path, 'r') as f_in:
        for line in non_blank_lines(f_in):
            data.append(json.loads(line))
    return pandas.json_normalize(data)


@require('os', 'pandas')
def convert_to_csv(base_path):
    data_input = os.path.join(base_path, 'data.txt')
    data_output = os.path.join(base_path, 'data.csv')
    if os.path.isfile(data_output):
        return False
    df = extract_data(data_input)
    df.to_csv(data_output, index=False)
    return True


def process(func, *values):
    """
    Processes the given values based on the defined function. A ipyparallel cluster is required.
    To start a cluster type '*path_to_python*\\\\Python\\\\Python39\\\\Scripts\\\\ipcluster.exe start' into a console
    """
    with ipp.Client() as rc:
        view = rc[:]
        async_result = view.map_async(func, *values)
        async_result.wait_interactive()
        return async_result.get()


def get_missing_cycles(cycle_data):
    row_iterator = cycle_data.sort_values('Cycle').iterrows()
    last_row = next(row_iterator)[1]
    missing_cycles = list()
    for index, row in row_iterator:
        currentCycle = last_row['Cycle'] + 1
        while currentCycle < row['Cycle']:
            c = last_row.copy()
            c['Cycle'] = currentCycle
            missing_cycles.append(c)
            currentCycle += 1
        last_row = row
    return missing_cycles


def complete_missing_data(data_path, cycle_limit=None):
    df = pandas.read_csv(data_path)
    if cycle_limit is not None:
        df = df[df['Cycle'] <= cycle_limit]
    missing = list(itertools.chain.from_iterable([get_missing_cycles(group) for index, group in df.groupby('Id')]))
    df.append(missing).to_csv(os.path.join(os.path.split(data_path)[0], 'data_complete.csv'), index=False)
    return len(missing)


def convert_and_complete_data(input_file, cycle_limit=None):
    if not os.path.isfile(input_file):
        return -1
    output_file = os.path.splitext(input_file)[0] + '.csv'
    df = extract_data(input_file)
    if cycle_limit is not None:
        df = df[df['Cycle'] <= cycle_limit]
    missing = list(itertools.chain.from_iterable([get_missing_cycles(group) for index, group in df.groupby('Id')]))
    df.append(missing).to_csv(output_file, index=False)
    os.remove(input_file)
    return len(missing)


def get_config(path: str):
    with open(path, 'r') as infile:
        return json.load(infile)


def create_config(json_data, parameters: list[str], values: list):
    pKeys = list(parameters)
    for parameter in parameters:
        set_json_value(json_data, parameter, values[pKeys.index(parameter)])
    return json_data


def map_config(json_data, parameters: list[str], function: Callable[[str, any], any]):
    for parameter in parameters:
        value = get_json_value(json_data, parameter)
        set_json_value(json_data, parameter, function(parameter, value))
    return json_data


def save_config(path: str, json_data):
    with open(path, 'w+') as outfile:
        json.dump(json_data, outfile, indent=2)


def calculate_distances(data):
    data.sort_values('Cycle', inplace=True)
    last = None
    for row in data[['Position.X', 'Position.Y']].values:
        if last is None:
            last = row
            continue
        yield math.sqrt((row[0] - last[0]) ** 2 + (row[1] - last[1]) ** 2)
        last = row


def calculate_accelerations(speed_data):
    yield 0  # acc is always 0 at the start
    for i in range(1, len(speed_data)):
        yield abs(speed_data[i] - speed_data[i - 1])


def generate_agility_data(data, datapoint_interval):
    distances = list(calculate_distances(data))
    speeds = list(map(lambda x: x / datapoint_interval, distances))
    accelerations = list(calculate_accelerations(speeds))
    return pandas.DataFrame(list(zip(distances, speeds, accelerations)), columns=['Distance', 'Speed', 'Acceleration'])


def get_agility_values(entity_df):
    return {
        'Distance': entity_df['Distance'].sum(), #        'Distance': entity_df['Distance'].max(),
        'Max_Speed': entity_df['Speed'].max(),
        'Mean_Speed': entity_df['Speed'].mean(),
        'Mean_Acceleration': entity_df['Acceleration'].mean(),
        'Max_Acceleration': entity_df['Acceleration'].max(),
        'Std_Acceleration': entity_df['Acceleration'].std()
    }


class DataFileEntry:
    SampleId: ''
    RunId: ''
    File: ''

    def __init__(self, sample_id, run_id, file):
        self.SampleId = sample_id
        self.RunId = run_id
        self.File = file

    def __str__(self):
        return f'Entry({self.RunId}, {self.SampleId}, {self.File})'


def get_data_files(base_path: str, file_filter: str) -> [DataFileEntry]:
    data_directories = list(filter(lambda x: os.path.isdir(os.path.join(base_path, x)), os.listdir(base_path)))
    return [DataFileEntry(os.path.split(os.path.dirname(file))[1], directory, file) for directory in data_directories
            for file in glob.iglob(os.path.join(base_path, directory, '**', file_filter), recursive=True)]
