import math


class ParameterEntry:
    Name: ''
    Path: ''
    Range: None

    def __init__(self, name, path, param_range=None):
        self.Name = name
        self.Path = path
        self.Range = param_range

    def __str__(self):
        return f'ParamEntry({self.Name})'


MovementParameter = [
        ParameterEntry('Mass', '[2].State.Mass.Value', [0.001, 100.0]),
        ParameterEntry('RotationVelocityMax', '[2].Components.[4].Component.RotationVelocityMax', [0.001, math.pi]),
        ParameterEntry('VelocityMax', '[2].Components.[4].Component.VelocityMax', [0.001, 100.0]),
        ParameterEntry('RotationForceMax', '[2].Components.[4].Component.RotationForceMax', [0.001, 10.0]),
        ParameterEntry('LinearForceMax', '[2].Components.[4].Component.LinearForceMax', [0.001, 10.0]),
        ParameterEntry('EnergyLoss', '[2].Components.[4].Component.EnergyLoss', [0.001, 0.999])
]

