\begin{tabular}{llrrrrrrrr}
\toprule
                &              &  Energy Mean &  Consumption Rate &  Energy Loss &  Linear Force Max &  Mass &  Rotation Force Max &  Rotation Velocity Max &  Velocity Max \\
Run Id & Sample Id &              &                   &              &                   &       &                     &                        &               \\
\midrule
20211013-232317 & sample_30-4 &         0.29 &              0.75 &         0.03 &              6.93 &  5.01 &                1.34 &                   0.39 &         36.88 \\
                & sample_50-145 &         0.13 &              1.25 &         0.00 &              4.79 & 50.41 &                9.69 &                   0.12 &         51.45 \\
                & sample_50-104 &         0.16 &              1.25 &         0.03 &              6.93 &  5.01 &                1.34 &                   0.39 &         36.88 \\
                & sample_30-45 &         0.23 &              0.75 &         0.00 &              4.79 & 50.41 &                9.69 &                   0.12 &         51.45 \\
                & sample_40-54 &         0.18 &              1.00 &         0.03 &              6.93 &  5.01 &                1.34 &                   0.39 &         36.88 \\
\bottomrule
\end{tabular}

